# VideoShader
web上のvideo要素にWebGL2によるシェーダエフェクトをかけるchrome extensionです。
エフェクトの切替、パラメータの操作に対応しています。
また、エディタでシェーダコードを作成・編集することでエフェクトの作成も可能です。
![ex1](images/slice.gif)![ex2](images/gameboy.gif)![ex3](images/wave.gif)![ex4](images/break.gif)

## Install
chromeの拡張機能管理画面にて*パッケージ化されていない拡張機能を読み込む*ボタンをクリックし、*videoshader*フォルダを選択してください。
※デベロッパーモードへのチェックが必要になります。

## usage
拡張機能がwebページでvideo要素を発見した場合、アイコンをクリックするとメニューがポップアップします。適用したいエフェクトを選択して*effect*スイッチをonにしてください。
- *index*にてエフェクトを適用するvideo要素を選択することができます。希望するvideo要素にエフェクトがかからない場合はこのindex番号を操作してみてください。
- *scale*にてエフェクトの描画解像度の倍率を変更できます。スペックが足りない、4kディスプレイを使用していて描画ピクセル数が多いなどで処理が重い場合はここで低い倍率を選択すると改善するかもしれません。
- *fps*にてエフェクトのフレームレートを変更することができます。ここで設定したフレームレートにて動画からフレーム画像を取得してエフェクトをかけている為、動画より高いフレームレートを設定している場合は一部のエフェクトで期待通りにならなかったり無駄に負荷が高くなったりします。また、このfpsは厳密に正確では有りません。
- エフェクトによってはパラメータをスライドバーにて操作することができます。
- 拡張機能のオプションからeditorに移動することができます。ここではシェーダコードの作成・編集・確認が可能です。

## editor
### effectについて
背景色、モデル情報、各種コード、uniform情報をひとまとめにしたものがeffectになります。
*save*ボタンを押した場合上記のデータが保存されます。ただ、コードについて保存されるのは選択されている名前のみになる為、コード内容自体の保存は各コードの*save*ボタンで行って下さい。
テキストエリアに任意の名前を入力することで別名保存と名前の変更が可能です。
### background colorについて
gl.clearColorに使用する値を設定することができます。
### instanceについて
使用するモデルとモデルの描画個数を設定することができます。
モデルは基本的に正規化デバイス空間内にいっぱいになるサイズになっています。
モデル名の先頭に*fit*がついているものについては回転させても正規化デバイス座標に収まるように縮小されています。
モデルを追加したい場合はvao.mjsとoptions.htmlのselectタブに追記して下さい。
モデルの頂点情報は頂点座標xyz・法線ベクトルxyz・テクスチャ座標xyのインターリーブ配列となっています。
### shader codesについて
各種コードを作成、編集することが可能です。
*feedback*はtransform feedbackを使用することができるvertex shaderです。ここで計算した頂点情報を次の通常のvertex shaderで使用することができます。transform feedbackを使用しない場合は*none*を選択して下さい。
*vertex*と*fragment*はそれぞれ通常のvertex shaderとfragment shaderです。
各種コードで使用可能なattributeについては下記を参照して下さい。
テキストエディタにはMonaco Editorを使用しています。
### デフォルトで使用可能なattribute
- feedback shader
  - layout (location = 0 ~ 4) in \[xxx] → transform feedbackのバッファ入力
  - out \[xxx] _buffer_a ~ e → transform feedbackのバッファ出力
    - \[xxx]はfloat, vec2, vec3, vec4, mat2, mat3, mat4のいずれかを使用できます
    - instance1つにつき1つのバッファが用意されます
- vertex shader
  - layout (location = 0) in vec3 → instanceの頂点座標
  - layout (location = 1) in vec3 → instanceの法線ベクトル
  - layout (location = 2) in vec2 → instanceのテクスチャ座標
  - layout (location = 3 ~ 7) in \[xxx] → transform feedbackのバッファ入力
- fragment shader
  - layout (location = 0) out vec4 → canvasへの出力
  - layout (location = 1) out vec4 → 浮動小数点テクスチャバッファへの出力
### uniformについて
各種コードで使用することができるuniform変数を設定することができます。
任意の次元数の識別子を選択して*add*ボタンを押して追加し、名前を入力することでスライダの値をuniform変数からコード内で使用することができるようになります。
名前と同様にスライダの最小値、最大値、ステップを設定することも可能です。
ここで設定したuniform変数の他に使用することができる変数は下記を参照して下さい。
### デフォルトで使用可能なuniform変数
- float _frame
  - 描画済のフレーム数
- float _fps
  - エフェクトのfps
- vec2 _canvas_res
  - エフェクトのレンダリング先の解像度
- vec2 _video_res
  - 動画の解像度
- float _duration
  - 動画の全体の長さ（秒）
- float _time
  - 現在の動画再生時間（秒）
- float _instance
  - instanceの描画個数
- sampler2D _frag_buffer
  - 前回描画時にfragment shaderでlayout (location = 1)に出力したバッファ
  - 解像度は_canvas_resと同じ
- sampler2D _video
  - 現在の動画フレーム画像
- sampler2D _prev_video
  - 1つ前のフレームの動画フレーム画像
  - fpsの設定が動画のフレームレートより高速だった場合は_videoと_prev_videoの内容が同一になる可能性有り
- sampler2D _audio
  - 現在の動画フレームのオーディオデータ(W　*　H →　512 * 4)
  - height = 0.0 → Lchの周波数データ
  - height = 1.0 → Rchの周波数データ
  - height = 2.0 → Lchの波形データ
  - height = 3.0 → Rchの波形データ
### checkについて
エフェクトの確認をすることができます。
*compile*ボタンを押すことで現在のエフェクトをコンパイルします。
コンパイルに失敗した場合はテキストエリアにエラーメッセージが表示されます。
コンパイルに成功した場合は右側の動画にエフェクトが適応されます。
### videoについて
エフェクト確認用の動画です。
動画の再生は自動的に繰り返されます。
再生中に*rec*ボタンを押すとレンダリング結果を動画で、
停止中に*rec*ボタンを押すとレンダリング結果を画像で書き出します。
書き出されるのはあくまでWebGLによるレンダリング結果のため透過を利用して元動画を表示させている部分は書き出されません。
動画が気に入らない場合は*sample.mp4*ファイルを差し替えて下さい。(動画サイズ → 640 * 360)

## notes
- youtubeで動作確認をしている為他のサイトでの動作はあまり確認していません
- video要素のクロスオリジン対応ができていないです
- デフォルトのuniform変数名はmodulesフォルダ内のconfig.mjsで好きな名前に変更可能です
- transform feedbackのバッファが5つでは足りない場合はconfig.mjs内のプロパティvaryingsの配列に追加することで増やせます
