'use strict';

const kUI = document.getElementById("ui");
const kStatus = document.getElementById("status");
const kSelect = document.getElementById("effect-select");
const kSwitch = document.getElementById("effect-switch");
const kIndex = document.getElementById("index");
const kFPS = document.getElementById("fps");
const kScale = document.getElementById("scale");
const kUniformRanges = document.getElementById("uniform-ranges");
const kUniformEvents = new WeakMap;
const kTabsQueryInfo = {
  currentWindow: true,
  active: true,
  url: [
    "http://*/*",
    "https://*/*"
  ]
};

let kUniformDatas = [];

function UniformsUpdate() {
  chrome.tabs.query({ currentWindow: true, active: true }, tabs => {
    chrome.tabs.sendMessage(tabs[0].id, {
      subject: "uniforms_change",
      message: kUniformDatas
    }
    );
  });
}

function AppendNamedRangeDiv(elm, name, class_names, uniform_index, value_index, setting) {
  let div = document.createElement("div");
  let name_span = document.createElement("span");
  name_span.innerText = name.display;
  name_span.classList.add(class_names.index);
  let range = document.createElement("input");
  range.type = "range";
  range.min = setting.min;
  range.max = setting.max;
  range.step = setting.step;
  range.value = setting.value;
  range.classList.add(class_names.range);
  range.setAttribute("id", "r" + value_index);
  let range_value = document.createElement("span");
  range_value.innerText = range.value;
  range_value.classList.add(class_names.value);
  div.appendChild(name_span);
  div.appendChild(range_value);
  div.appendChild(range);
  kUniformEvents.set(range, () => {
    range_value.innerText = range.value;
    kUniformDatas[uniform_index].values[value_index] = range.value;
    UniformsUpdate();
  });
  range.addEventListener("input", kUniformEvents.get(range));
  elm.appendChild(div);
  return range;
}

function SetUniformsData(datas) {
  kUniformDatas = datas;
  datas.forEach((data, uniform_index) => {
    let uniform_div = document.createElement("div");
    uniform_div.classList.add("PopupUI");
    let uniform_name = document.createElement("div");
    uniform_name.innerText = data.name;
    uniform_div.appendChild(uniform_name);
    let ranges = [];
    let range_class_name = {
      index: "UniformIndex",
      value: "RangeValue",
      range: "Range"
    }
    if (data.type === "float") {
      let name = {
        display: "n",
        data: data.name
      }
      let setting = {
        min: data.min,
        max: data.max,
        step: data.step,
        value: data.values[0]
      }
      ranges.push(AppendNamedRangeDiv(uniform_div, name, range_class_name, uniform_index, 0, setting));
    }
    else {
      let chars = [...data.type];
      let setting = {
        min: data.min,
        max: data.max,
        step: data.step,
        value: null
      }
      chars.forEach((char, value_index) => {
        setting.value = data.values[value_index];
        let name = {
          display: char,
          data: data.name
        }
        ranges.push(AppendNamedRangeDiv(uniform_div, name, range_class_name, uniform_index, value_index, setting));
      });
    }
    kUniformRanges.appendChild(uniform_div);
  });
  if (kUniformDatas.length === 0) {
    document.getElementById("rand-block").style.display = "none";
  } else {
    document.getElementById("rand-block").style.display = "block";
  }
}

function RandomizeUniforms() {
  let ranges = kUniformRanges.getElementsByTagName("input");
  ranges = Array.prototype.slice.call(ranges);
  ranges.forEach(range => {
    let min = Number(range.min);
    let max = Number(range.max);
    range.value = Math.random() * (max - min) + min;
    kUniformEvents.get(range)();
  });
}

document.getElementById("randomize").addEventListener("click", RandomizeUniforms);

function ResetUniformsDiv() {
  let ranges = kUniformRanges.getElementsByTagName("input");
  ranges = Array.prototype.slice.call(ranges);
  ranges.forEach(range => {
    range.removeEventListener("input", kUniformEvents.get(range));
  });
  kUniformRanges.textContent = null;
}

function EffectChange(effect) {
  chrome.storage.local.set({ current: effect.name });
  chrome.tabs.query({ currentWindow: true, active: true }, tabs => {
    chrome.tabs.sendMessage(tabs[0].id, {
      subject: "effect_change",
      message: effect
    }
    );
  });
}

function AppendSelect(select_elm, new_option) {
  let list = Array.prototype.slice.call(select_elm.children);
  if (list.length === 0) {
    select_elm.appendChild(new_option);
    return;
  }
  else {
    let item = list.find(item => item.value > new_option.value);
    if (item !== void 0) select_elm.insertBefore(new_option, item);
    else select_elm.appendChild(new_option);
  }
}

function InitSelect() {
  chrome.storage.local.get("effects", effects => {
    effects.effects.forEach(effect => {
      let opt = document.createElement("option");
      opt.text = effect.name;
      opt.value = effect.name;
      AppendSelect(kSelect, opt);
    });
  });
  kSelect.addEventListener("change", event => {
    chrome.storage.local.set({ current: event.target.value });
    chrome.runtime.sendMessage({
      subject: "change_current",
      message: event.target.value
    });
    chrome.storage.local.get("effects", effects => {
      let effect = effects.effects.find(effect => effect.name === event.target.value);
      ResetUniformsDiv();
      SetUniformsData(effect.uniforms);
    });
  });
}

function InitScale() {
  chrome.storage.local.get("scale", current_scale => {
    kScale.value = current_scale.scale;
    console.log(current_scale.scale);
  });
  kScale.addEventListener("change", event => {
    chrome.storage.local.set({ scale: event.target.value });
    chrome.tabs.query({ currentWindow: true, active: true }, tabs => {
      chrome.tabs.sendMessage(tabs[0].id, {
        subject: "scale_change",
        message: event.target.value
      }
      );
    });
  });
}

function InitFps() {
  chrome.storage.local.get("fps", current_fps => {
    kFPS.value = current_fps.fps;
  });
  kFPS.addEventListener("change", event => {
    chrome.storage.local.set({ fps: event.target.value });
    chrome.tabs.query({ currentWindow: true, active: true }, tabs => {
      chrome.tabs.sendMessage(tabs[0].id, {
        subject: "fps_change",
        message: event.target.value
      }
      );
    });
  });
}

function BranchByStatus(status) {
  switch (status) {
    case "loading":
      kUI.style.display = "none";
      kStatus.innerText = "now loading...";
      break;
    case "outofscope":
      kUI.style.display = "none";
      kStatus.innerText = "this page is out of scope";
      break;
    case "searching":
      kUI.style.display = "none";
      kStatus.innerText = "searching...";
      break;
    case "nothing":
      kUI.style.display = "none";
      kStatus.innerText = "video not found";
      break;
    case "preparation":
      kUI.style.display = "none";
      kStatus.innerText = "in preparation";
      break;
    case "ready":
      kUI.style.display = "block";
      kStatus.style.display = "none";
      InitUI();
      break;
  }
}

function InitUI() {
  InitSelect();
  InitFps();
  InitScale();
  chrome.tabs.query({ currentWindow: true, active: true }, tabs => {
    chrome.tabs.sendMessage(tabs[0].id, {
      subject: "effect_name"
    },
      response => kSelect.value = response
    );
    chrome.tabs.sendMessage(tabs[0].id, {
      subject: "effect_status"
    },
      response => InitEffectSwitch(response)
    );
    chrome.tabs.sendMessage(tabs[0].id, {
      subject: "video_length"
    },
      response => InitVideoIndexList(response)
    );
    chrome.tabs.sendMessage(tabs[0].id, {
      subject: "video_index"
    },
      response => InitCurrentVideoIndex(response)
    );
    chrome.tabs.sendMessage(tabs[0].id, {
      subject: "uniforms_status"
    },
      response => SetUniformsData(response)
    );
  });
}

function InitEffectSwitch(flag) {
  kSwitch.checked = flag === "on";
  kSwitch.addEventListener("change", e => {
    let message = e.target.checked ? "on" : "off";
    chrome.tabs.query({ currentWindow: true, active: true }, tabs => {
      chrome.tabs.sendMessage(tabs[0].id, {
        subject: "effect_switch",
        message: message
      }
      );
    });
  });
}

function SetIndexByOrigin(data) {
  chrome.storage.local.get("indices", indices => {
    let index = indices.indices.findIndex(index => index.origin === data.origin);
    if (index < 0) {
      indices.indices.push(data);
    }
    else {
      indices.indices[index] = data;
    }
    chrome.storage.local.set({ indices: indices.indices });
  });
}

function InitVideoIndexList(length) {
  if (length !== null) {
    for (let i = 0; i < length; ++i) {
      let opt = document.createElement("option");
      opt.text = i;
      opt.value = i;
      kIndex.appendChild(opt);
    }
    kIndex.addEventListener("change", event => {
      chrome.tabs.query({ currentWindow: true, active: true }, tabs => {
        chrome.tabs.sendMessage(tabs[0].id, {
          subject: "index_change",
          message: event.target.value
        }
        );
        SetIndexByOrigin({
          origin: (new URL(tabs[0].url)).origin,
          index: event.target.value
        });
      });
    });
  }
}

function InitCurrentVideoIndex(index) {
  kIndex.value = index;
}

chrome.tabs.query(kTabsQueryInfo, tabs => {
  if (tabs.length !== 0) {
    switch (tabs[0].status) {
      case "unloaded":
      case "loading":
        BranchByStatus("loading");
        break;
      case "complete":
        chrome.tabs.sendMessage(tabs[0].id, {
          subject: "video_status"
        },
          response => BranchByStatus(response)
        );
        break;
    }
  }
  else BranchByStatus("outofscope")
});

chrome.runtime.onMessage.addListener(
  function (request, sender, callback) {
    switch (request.subject) {
      case "video_existence":
        if (request.message === "true") BranchByStatus("preparation");
        if (request.message === "false") BranchByStatus("nothing");
        break;
      case "renderer_getready":
        if (request.message === "true") BranchByStatus("ready");
        break;
      default:
        break;
    }
    return true;
  }
);