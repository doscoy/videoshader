"use strict";
import { EffectManager, CodeManager } from "./modules/data_manager.mjs"
import { UniformsManager } from "./modules/uniform_manager.mjs"
import { BgColorManager } from "./modules/bgcolor_manager.mjs"
import { InstanceManager } from "./modules/instance_manager.mjs"
import { PreviewRenderer } from "./modules/renderer.mjs";

class Conductor {
  effect = null;
  background = null;
  instance = null;
  feedback = null;
  vertex = null;
  fragment = null;
  uniforms = null;
  renderer = null;

  constructor(init) {
    this.effect = new EffectManager(init.effect);
    this.background = new BgColorManager(init.background);
    this.instance = new InstanceManager(init.instance);
    this.feedback = new CodeManager(init.feedback);
    this.vertex = new CodeManager(init.vertex);
    this.fragment = new CodeManager(init.fragment);
    this.uniforms = new UniformsManager(init.uniforms);
    this.renderer = new PreviewRenderer(this.uniforms,
      this.background,
      this.instance);
    chrome.storage.local.get("current", item => this._ChangeEffect(item.current));
    this._RegisterEvent(init);
  }

  async _ChangeEffect(name) {
    await this.effect.ChangeCurrent(name);
    this._MatchingEffect();
  }

  _MatchingEffect() {
    this.feedback.ChangeCurrent(this.effect.GetCurrentFdbk());
    this.vertex.ChangeCurrent(this.effect.GetCurrentVert());
    this.fragment.ChangeCurrent(this.effect.GetCurrentFrag());
    this.uniforms.Reset();
    this.uniforms.SetUniforms(this.effect.GetCurrentUniforms());
    this.background.SetValues(this.effect.GetCurrentBgColor());
    this.instance.SetModel(this.effect.GetCurrentModel());
    this.instance.SetCount(this.effect.GetCurrentCount());
    this.renderer.IntervalStop();
  }

  Import() {
    let input = document.createElement("input");
    input.type = "file";
    input.accept = ".videoshader"
    let callback = () => {
      if (input.files.length == 0) return;
      let reader = new FileReader();
      reader.onload = () => {
        let data = JSON.parse(reader.result);
        this.effect.SetTextValue(data.effect.name);
        this.uniforms.Reset();
        this.uniforms.SetUniforms(data.effect.uniforms);
        this.background.SetValues(data.effect.background);
        this.instance.SetModel(data.effect.model);
        this.instance.SetCount(data.effect.count);
        this.feedback.SetTextValue(data.fdbk_code.name);
        this.feedback.SetCode(data.fdbk_code.code);
        this.vertex.SetTextValue(data.vert_code.name);
        this.vertex.SetCode(data.vert_code.code);
        this.fragment.SetTextValue(data.frag_code.name);
        this.fragment.SetCode(data.frag_code.code);
        this.renderer.IntervalStop();
      }
      reader.readAsText(input.files[0]);
      input.removeEventListener("input", callback);
      input = null;
    };
    input.addEventListener("input", callback)
    input.click();
  }

  ExportCurrent() {
    let effect_name = this.effect.GetTextValue();
    let fdbk_name = this.feedback.GetTextValue();
    let vert_name = this.vertex.GetTextValue();
    let frag_name = this.fragment.GetTextValue();
    let data = {
      effect: {
        name: effect_name,
        fdbk: fdbk_name,
        vert: vert_name,
        frag: frag_name,
        background: this.background.GetValues(),
        model: this.instance.GetModel(),
        count: this.instance.GetCount(),
        uniforms: this.uniforms.GetDatas()
      },
      fdbk_code: {
        name: fdbk_name,
        code: this.feedback.GetCurrentCode()
      },
      vert_code: {
        name: vert_name,
        code: this.vertex.GetCurrentCode()
      },
      frag_code: {
        name: frag_name,
        code: this.fragment.GetCurrentCode()
      }
    };
    let json = JSON.stringify(data, null, 2);
    let blob = new Blob([json], { type: "application/json" });
    let a = document.createElement("a");
    a.href = URL.createObjectURL(blob)
    a.download = effect_name + ".videoshader";
    a.click();
    URL.revokeObjectURL(a.href);
  }

  _RegisterEvent(init) {
    init.effect.select_elm.addEventListener("change", event => {
      this._ChangeEffect(event.target.value);
      init.renderer.message_elm.value = "";
    });
    init.feedback.select_elm.addEventListener("change", event => {
      this.feedback.ChangeCurrent(event.target.value);
    });
    init.vertex.select_elm.addEventListener("change", event => {
      this.vertex.ChangeCurrent(event.target.value);
    });
    init.fragment.select_elm.addEventListener("change", event => {
      this.fragment.ChangeCurrent(event.target.value);
    });
    init.effect.save_elm.addEventListener("click", () => {
      let fdbk_name = this.feedback.GetSelectValue();
      let vert_name = this.vertex.GetSelectValue();
      let frag_name = this.fragment.GetSelectValue();
      let uniforms = this.uniforms.GetDatas();
      let bg_rgba = this.background.GetValues();
      let model = this.instance.GetModel();
      let count = this.instance.GetCount();
      this.effect.SaveData(fdbk_name, vert_name, frag_name, uniforms, bg_rgba, model, count);
    });
    init.feedback.save_elm.addEventListener("click", () => this.feedback.SaveCode());
    init.vertex.save_elm.addEventListener("click", () => this.vertex.SaveCode());
    init.fragment.save_elm.addEventListener("click", () => this.fragment.SaveCode());
    init.effect.rename_elm.addEventListener("click", () => this.effect.RenameCurrent());
    init.feedback.rename_elm.addEventListener("click", () => this.feedback.RenameCurrent());
    init.vertex.rename_elm.addEventListener("click", () => this.vertex.RenameCurrent());
    init.fragment.rename_elm.addEventListener("click", () => this.fragment.RenameCurrent());
    init.effect.new_elm.addEventListener("click", () => {
      this.uniforms.Reset();
      this.background.SetValues({ r: 0.0, g: 0.0, b: 0.0, a: 1.0 });
      this.instance.SetModel("panel");
      this.instance.SetCount(1);
    });
    init.feedback.new_elm.addEventListener("click", () => this.feedback.SetDefaultCode());
    init.vertex.new_elm.addEventListener("click", () => this.vertex.SetDefaultCode());
    init.fragment.new_elm.addEventListener("click", () => this.fragment.SetDefaultCode());
    init.effect.del_elm.addEventListener("click", async () => {
      if (confirm("remove the current effect.\nare you sure?")) {
        await this.effect.DeleteData();
        this._MatchingEffect();
      }
    });
    init.feedback.del_elm.addEventListener("click", () => {
      if (confirm("remove the current feedback code.\nare you sure?")) this.feedback.DeleteCode();
    });
    init.vertex.del_elm.addEventListener("click", () => {
      if (confirm("remove the current vertex code.\nare you sure?")) this.vertex.DeleteCode();
    });
    init.fragment.del_elm.addEventListener("click", () => {
      if (confirm("remove the current fragment code.\nare you sure?")) this.fragment.DeleteCode();
    });
    init.effect.import_elm.addEventListener("click", () => this.Import());
    init.effect.export_elm.addEventListener("click", () => this.ExportCurrent());
    init.renderer.compile_elm.addEventListener("click", () => {
      let code = {
        feedback: this.feedback.GetCurrentCode(),
        vertex: this.vertex.GetCurrentCode(),
        fragment: this.fragment.GetCurrentCode(),
        uniforms: this.uniforms.GetDatas(),
        background: this.background.GetValues(),
        model: this.instance.GetModel(),
        count: this.instance.GetCount()
      }
      this.renderer.ChangeShader(code);
      let ready = this.renderer.CheckShaderIsReady();
      let msg = ready ? "compiled it!!\n\n" : "";
      msg += this.renderer.CheckShaderMessage();
      init.renderer.message_elm.value = msg;
      if (ready) this.renderer.IntervalDraw();
    });
  }
}

const kConductorInit = {
  effect: {
    name: "effects",
    select_elm: document.getElementById("effect"),
    text_elm: document.getElementById("effect-name"),
    save_elm: document.getElementById("effect-save"),
    rename_elm: document.getElementById("effect-rename"),
    new_elm: document.getElementById("effect-new"),
    del_elm: document.getElementById("effect-delete"),
    import_elm: document.getElementById("effect-import"),
    export_elm: document.getElementById("effect-export")
  },
  feedback: {
    name: "fdbk_codes",
    select_elm: document.getElementById("feedback"),
    text_elm: document.getElementById("fdbk-name"),
    save_elm: document.getElementById("fdbk-save"),
    rename_elm: document.getElementById("fdbk-rename"),
    new_elm: document.getElementById("fdbk-new"),
    del_elm: document.getElementById("fdbk-delete"),
    code_elm: document.getElementById("fdbk-code"),
    default: "#version 300 es\n\nvoid main(void)\n{\n  // code here!!\n}"
  },
  vertex: {
    name: "vert_codes",
    select_elm: document.getElementById("vertex"),
    text_elm: document.getElementById("vert-name"),
    save_elm: document.getElementById("vert-save"),
    rename_elm: document.getElementById("vert-rename"),
    new_elm: document.getElementById("vert-new"),
    del_elm: document.getElementById("vert-delete"),
    code_elm: document.getElementById("vert-code"),
    default: "#version 300 es\n\nlayout (location = 0) in vec2 _vp;\nlayout (location = 1) in vec2 _vn;\nlayout (location = 2) in vec2 _vt;\n\nvoid main(void)\n{\n  // code here!!\n}"
  },
  fragment: {
    name: "frag_codes",
    select_elm: document.getElementById("fragment"),
    text_elm: document.getElementById("frag-name"),
    save_elm: document.getElementById("frag-save"),
    rename_elm: document.getElementById("frag-rename"),
    new_elm: document.getElementById("frag-new"),
    del_elm: document.getElementById("frag-delete"),
    code_elm: document.getElementById("frag-code"),
    default: "#version 300 es\nprecision mediump float;\n\nlayout (location = 0) out vec4 _color;\n\nvoid main(void)\n{\n  // code here!!\n}"
  },
  uniforms: {
    target_div: document.getElementById("uniform-ranges"),
    type_elm: document.getElementById("uniform-dimension"),
    add_elm: document.getElementById("uniform-add")
  },
  renderer: {
    compile_elm: document.getElementById("compile"),
    message_elm: document.getElementById("message")
  },
  background: {
    r_elm: document.getElementById("bg_r"),
    g_elm: document.getElementById("bg_g"),
    b_elm: document.getElementById("bg_b"),
    a_elm: document.getElementById("bg_a"),
    r_value: document.getElementById("bg_r_value"),
    g_value: document.getElementById("bg_g_value"),
    b_value: document.getElementById("bg_b_value"),
    a_value: document.getElementById("bg_a_value")
  },
  instance: {
    model_elm: document.getElementById("model_name"),
    count_elm: document.getElementById("draw_count")
  }
}

class Recorder {
  canvas = null;
  video = null;
  div = null;
  button = null;
  bit_rate = null;
  recorder = null;
  rec_active = false;

  constructor(canvas_id, video_id, div_id) {
    this.canvas = document.getElementById(canvas_id);
    this.video = document.getElementById(video_id);
    this.div = document.getElementById(div_id);
    this.button = document.createElement("button");
    this.button.type = "button";
    this.button.innerText = "rec";
    this.button.classList = "Buttons";
    this.button.addEventListener("click", () => { this._ButtonClickEvent(); });
    let bit_rate_text = document.createElement("span");
    bit_rate_text.innerText = "bit rate"
    this.bit_rate = document.createElement("input");
    this.bit_rate.type = "number";
    this.bit_rate.value = 5000000;
    this.bit_rate.classList = "MiniInputText";
    this.bit_rate.style = "width: 80px;";
    this.bit_rate.addEventListener("change", e => {
      e.target.value = Math.min(Math.max(e.target.value, 100000), 1000000000);
    });
    this.div.appendChild(bit_rate_text);
    this.div.appendChild(this.bit_rate);
    this.div.appendChild(this.button);
  }

  _SavePNG() {
    let a = document.createElement("a");
    a.href = this.canvas.toDataURL("image/png");
    a.download = "videoshader_snap.png";
    a.click();
  }

  _OnDataAvailable(event) {
    let blob = new Blob([event.data], { type: event.data.type });
    let a = document.createElement("a");
    a.href = URL.createObjectURL(blob)
    a.download = "videoshader_rec.webm";
    a.click();
    URL.revokeObjectURL(a.href);
  }

  _GetAudioStream() {
    let context = new AudioContext();
    let video_stream = this.video.captureStream();
    if (video_stream.getAudioTracks().length > 0) {
      let source = context.createMediaStreamSource(this.video.captureStream());
      let stream_dest = context.createMediaStreamDestination();
      source.connect(stream_dest);
      return stream_dest.stream;
    } else {
      return new MediaStream();
    }
  }

  _SetTracks2Stream(stream, tracks) {
    tracks.forEach( track => { stream.addTrack(track); });
  }

  _RecStart() {
    let media_stream = new MediaStream();
    let canvas_tracks = this.canvas.captureStream().getTracks();
    let audio_tracks = this._GetAudioStream().getTracks();
    this._SetTracks2Stream(media_stream, canvas_tracks);
    this._SetTracks2Stream(media_stream, audio_tracks);
    let option = {
      videoBitsPerSecond : this.bit_rate.value,
      mimeType : "video/webm; codecs=vp9"
    };
    this.recorder = new MediaRecorder(media_stream, option);
    this.recorder.ondataavailable = e => { this._OnDataAvailable(e); };
    this.recorder.start();
    this.button.innerText = "stop";
    this.rec_active = true;
  }

  _RecStop() {
    this.recorder.stop();
    this.recorder = null;
    this.button.innerText = "rec";
    this.rec_active = false;
  }

  _ButtonClickEvent() {
    if (this.rec_active) {
      this._RecStop();
    } else {
      if (this.video.paused) {
        this._SavePNG();
      } else {
        this._RecStart();
      }
    }
  }
}

let conductor = null;
let recorder = null;

onload = () => {
  let video = document.getElementById("video");
  let box = document.getElementById("video-box");
  box.style.width = video.videoWidth + "px";
  box.style.height = video.videoHeight + "px";
  conductor = new Conductor(kConductorInit);
  recorder = new Recorder("effector", "video", "rec-container");
}

function StorageInfo() {
  return new Promise(resolve => {
    chrome.storage.local.getBytesInUse(use => {
      let use_str = "use: " + use.toLocaleString() + "byte";
      let nemaining_bytes = chrome.storage.local.QUOTA_BYTES - use;
      let remaining_str = "remaining: " + (nemaining_bytes).toLocaleString() + "byte";
      let msg = "storage contents were output to the console"
      resolve([use_str, remaining_str, msg].join("\n"));
      chrome.storage.local.get(items => {
        console.log(items);
        items.indices = [];
        console.log(JSON.stringify(items, null, 2));
      });
    });
  });
}

function GPUInfo() {
  let gl = document.createElement("canvas").getContext("webgl2");
  let info = gl.getExtension('WEBGL_debug_renderer_info');
  let user_agent = "user agent: " + window.navigator.userAgent
  var vendor = "vendor: " + gl.getParameter(info.UNMASKED_VENDOR_WEBGL);
  var renderer = "renderer: " + gl.getParameter(info.UNMASKED_RENDERER_WEBGL);
  return [user_agent, vendor, renderer].join("\n");
}

document.getElementById("code_adjust").addEventListener("mousedown", (e) => {
  let us = document.body.style.userSelect;
  document.body.style.userSelect = "none";
  let target = document.getElementById("code-editor");
  let height = target.getBoundingClientRect().height;
  let y = e.pageY;
  let ScrollBan = (e) => e.preventDefault();
  let Resize = (e) => {
    let dy = e.pageY - y;
    let new_height = height + dy;
    target.style.height = new_height > 100 ? new_height : 100 + "px";
    if (e.movementY < 0) scrollBy(0, e.movementY);
  }
  let Cleaning = () => {
    document.removeEventListener("mousemove", Resize);
    document.removeEventListener("mousewheel", ScrollBan, { passive: false });
    document.removeEventListener("mouseup", Cleaning);
    document.body.style.userSelect = us;
  }
  document.addEventListener("mousemove", Resize);
  document.addEventListener("mousewheel", ScrollBan, { passive: false });
  document.addEventListener("mouseup", Cleaning);
});

document.getElementById("video-button").addEventListener("click", e => {
  let video = document.getElementById("video");
  if (video.paused) {
    video.play();
    e.target.innerText = "stop";
  } else { 
    video.pause();
    e.target.innerText = "play";
  }
});

document.getElementById("video-range").addEventListener("input", e => {
  let video = document.getElementById("video");
  if (e.target.value === 0) video.muted = true;
  else {
    video.muted = false;
    video.volume = e.target.value;
  }
});

document.getElementById("init").addEventListener("click", () => {
  let message = "remove all your code and restore it to its initial state.\nare you sure?"
  let flag = confirm(message);
  if (flag) {
    import("./modules/init.mjs").then(module => {
      chrome.storage.local.clear();
      chrome.storage.local.set(module.kInitData);
      location.reload();
    });
  }
});

document.getElementById("debug").addEventListener("click", async () => {
  let s = await StorageInfo();
  let g = GPUInfo();
  kInit.renderer.message_elm.value = g + "\n\n" + s;
});