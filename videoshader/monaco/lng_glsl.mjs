export const kConfig = {
  comments: {
    lineComment: '//',
    blockComment: ['/*', '*/']
  },
  brackets: [
      ['{', '}'],
      ['[', ']'],
      ['(', ')']
  ],
  autoClosingPairs: [
      { open: '[', close: ']' },
      { open: '{', close: '}' },
      { open: '(', close: ')' }
  ],
  surroundingPairs: [
      { open: '{', close: '}' },
      { open: '[', close: ']' },
      { open: '(', close: ')' }
  ]
}

export const kLanguage = {
  keywords: [
    'const',                'uniform',           'layout',
    'centroid',             'flat',              'smooth',
    'in',                   'out',               'inout',
    'true',                 'false',             'invariant',
    'lowp',                 'mediump',           'highp',
    'precision',            'struct'
  ],
  controls: [
    'break',                'continue',          'do',
    'for',                  'while',             'switch',
    'case',                 'default',           'if',
    'else',                 'discard',           'return'
  ],
  typeKeywords: [
    'void',                 'bool',              'int',
    'uint',                 'float',             'vec2',
    'vec3',                 'vec4',              'bvec2',
    'bvec3',                'bvec4',             'ivec2',
    'ivec3',                'ivec4',             'uvec2',
    'uvec3',                'uvec4',             'mat2',
    'mat3',                 'mat4',              'mat2x2',
    'mat2x3',               'mat2x4',            'mat3x2',
    'mat3x3',               'mat3x4',            'mat4x2',
    'mat4x3',               'mat4x4',            'sampler2D',
    'sampler3D',            'samplerCube',       'samplerCubeShadow',
    'sampler2DShadow',      'sampler2DArray',    'sampler2DArrayShadow',
    'isampler2D',           'isampler3D',        'isamplerCube',
    'isampler2DArray',      'usampler2D',        'usampler3D',
    'usamplerCube',         'usampler2DArray'
  ],
  builtinVariables: [
    'gl_VertexID',          'gl_InstanceID',     'gl_Position',
    'gl_PointSize',         'gl_FragCoord',      'gl_FrontFacing',
    'gl_FragDepth',         'gl_PointCoord'
  ],
  builtinConstants: [
    'gl_MaxVertexAttribs',              'gl_MaxVertexUniformVectors',
    'gl_MaxVertexOutputVectors',        'gl_MaxFragmentInputVectors',
    'gl_MaxVertexTextureImageUnits',    'gl_MaxCombinedTextureImageUnits',
    'gl_MaxTextureImageUnits',          'gl_MaxFragmentUniformVectors',
    'gl_MaxDrawBuffers',                'gl_MinProgramTexelOffset',
    'gl_MaxProgramTexelOffset'
  ],
  builtinFunctions: [
    'radians',               'degrees',              'sin',
    'cos',                   'tan',                  'asin',
    'acos',                  'atan',                 'sinh',
    'cosh',                  'tanh',                 'asinh',
    'acosh',                 'atanh',                'pow',
    'exp',                   'log',                  'exp2',
    'log2',                  'sqrt',                 'inversesqrt',
    'abs',                   'sign',                 'floor',
    'trunc',                 'round',                'roundEven',
    'ceil',                  'fract',                'mod',
    'modf',                  'min',                  'max',
    'clamp',                 'mix',                  'step',
    'smoothstep',            'isnan',                'isinf',
    'floatBitsToInt',        'intBitsToFloat',       'packSnorm2x16',
    'unpackSnorm2x16',       'packUnorm2x16',        'unpackUnorm2x16',
    'packHalf2x16',          'unpackHalf2x16',       'length',
    'distance',              'dot',                  'cross',
    'normalize',             'faceforward',          'reflect',
    'refract',               'matrixCompMult',       'outerProduct',
    'transpose',             'determinant',          'inverse',
    'lessThan',              'lessThanEqual',        'greaterThan',
    'greaterThanEqual',      'equal',                'notEqual',
    'any',                   'all',                  'not',
    'textureSize',           'texture',              'textureProj',
    'textureLod',            'textureOffset',        'texelFetch',
    'texelFetchOffset',      'textureProjOffset',    'textureLodOffset',
    'textureProjLod',        'textureProjLodOffset', 'textureGrad',
    'textureGradOffset',     'textureProjGrad',      'textureProjGradOffset',
    'dFdx',                  'dFdy',                 'fwidth'
  ],
  operators: [
    '=', '>', '<', '!', '~', '?', ':', '==', '<=', '>=', '!=',
    '&&', '||', '++', '--', '+', '-', '*', '/', '&', '|', '^', '%',
    '<<', '>>', '>>>', '+=', '-=', '*=', '/=', '&=', '|=', '^=',
    '%=', '<<=', '>>=', '>>>='
  ],
  symbols:  /[=><!~?:&|+\-*\/\^%]+/,
  escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,
  tokenizer: {
    root: [
      [/[a-zA-Z_$][\w$]*(?=\()/, {
        cases: {
          '@typeKeywords': 'type',
          '@keywords'    : 'keyword',
          '@controls'    : 'control',
          '@default'     : 'function'
        }
      }],
      [/[a-zA-Z_$][\w$]*/, {
        cases: {
          '@typeKeywords'    : 'type',
          '@keywords'        : 'keyword',
          '@controls'        : 'control',
          '@builtinFunctions': 'function',
          '@builtinConstants': 'constant',
          '@default'         : 'variable'
        }
      }],
      {include: '@whitespace'},
      [/[{}()\[\]]/, '@brackets'],
      [/[<>](?!@symbols)/, '@brackets'],
      [/@symbols/, {
        cases: {
          '@operators': 'operator',
          '@default'  : ''
        }
      }],
      [/\#.*/, 'special'],
      [/\d*\.\d+([eE][\-+]?\d+)?/, 'number.float'],
      [/0[xX][0-9a-fA-F]+/, 'number.hex'],
      [/\d+/, 'number'],
      [/[;,.]/, 'delimiter'],
      [/"([^"\\]|\\.)*$/, 'string.invalid'],
      [/"/, {
        token: 'string.quote',
        bracket: '@open',
        next: '@string'
      }],
      [/'[^\\']'/, 'string'],
      [/(')(@escapes)(')/, ['string', 'string.escape', 'string']],
      [/'/, 'string.invalid']
    ],
    comment: [
      [/[^\/*]+/, 'comment' ],
      [/\/\*/,    'comment', '@push' ],
      ["\\*/",    'comment', '@pop'  ],
      [/[\/*]/,   'comment' ]
    ],
    string: [
      [/[^\\"]+/,  'string'],
      [/@escapes/, 'string.escape'],
      [/\\./,      'string.escape.invalid'],
      [/"/,        { token: 'string.quote', bracket: '@close', next: '@pop' } ]
    ],
    whitespace: [
      [/[ \t\r\n]+/, 'white'],
      [/\/\*/,       'comment', '@comment' ],
      [/\/\/.*$/,    'comment'],
    ],
  }
}

export const kTheme = {
  base: 'vs-dark',
  inherit: true,
  rules: [
    {token: 'variable', foreground: '9CDCFE'},
    {token: 'constant', foreground: '4FC1FF'},
    {token: 'function', foreground: 'DCDCAA'},
    {token: 'keyword',  foreground: '569CD6'},
    {token: 'type',     foreground: '569CD6'},
    {token: 'control',  foreground: 'C586C0'},
    {token: 'special',  foreground: 'C586C0'}
  ]
}

export const kCompletion = {
  provideCompletionItems: (model, position) =>
  {
    const kSuggestionData = [
      {
        "label": "abs",
        "kind": 1,
        "insertText": "abs",
        "detail": "return the absolute value of the parameter",
        "documentation": "Declaration\ngenType abs(genType x)\ngenIType abs(genIType x)\n\nParameters\nx - Specify the value of which to return the absolute.\n\nDescription\nabs returns x if x ≥ 0, otherwise returns -x.\n"
      },
      {
        "label": "acos",
        "kind": 1,
        "insertText": "acos",
        "detail": "return the arccosine of the parameter",
        "documentation": "Declaration\ngenType acos(genType x)\n\nParameters\nx - Specify the value whose arccosine to return.\n\nDescription\natan returns the angle whose trigonometric cosine is x. The range of values returned by acos is 0. The result is undefined if x1.\n"
      },
      {
        "label": "acosh",
        "kind": 1,
        "insertText": "acosh",
        "detail": "return the arc hyperbolic cosine of the parameter",
        "documentation": "Declaration\ngenType acosh(genType x)\n\nParameters\nx - Specify the value whose arc hyperbolic cosine to return.\n\nDescription\nacosh returns the arc hyperbolic cosine of x; the non-negative inverse of cosh. Results are undefined if x1.\n"
      },
      {
        "label": "all",
        "kind": 1,
        "insertText": "all",
        "detail": "check whether all elements of a boolean vector are true",
        "documentation": "Declaration\nbool all(bvec x)\n\nParameters\nx - Specifies the vector to be tested for truth.\n\nDescription\nall returns true if all elements of x are true and false otherwise. It is functionally equivalent to:\nbool all(bvec x) // bvec can be bvec2, bvec3 or bvec4 { bool result = true; int i; for (i = 0; i < x.length(); ++i) { result &= x[i]; } return result; }\n"
      },
      {
        "label": "any",
        "kind": 1,
        "insertText": "any",
        "detail": "check whether any element of a boolean vector is true",
        "documentation": "Declaration\nbool any(bvec x)\n\nParameters\nx - Specifies the vector to be tested for truth.\n\nDescription\nany returns true if any element of x is true and false otherwise. It is functionally equivalent to:\nbool any(bvec x) // bvec can be bvec2, bvec3 or bvec4 { bool result = false; int i; for (i = 0; i < x.length(); ++i) { result |= x[i]; } return result; }\n"
      },
      {
        "label": "asin",
        "kind": 1,
        "insertText": "asin",
        "detail": "return the arcsine of the parameter",
        "documentation": "Declaration\ngenType asin(genType x)\n\nParameters\nx - Specify the value whose arcsine to return.\n\nDescription\natan returns the angle whose trigonometric sine is x. The range of values returned by asin is −22. The result is undefined if x1.\n"
      },
      {
        "label": "asinh",
        "kind": 1,
        "insertText": "asinh",
        "detail": "return the arc hyperbolic sine of the parameter",
        "documentation": "Declaration\ngenType asinh(genType x)\n\nParameters\nx - Specify the value whose arc hyperbolic sine to return.\n\nDescription\nasinh returns the arc hyperbolic sine of x; the inverse of sinh.\n"
      },
      {
        "label": "atan",
        "kind": 1,
        "insertText": "atan",
        "detail": "return the arc-tangent of the parameters",
        "documentation": "Declaration\ngenType atan(genType y,  genType x)\ngenType atan(genType y_over_x)\n\nParameters\ny - Specify the numerator of the fraction whose arctangent to return.\nx - Specify the denominator of the fraction whose arctangent to return.\ny_over_x - Specify the fraction whose arctangent to return.\n\nDescription\natan returns the angle whose trigonometric arctangent is yx or y_over_x, depending on which overload is invoked. In the first overload, the signs of y and x are used to determine the quadrant that the angle lies in. The values returned by atan in this case are in the range −. Results are undefined if x is zero.\nFor the second overload, atan returns the angle whose tangent is y_over_x. Values returned in this case are in the range −22.\n"
      },
      {
        "label": "atanh",
        "kind": 1,
        "insertText": "atanh",
        "detail": "return the arc hyperbolic tangent of the parameter",
        "documentation": "Declaration\ngenType atanh(genType x)\n\nParameters\nx - Specify the value whose arc hyperbolic tangent to return.\n\nDescription\natanh returns the arc hyperbolic tangent of x; the inverse of tanh. Results are undefined if x1.\n"
      },
      {
        "label": "ceil",
        "kind": 1,
        "insertText": "ceil",
        "detail": "find the nearest integer that is greater than or equal to the parameter",
        "documentation": "Declaration\ngenType ceil(genType x)\n\nParameters\nx - Specify the value to evaluate.\n\nDescription\nceil returns a value equal to the nearest integer that is greater than or equal to x.\n"
      },
      {
        "label": "clamp",
        "kind": 1,
        "insertText": "clamp",
        "detail": "constrain a value to lie between two further values",
        "documentation": "Declaration\ngenType clamp(genType x,  genType minVal,  genType maxVal)\ngenType clamp(genType x,  float minVal,  float maxVal)\ngenIType clamp(genIType x,  genIType minVal,  genIType maxVal)\ngenIType clamp(genIType x,  int minVal,  int maxVal)\ngenUType clamp(genUType x,  genUType minVal,  genUType maxVal)\ngenUType clamp(genUType x,  uint minVal,  uint maxVal)\n\nParameters\nx - Specify the value to constrain.\nminVal - Specify the lower end of the range into which to constrain x.\nmaxVal - Specify the upper end of the range into which to constrain x.\n\nDescription\nclamp returns the value of x constrained to the range minVal to maxVal. The returned value is computed as min(max(x, minVal), maxVal). The result is undefined if minVal ≥ maxVal.\n"
      },
      {
        "label": "cos",
        "kind": 1,
        "insertText": "cos",
        "detail": "return the cosine of the parameter",
        "documentation": "Declaration\ngenType cos(genType angle)\n\nParameters\nangle - Specify the quantity, in radians, of which to return the cosine.\n\nDescription\ncos returns the trigonometric cosine of angle.\n"
      },
      {
        "label": "cosh",
        "kind": 1,
        "insertText": "cosh",
        "detail": "return the hyperbolic cosine of the parameter",
        "documentation": "Declaration\ngenType cosh(genType x)\n\nParameters\nx - Specify the value whose hyperbolic cosine to return.\n\nDescription\ncosh returns the hyperbolic cosine of x. The hyperbolic cosine of x is computed as ex+e−x2.\n"
      },
      {
        "label": "cross",
        "kind": 1,
        "insertText": "cross",
        "detail": "calculate the cross product of two vectors",
        "documentation": "Declaration\nvec3 cross(vec3 x,  vec3 y)\n\nParameters\nx - Specifies the first of two vectors\ny - Specifies the second of two vectors\n\nDescription\ncross returns the cross product of two vectors, x and y. i.e.,\nx[1]⋅y[2]−y[1]⋅x[2]x[2]⋅y[0]−y[2]⋅x[0]x[0]⋅y[1]−y[1]⋅x[1]\n"
      },
      {
        "label": "dFdx",
        "kind": 1,
        "insertText": "dFdx",
        "detail": "return the partial derivative of an argument with respect to x or y",
        "documentation": "Declaration\ngenType dFdx(genType p)\n\nParameters\np - Specifies the expression of which to take the partial derivative.\n\nDescription\nAvailable only in the fragment shader, dFdx and dFdy return the partial derivative of expression p in x and y, respectively. Deviatives are calculated using local differencing. Expressions that imply higher order derivatives such as dFdx(dFdx(n)) have undefined results, as do mixed-order derivatives such as dFdx(dFdy(n)). It is assumed that the expression p is continuous and therefore, expressions evaluated via non-uniform control flow may be undefined.\n"
      },
      {
        "label": "dFdy",
        "kind": 1,
        "insertText": "dFdy",
        "detail": "return the partial derivative of an argument with respect to x or y",
        "documentation": "Declaration\ngenType dFdy(genType p)\n\nParameters\np - Specifies the expression of which to take the partial derivative.\n\nDescription\nAvailable only in the fragment shader, dFdx and dFdy return the partial derivative of expression p in x and y, respectively. Deviatives are calculated using local differencing. Expressions that imply higher order derivatives such as dFdx(dFdx(n)) have undefined results, as do mixed-order derivatives such as dFdx(dFdy(n)). It is assumed that the expression p is continuous and therefore, expressions evaluated via non-uniform control flow may be undefined.\n"
      },
      {
        "label": "degrees",
        "kind": 1,
        "insertText": "degrees",
        "detail": "convert a quantity in radians to degrees",
        "documentation": "Declaration\ngenType degrees(genType radians)\n\nParameters\nradians - Specify the quantity, in radians, to be converted to degrees.\n\nDescription\ndegrees converts a quantity, specified in radians into degrees. That is, the return value is 180⋅radians.\n"
      },
      {
        "label": "determinant",
        "kind": 1,
        "insertText": "determinant",
        "detail": "calculate the determinant of a matrix",
        "documentation": "Declaration\nfloat determinant(mat2 m)\nfloat determinant(mat3 m)\nfloat determinant(mat4 m)\n\nParameters\nm - Specifies the matrix of which to take the determinant.\n\nDescription\ndeterminant returns the determinant of the matrix m.\n"
      },
      {
        "label": "distance",
        "kind": 1,
        "insertText": "distance",
        "detail": "calculate the distance between two points",
        "documentation": "Declaration\nfloat distance(genType p0,  genType p1)\n\nParameters\np0 - Specifies the first of two points\np1 - Specifies the second of two points\n\nDescription\ndistance returns the distance between the two points p0 and p1. i.e., length(p0 - p1);\n"
      },
      {
        "label": "dot",
        "kind": 1,
        "insertText": "dot",
        "detail": "calculate the dot product of two vectors",
        "documentation": "Declaration\nfloat dot(genType x,  genType y)\n\nParameters\nx - Specifies the first of two vectors\ny - Specifies the second of two vectors\n\nDescription\ndot returns the dot product of two vectors, x and y. i.e., x[0]⋅y[0]+x[1]⋅y[1]+...\n"
      },
      {
        "label": "equal",
        "kind": 1,
        "insertText": "equal",
        "detail": "perform a component-wise equal-to comparison of two vectors",
        "documentation": "Declaration\nbvec equal(vec x,  vec y)\nbvec equal(bvec x,  bvec y)\nbvec equal(ivec x,  ivec y)\nbvec equal(uvec x,  uvec y)\n\nParameters\nx - Specifies the first vector to be used in the comparison operation.\ny - Specifies the second vector to be used in the comparison operation.\n\nDescription\nequal returns a boolean vector in which each element i is computed as x[i] == y[i].\n"
      },
      {
        "label": "exp",
        "kind": 1,
        "insertText": "exp",
        "detail": "return the natural exponentiation of the parameter",
        "documentation": "Declaration\ngenType exp(genType x)\n\nParameters\nx - Specify the value to exponentiate.\n\nDescription\nexp returns the natural exponentiation of x. i.e., ex.\n"
      },
      {
        "label": "exp2",
        "kind": 1,
        "insertText": "exp2",
        "detail": "return 2 raised to the power of the parameter",
        "documentation": "Declaration\ngenType exp2(genType x)\n\nParameters\nx - Specify the value of the power to which 2 will be raised.\n\nDescription\nexp2 returns 2 raised to the power of x. i.e., 2x.\n"
      },
      {
        "label": "faceforward",
        "kind": 1,
        "insertText": "faceforward",
        "detail": "return a vector pointing in the same direction as another",
        "documentation": "Declaration\ngenType faceforward(genType N,  genType I,  genType Nref)\n\nParameters\nN - Specifies the vector to orient.\nI - Specifies the incident vector.\nNref - Specifies the reference vector.\n\nDescription\nfaceforward orients a vector to point away from a surface as defined by its normal. If dot(Nref, I) < 0faceforward returns N, otherwise it returns -N.\n"
      },
      {
        "label": "floatBitsToInt",
        "kind": 1,
        "insertText": "floatBitsToInt",
        "detail": "produce the encoding of a floating point value as an integer",
        "documentation": "Declaration\ngenIType floatBitsToInt(genType x)\ngenUType floatBitsToUint(genType x)\n\nParameters\nx - Specifies the value whose floating point encoding to return.\n\nDescription\nfloatBitsToInt and floatBitsToUint return the encoding of their floating-point parameters as highp int or uint, respectively. The floating-point bit-level representation is preserved. For mediump and lowp, the value is first converted to highp floating point and the encoding of that value is returned.\n"
      },
      {
        "label": "floor",
        "kind": 1,
        "insertText": "floor",
        "detail": "find the nearest integer less than or equal to the parameter",
        "documentation": "Declaration\ngenType floor(genType x)\n\nParameters\nx - Specify the value to evaluate.\n\nDescription\nfloor returns a value equal to the nearest integer that is less than or equal to x.\n"
      },
      {
        "label": "fract",
        "kind": 1,
        "insertText": "fract",
        "detail": "compute the fractional part of the argument",
        "documentation": "Declaration\ngenType fract(genType x)\n\nParameters\nx - Specify the value to evaluate.\n\nDescription\nfract returns the fractional part of x. This is calculated as x - floor(x).\n"
      },
      {
        "label": "fwidth",
        "kind": 1,
        "insertText": "fwidth",
        "detail": "return the sum of the absolute derivatives in x and y",
        "documentation": "Declaration\ngenType fwidth(genType p)\n\nParameters\np - Specifies the expression of which to take the partial derivative.\n\nDescription\nAvailable only in the fragment shader, fwidth returns the sum of the absolute derivatives in x and y using local differencing for the input argument p. It is equivalent to abs(dFdx(p)) + abs(dFdy(p)).\n"
      },
      {
        "label": "gl_FragCoord",
        "kind": 4,
        "insertText": "gl_FragCoord",
        "detail": "contains the window-relative coordinates of the current fragment",
        "documentation": "Description\nAvailable only in the fragment language, gl_FragCoord is an input variable that contains the window relative coordinate (x, y, z, 1/w) values for the fragment. If multi-sampling, this value can be for any location within the pixel, or one of the fragment samples. This value is the result of fixed functionality that interpolates primitives after vertex processing to generate fragments. The z component is the depth value that would be used for the fragment's depth if no shader contained any writes to gl_FragDepth.\n"
      },
      {
        "label": "gl_FragDepth",
        "kind": 4,
        "insertText": "gl_FragDepth",
        "detail": "establishes a depth value for the current fragment",
        "documentation": "Description\nAvailable only in the fragment language, gl_FragDepth is an output variable that is used to establish the depth value for the current fragment. If depth buffering is enabled and no shader writes to gl_FragDepth, then the fixed varname value for depth will be used (this value is contained in the z component of gl_FragCoord) otherwise, the value written to gl_FragDepth is used. If a shader statically assigns to gl_FragDepth, then the value of the fragment's depth may be undefined for executions of the shader that take that path. That is, if the set of linked fragment shaders statically contain a write to gl_FragDepth, then it is responsible for always writing it.\n"
      },
      {
        "label": "gl_FrontFacing",
        "kind": 4,
        "insertText": "gl_FrontFacing",
        "detail": "indicates whether a primitive is front or back facing",
        "documentation": "Description\nAvailable only in the fragment language, gl_FrontFacing is an input variable whose value is true if the fragment belongs to a front-facing primitive and false otherwise. The determination of whether a triangle primitive is front-facing is made by examining the sign of the area of the triangle, including a possible reversal of this sign as controlled by glFrontFace. One way to compute this area is:\na=12∑j=0n-1xwiywi+1-xwi+1ywi\nwhere xwi and ywi are the x and y window coordinates of the ith vertex of the n-vertex polygon.\n"
      },
      {
        "label": "gl_InstanceID",
        "kind": 4,
        "insertText": "gl_InstanceID",
        "detail": "contains the index of the current primitive in an instanced draw command",
        "documentation": "Description\ngl_InstanceID is a vertex language input variable that holds the integer index of the current primitive in an instanced draw command such as glDrawArraysInstanced. If the current primitive does not originate from an instanced draw command, the value of gl_InstanceID is zero.\n"
      },
      {
        "label": "gl_PointCoord",
        "kind": 4,
        "insertText": "gl_PointCoord",
        "detail": "contains the coordinate of a fragment within a point",
        "documentation": "Description\ngl_PointCoord is a fragment language input variable that contains the two-dimensional coordinates indicating where within a point primitive the current fragment is located. If the current primitive is not a point, then values read from gl_PointCoord are undefined.\ngl_PointCoord.s ranges from 0.0 to 1.0 across the point horizontally from left to right. gl_PointCoord.t varies from 0.0 to 1.0 vertically from top to bottom.\n"
      },
      {
        "label": "gl_PointSize",
        "kind": 4,
        "insertText": "gl_PointSize",
        "detail": "contains size of rasterized points, in pixels",
        "documentation": "Description\nThe variable gl_PointSize is intended for a vertex shader to write the size of the point to be rasterized. It is measured in pixels. If gl_PointSize is not written to, its value is undefined in subsequent pipeline stages.\n"
      },
      {
        "label": "gl_Position",
        "kind": 4,
        "insertText": "gl_Position",
        "detail": "contains the position of the current vertex",
        "documentation": "Description\nThe variable gl_Position is intended for writing the homogeneous vertex position. It can be written at any time during vertexshader execution. This value will be used by primitive assembly, clipping, culling, and other fixed functionality operations, if present, that operate on primitives after vertex processing has occurred. Its value is undefined after the vertex processing stage if the vertex shader executable does not write gl_Position.\n"
      },
      {
        "label": "gl_VertexID",
        "kind": 4,
        "insertText": "gl_VertexID",
        "detail": "contains the index of the current vertex",
        "documentation": "Description\ngl_VertexID is a vertex language input variable that holds an integer index for the vertex. The index is impliclty generated by glDrawArrays and other commands that do not reference the content of the GL_ELEMENT_ARRAY_BUFFER, or explicitly generated from the content of the GL_ELEMENT_ARRAY_BUFFER by commands such as glDrawElements.\n"
      },
      {
        "label": "greaterThan",
        "kind": 1,
        "insertText": "greaterThan",
        "detail": "perform a component-wise greater-than comparison of two vectors",
        "documentation": "Declaration\nbvec greaterThan(vec x,  vec y)\nbvec greaterThan(ivec x,  ivec y)\nbvec greaterThan(uvec x,  uvec y)\n\nParameters\nx - Specifies the first vector to be used in the comparison operation.\ny - Specifies the second vector to be used in the comparison operation.\n\nDescription\ngreaterThan returns a boolean vector in which each element i is computed as x[i] > y[i].\n"
      },
      {
        "label": "greaterThanEqual",
        "kind": 1,
        "insertText": "greaterThanEqual",
        "detail": "perform a component-wise greater-than-or-equal comparison of two vectors",
        "documentation": "Declaration\nbvec greaterThanEqual(vec x,  vec y)\nbvec greaterThanEqual(ivec x,  ivec y)\nbvec greaterThanEqual(uvec x,  uvec y)\n\nParameters\nx - Specifies the first vector to be used in the comparison operation.\ny - Specifies the second vector to be used in the comparison operation.\n\nDescription\ngreaterThanEqual returns a boolean vector in which each element i is computed as x[i] ≥ y[i].\n"
      },
      {
        "label": "intBitsToFloat",
        "kind": 1,
        "insertText": "intBitsToFloat",
        "detail": "produce a floating point using an encoding supplied as an integer",
        "documentation": "Declaration\ngenType intBitsToFloat(genIType x)\n\nParameters\nx - Specifies the bit encoding to return as a floating point value.\n\nDescription\nintBitsToFloat and uintBitsToFloat return the encoding passed in parameter x as a highp floating-point value. If the encoding of a NaN is passed in x, it will not signal and the resulting value will be undefined. If the encoding of a floating point infinity is passed in parameter x, the resulting floating-point value is the corresponding (positive or negative) floating point infinity. For lowp and mediump, the value is first converted to the corresponding signed or unsigned highp integer and then reinterpreted as a highp floating point value as before.\n"
      },
      {
        "label": "uintBitsToFloat",
        "kind": 1,
        "insertText": "uintBitsToFloat",
        "detail": "produce a floating point using an encoding supplied as an integer",
        "documentation": "Declaration\ngenType uintBitsToFloat(genUType x)\n\nParameters\nx - Specifies the bit encoding to return as a floating point value.\n\nDescription\nintBitsToFloat and uintBitsToFloat return the encoding passed in parameter x as a highp floating-point value. If the encoding of a NaN is passed in x, it will not signal and the resulting value will be undefined. If the encoding of a floating point infinity is passed in parameter x, the resulting floating-point value is the corresponding (positive or negative) floating point infinity. For lowp and mediump, the value is first converted to the corresponding signed or unsigned highp integer and then reinterpreted as a highp floating point value as before.\n"
      },
      {
        "label": "inverse",
        "kind": 1,
        "insertText": "inverse",
        "detail": "calculate the inverse of a matrix",
        "documentation": "Declaration\nmat2 inverse(mat2 m)\nmat3 inverse(mat3 m)\nmat4 inverse(mat4 m)\n\nParameters\nm - Specifies the matrix of which to take the inverse.\n\nDescription\ninverse returns the inverse of the matrix m. The values in the returned matrix are undefined if m is singular or poorly-conditioned (nearly singular).\n"
      },
      {
        "label": "inversesqrt",
        "kind": 1,
        "insertText": "inversesqrt",
        "detail": "return the inverse of the square root of the parameter",
        "documentation": "Declaration\ngenType inversesqrt(genType x)\n\nParameters\nx - Specify the value of which to take the inverse of the square root.\n\nDescription\ninversesqrt returns the inverse of the square root of x. i.e., the value 1x. Results are undefined if x0.\n"
      },
      {
        "label": "isinf",
        "kind": 1,
        "insertText": "isinf",
        "detail": "determine whether the parameter is positive or negative infinity",
        "documentation": "Declaration\ngenBType isinf(genType x)\n\nParameters\nx - Specifies the value to test for infinity.\n\nDescription\nFor each element element i of the result, isinf returns true if x[i] is posititve or negative floating point infinity and false otherwise.\n"
      },
      {
        "label": "isnan",
        "kind": 1,
        "insertText": "isnan",
        "detail": "determine whether the parameter is a number",
        "documentation": "Declaration\ngenBType isnan(genType x)\n\nParameters\nx - Specifies the value to test for NaN.\n\nDescription\nFor each element element i of the result, isnan returns true if x[i] is posititve or negative floating point NaN (Not a Number) and false otherwise. NaNs may not be supported by the implementation, in which case isnan returns false.\n"
      },
      {
        "label": "length",
        "kind": 1,
        "insertText": "length",
        "detail": "calculate the length of a vector",
        "documentation": "Declaration\nfloat length(genType x)\n\nParameters\nx - Specifies a vector of which to calculate the length.\n\nDescription\nlength returns the length of the vector. i.e., x[0]2+x[1]2+...\n"
      },
      {
        "label": "lessThan",
        "kind": 1,
        "insertText": "lessThan",
        "detail": "perform a component-wise less-than comparison of two vectors",
        "documentation": "Declaration\nbvec lessThan(vec x,  vec y)\nbvec lessThan(ivec x,  ivec y)\nbvec lessThan(uvec x,  uvec y)\n\nParameters\nx - Specifies the first vector to be used in the comparison operation.\ny - Specifies the second vector to be used in the comparison operation.\n\nDescription\nlessThan returns a boolean vector in which each element i is computed as x[i] < y[i].\n"
      },
      {
        "label": "lessThanEqual",
        "kind": 1,
        "insertText": "lessThanEqual",
        "detail": "perform a component-wise less-than-or-equal comparison of two vectors",
        "documentation": "Declaration\nbvec lessThanEqual(vec x,  vec y)\nbvec lessThanEqual(ivec x,  ivec y)\nbvec lessThanEqual(uvec x,  uvec y)\n\nParameters\nx - Specifies the first vector to be used in the comparison operation.\ny - Specifies the second vector to be used in the comparison operation.\n\nDescription\nlessThanEqual returns a boolean vector in which each element i is computed as x[i] ≤ y[i].\n"
      },
      {
        "label": "log",
        "kind": 1,
        "insertText": "log",
        "detail": "return the natural logarithm of the parameter",
        "documentation": "Declaration\ngenType log(genType x)\n\nParameters\nx - Specify the value of which to take the natural logarithm.\n\nDescription\nlog returns the natural logarithm of x. i.e., the value y which satisfies xey. Results are undefined if x ≤ 0.\n"
      },
      {
        "label": "log2",
        "kind": 1,
        "insertText": "log2",
        "detail": "return the base 2 logarithm of the parameter",
        "documentation": "Declaration\ngenType log2(genType x)\n\nParameters\nx - Specify the value of which to take the base 2 logarithm.\n\nDescription\nlog2 returns the base 2 logarithm of x. i.e., the value y which satisfies x2y. Results are undefined if x ≤ 0.\n"
      },
      {
        "label": "matrixCompMult",
        "kind": 1,
        "insertText": "matrixCompMult",
        "detail": "perform a component-wise multiplication of two matrices",
        "documentation": "Declaration\nmat matrixCompMult(mat x,  mat y)\n\nParameters\nx - Specifies the first matrix multiplicand.\ny - Specifies the second matrix multiplicand.\n\nDescription\nmatrixCompMult performs a component-wise multiplication of two matrices, yielding a result matrix where each component, result[i][j] is computed as the scalar product of x[i][j] and y[i][j].\n"
      },
      {
        "label": "max",
        "kind": 1,
        "insertText": "max",
        "detail": "return the greater of two values",
        "documentation": "Declaration\ngenType max(genType x,  genType y)\ngenType max(genType x,  float y)\ngenIType max(genIType x,  genIType y)\ngenIType max(genIType x,  int y)\ngenUType max(genUType x,  genUType y)\ngenUType max(genUType x,  uint y)\n\nParameters\nx - Specify the first value to compare.\ny - Specify the second value to compare.\n\nDescription\nmax returns the maximum of the two parameters. It returns y if y is greater than x, otherwise it returns x.\n"
      },
      {
        "label": "min",
        "kind": 1,
        "insertText": "min",
        "detail": "return the lesser of two values",
        "documentation": "Declaration\ngenType min(genType x,  genType y)\ngenType min(genType x,  float y)\ngenIType min(genIType x,  genIType y)\ngenIType min(genIType x,  int y)\ngenUType min(genUType x,  genUType y)\ngenUType min(genUType x,  uint y)\n\nParameters\nx - Specify the first value to compare.\ny - Specify the second value to compare.\n\nDescription\nmin returns the minimum of the two parameters. It returns y if y is less than x, otherwise it returns x.\n"
      },
      {
        "label": "mix",
        "kind": 1,
        "insertText": "mix",
        "detail": "linearly interpolate between two values",
        "documentation": "Declaration\ngenType mix(genType x,  genType y,  genType a)\ngenType mix(genType x,  genType y,  float a)\ngenType mix(genType x,  genType y,  genBType a)\n\nParameters\nx - Specify the start of the range in which to interpolate.\ny - Specify the end of the range in which to interpolate.\na - Specify the value to use to interpolate between x and y.\n\nDescription\nmix performs a linear interpolation between x and y using a to weight between them. The return value is computed as follows: x⋅(1−a)+y⋅a.\nFor the variants of mix where a is genBType, elements for which a[i] is false, the result for that element is taken from x, and where a[i] is true, it will be taken from y. Components of x and y that are not selected are allowed to be invalid floating point values and will have no effect on the results.\n"
      },
      {
        "label": "mod",
        "kind": 1,
        "insertText": "mod",
        "detail": "compute value of one parameter modulo another",
        "documentation": "Declaration\ngenType mod(genType x,  float y)\ngenType mod(genType x,  genType y)\n\nParameters\nx - Specify the value to evaluate.\n\nDescription\nmod returns the value of x modulo y. This is computed as x - y * floor(x/y).\n"
      },
      {
        "label": "modf",
        "kind": 1,
        "insertText": "modf",
        "detail": "separate a value into its integer and fractional components",
        "documentation": "Declaration\ngenType modf(genType x,  out genType i)\n\nParameters\nx - Specify the value to separate.\nout i - A variable that receives the integer part of the argument.\n\nDescription\nmodf separates a floating point value x into its integer and fractional parts. The fractional part of the number is returned from the function and the integer part (as a floating point quantity) is returned in the output parameter i.\n"
      },
      {
        "label": "normalize",
        "kind": 1,
        "insertText": "normalize",
        "detail": "calculate the normalize product of two vectors",
        "documentation": "Declaration\ngenType normalize(genType v)\n\nParameters\nv - Specifies the vector to normalize.\n\nDescription\nnormalize returns a vector with the same direction as its parameter, v, but with length 1.\n"
      },
      {
        "label": "not",
        "kind": 1,
        "insertText": "not",
        "detail": "logically invert a boolean vector",
        "documentation": "Declaration\nbvec not(bvec x)\n\nParameters\nx - Specifies the vector to be inverted.\n\nDescription\nnot logically inverts the boolean vector x. It returns a new boolean vector for which each element i is computed as !x[i].\n"
      },
      {
        "label": "notEqual",
        "kind": 1,
        "insertText": "notEqual",
        "detail": "perform a component-wise not-equal-to comparison of two vectors",
        "documentation": "Declaration\nbvec notEqual(vec x,  vec y)\nbvec notEqual(ivec x,  ivec y)\nbvec notEqual(bvec x,  bvec y)\nbvec notEqual(uvec x,  uvec y)\n\nParameters\nx - Specifies the first vector to be used in the comparison operation.\ny - Specifies the second vector to be used in the comparison operation.\n\nDescription\nnotEqual returns a boolean vector in which each element i is computed as x[i] != y[i].\n"
      },
      {
        "label": "outerProduct",
        "kind": 1,
        "insertText": "outerProduct",
        "detail": "calculate the outer product of a pair of vectors",
        "documentation": "Declaration\nmat2 outerProduct(vec2 c,  vec2 r)\nmat3 outerProduct(vec3 c,  vec3 r)\nmat4 outerProduct(vec4 c,  vec4 r)\nmat2x3 outerProduct(vec3 c,  vec2 r)\nmat3x2 outerProduct(vec2 c,  vec3 r)\nmat2x4 outerProduct(vec4 c,  vec2 r)\nmat4x2 outerProduct(vec2 c,  vec4 r)\nmat3x4 outerProduct(vec4 c,  vec3 r)\nmat4x3 outerProduct(vec3 c,  vec4 r)\n\nParameters\nc - Specifies the parameter to be treated as a column vector.\nr - Specifies the parameter to be treated as a row vector.\n\nDescription\nouterProduct treats the first parameter c as a column vector (matrix with one column) and the second parameter r as a row vector (matrix with one row) and does a linear algebraic matrix multiply c * r, yielding a matrix whose number of rows is the number of components in c and whose number of columns is the number of components in r.\n"
      },
      {
        "label": "packHalf2x16",
        "kind": 1,
        "insertText": "packHalf2x16",
        "detail": "convert two 32-bit floating-point quantities to 16-bit quantities and pack them into a single 32-bit integer",
        "documentation": "Declaration\nuint packHalf2x16(vec2 v)\n\nParameters\nv - Specify a vector of two 32-bit floating point values that are to be converted to 16-bit representation and packed into the result.\n\nDescription\npackHalf2x16 returns an unsigned integer obtained by converting the components of a two-component floating-point vector to the 16-bit floating-point representation found in the OpenGL ES Specification, and then packing these two 16-bit integers into a 32-bit unsigned integer. The first vector component specifies the 16 least-significant bits of the result; the second component specifies the 16 most-significant bits.\n"
      },
      {
        "label": "packUnorm2x16",
        "kind": 1,
        "insertText": "packUnorm2x16",
        "detail": "pack floating-point values into an unsigned integer",
        "documentation": "Declaration\nuint packUnorm2x16(vec2 v)\n\nParameters\nv - Specifies a vector of values to be packed into an unsigned integer.\n\nDescription\npackUnorm2x16 and packSnorm2x16 converts each component of the normalized floating-point value v into 16-bit integer values and then packs the results into a 32-bit unsigned intgeter.\nThe conversion for component c of v to fixed-point is performed as follows:\npackUnorm2x16: round(clamp(c, 0.0, 1.0) * 65535.0)packSnorm2x16: round(clamp(c, -1.0, 1.0) * 32767.0)\nThe first component of the vector will be written to the least significant bits of the output; the last component will be written to the most significant bits.\n"
      },
      {
        "label": "packSnorm2x16",
        "kind": 1,
        "insertText": "packSnorm2x16",
        "detail": "pack floating-point values into an unsigned integer",
        "documentation": "Declaration\nuint packSnorm2x16(vec2 v)\n\nParameters\nv - Specifies a vector of values to be packed into an unsigned integer.\n\nDescription\npackUnorm2x16 and packSnorm2x16 converts each component of the normalized floating-point value v into 16-bit integer values and then packs the results into a 32-bit unsigned intgeter.\nThe conversion for component c of v to fixed-point is performed as follows:\npackUnorm2x16: round(clamp(c, 0.0, 1.0) * 65535.0)packSnorm2x16: round(clamp(c, -1.0, 1.0) * 32767.0)\nThe first component of the vector will be written to the least significant bits of the output; the last component will be written to the most significant bits.\n"
      },
      {
        "label": "pow",
        "kind": 1,
        "insertText": "pow",
        "detail": "return the value of the first parameter raised to the power of the second",
        "documentation": "Declaration\ngenType pow(genType x,  genType y)\n\nParameters\nx - Specify the value to raise to the power y.\ny - Specify the power to which to raise x.\n\nDescription\npow returns the value of x raised to the y power. i.e., xy. Results are undefined if x < 0 or if x == 0 and y ≤ 0.\n"
      },
      {
        "label": "radians",
        "kind": 1,
        "insertText": "radians",
        "detail": "convert a quantity in degrees to radians",
        "documentation": "Declaration\ngenType radians(genType degrees)\n\nParameters\ndegrees - Specify the quantity, in degrees, to be converted to radians.\n\nDescription\nradians converts a quantity, specified in degrees into radians. That is, the return value is ⋅degrees180.\n"
      },
      {
        "label": "reflect",
        "kind": 1,
        "insertText": "reflect",
        "detail": "calculate the reflection direction for an incident vector",
        "documentation": "Declaration\ngenType reflect(genType I,  genType N)\n\nParameters\nI - Specifies the incident vector.\nN - Specifies the normal vector.\n\nDescription\nFor a given incident vector I and surface normal N reflect returns the reflection direction calculated as I - 2.0 * dot(N, I) * N.\nN should be normalized in order to achieve the desired result.\n"
      },
      {
        "label": "refract",
        "kind": 1,
        "insertText": "refract",
        "detail": "calculate the refraction direction for an incident vector",
        "documentation": "Declaration\ngenType refract(genType I,  genType N,  float eta)\n\nParameters\nI - Specifies the incident vector.\nN - Specifies the normal vector.\neta - Specifies the ratio of indices of refraction.\n\nDescription\nFor a given incident vector I, surface normal N and ratio of indices of refraction, eta, refract returns the refraction vector, R.\nR is calculated as:\nk = 1.0 - eta * eta * (1.0 - dot(N, I) * dot(N, I)); if (k < 0.0) R = genType(0.0); // or genDType(0.0) else R = eta * I - (eta * dot(N, I) + sqrt(k)) * N;\nThe input parameters I and N should be normalized in order to achieve the desired result.\n"
      },
      {
        "label": "round",
        "kind": 1,
        "insertText": "round",
        "detail": "find the nearest integer to the parameter",
        "documentation": "Declaration\ngenType round(genType x)\n\nParameters\nx - Specify the value to evaluate.\n\nDescription\nround returns a value equal to the nearest integer to x. The fraction 0.5 will round in a direction chosen by the implementation, usually in the direction that is fastest. This includes the possibility that round(x) returns the same value as roundEven(x) for all values of x.\n"
      },
      {
        "label": "roundEven",
        "kind": 1,
        "insertText": "roundEven",
        "detail": "find the nearest even integer to the parameter",
        "documentation": "Declaration\ngenType roundEven(genType x)\n\nParameters\nx - Specify the value to evaluate.\n\nDescription\nroundEven returns a value equal to the nearest integer to x. The fractional part of 0.5 will round toward the nearest even integer. For example, both 3.5 and 4.5 will round to 4.0.\n"
      },
      {
        "label": "sign",
        "kind": 1,
        "insertText": "sign",
        "detail": "extract the sign of the parameter",
        "documentation": "Declaration\ngenType sign(genType x)\ngenIType sign(genIType x)\n\nParameters\nx - Specify the value from which to extract the sign.\n\nDescription\nsign returns -1.0 if x0.0, 0.0 if x0.0 and 1.0 if x0.0.\n"
      },
      {
        "label": "sin",
        "kind": 1,
        "insertText": "sin",
        "detail": "return the sine of the parameter",
        "documentation": "Declaration\ngenType sin(genType angle)\n\nParameters\nangle - Specify the quantity, in radians, of which to return the sine.\n\nDescription\nsin returns the trigonometric sine of angle.\n"
      },
      {
        "label": "sinh",
        "kind": 1,
        "insertText": "sinh",
        "detail": "return the hyperbolic sine of the parameter",
        "documentation": "Declaration\ngenType sinh(genType x)\n\nParameters\nx - Specify the value whose hyperbolic sine to return.\n\nDescription\nsinh returns the hyperbolic sine of x. The hyperbolic sine of x is computed as ex−e−x2.\n"
      },
      {
        "label": "smoothstep",
        "kind": 1,
        "insertText": "smoothstep",
        "detail": "perform Hermite interpolation between two values",
        "documentation": "Declaration\ngenType smoothstep(genType edge0,  genType edge1,  genType x)\ngenType smoothstep(float edge0,  float edge1,  genType x)\n\nParameters\nedge0 - Specifies the value of the lower edge of the Hermite function.\nedge1 - Specifies the value of the upper edge of the Hermite function.\nx - Specifies the source value for interpolation.\n\nDescription\nsmoothstep performs smooth Hermite interpolation between 0 and 1 when edge0 < x < edge1. This is useful in cases where a threshold function with a smooth transition is desired. smoothstep is equivalent to:\ngenType t; /* Or genDType t; */ t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0); return t * t * (3.0 - 2.0 * t);\nsmoothstep returns 0.0 if x ≤ edge0 and 1.0 if x ≥ edge1.\nResults are undefined if edge0 ≥ edge1.\n"
      },
      {
        "label": "sqrt",
        "kind": 1,
        "insertText": "sqrt",
        "detail": "return the square root of the parameter",
        "documentation": "Declaration\ngenType sqrt(genType x)\n\nParameters\nx - Specify the value of which to take the square root.\n\nDescription\nsqrt returns the square root of x. i.e., the value x. Results are undefined if x0.\n"
      },
      {
        "label": "step",
        "kind": 1,
        "insertText": "step",
        "detail": "generate a step function by comparing two values",
        "documentation": "Declaration\ngenType step(genType edge,  genType x)\ngenType step(float edge,  genType x)\n\nParameters\nedge - Specifies the location of the edge of the step function.\nx - Specify the value to be used to generate the step function.\n\nDescription\nstep generates a step function by comparing x to edge.\nFor element i of the return value, 0.0 is returned if x[i] < edge[i], and 1.0 is returned otherwise.\n"
      },
      {
        "label": "tan",
        "kind": 1,
        "insertText": "tan",
        "detail": "return the tangent of the parameter",
        "documentation": "Declaration\ngenType tan(genType angle)\n\nParameters\nangle - Specify the quantity, in radians, of which to return the tangent.\n\nDescription\ntan returns the trigonometric tangent of angle.\n"
      },
      {
        "label": "tanh",
        "kind": 1,
        "insertText": "tanh",
        "detail": "return the hyperbolic tangent of the parameter",
        "documentation": "Declaration\ngenType tanh(genType x)\n\nParameters\nx - Specify the value whose hyperbolic tangent to return.\n\nDescription\ntanh returns the hyperbolic tangent of x. The hyperbolic tangent of x is computed as sinh⁡xcosh⁡x.\n"
      },
      {
        "label": "texelFetch",
        "kind": 1,
        "insertText": "texelFetch",
        "detail": "perform a lookup of a single texel within a texture",
        "documentation": "Declaration\ngvec4 texelFetch(gsampler2D sampler,  ivec2 P,  int lod)\ngvec4 texelFetch(gsampler3D sampler,  ivec3 P,  int lod)\ngvec4 texelFetch(gsampler2DArray sampler,  ivec3 P,  int lod)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\nlod - If present, specifies the level-of-detail within the texture from which the texel will be fetched.\n\nDescription\ntexelFetch performs a lookup of a single texel from texture coordinate P in the texture bound to sampler. The array layer is specified in the last component of P for array forms. The lod parameter (if present) specifies the level-of-detail from which the texel will be fetched.\n"
      },
      {
        "label": "texelFetchOffset",
        "kind": 1,
        "insertText": "texelFetchOffset",
        "detail": "perform a lookup of a single texel within a texture with an offset",
        "documentation": "Declaration\ngvec4 texelFetchOffset(gsampler2D sampler,  ivec2 P,  int lod,  ivec2 offset)\ngvec4 texelFetchOffset(gsampler3D sampler,  ivec3 P,  int lod,  ivec3 offset)\ngvec4 texelFetchOffset(gsampler2DArray sampler,  ivec3 P,  int lod,  ivec2 offset)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\nlod - If present, specifies the level-of-detail within the texture from which the texel will be fetched.\noffset - Specifies offset, in texels that will be applied to P before looking up the texel.\n\nDescription\ntexelFetchOffset performs a lookup of a single texel from texture coordinate P in the texture bound to sampler. Before fetching the texel, the offset specified in offset is added to P. offset must be a constant expression. The array layer is specified in the last component of P for array forms. The lod parameter (if present) specifies the level-of-detail from which the texel will be fetched. The sample parameter specifies which sample within the texel will be returned when reading from a multi-sample texure.\n"
      },
      {
        "label": "texture",
        "kind": 1,
        "insertText": "texture",
        "detail": "retrieves texels from a texture",
        "documentation": "Declaration\ngvec4 texture(gsampler2D sampler,  vec2 P,  [float bias])\ngvec4 texture(gsampler3D sampler,  vec3 P,  [float bias])\ngvec4 texture(gsamplerCube sampler,  vec3 P,  [float bias])\nfloat texture(sampler2DShadow sampler,  vec3 P,  [float bias])\nfloat texture(samplerCubeShadow sampler,  vec4 P,  [float bias])\ngvec4 texture(gsampler2DArray sampler,  vec3 P,  [float bias])\nfloat texture(gsampler2DArrayShadow sampler,  vec4 P)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\nbias - Specifies an optional bias to be applied during level-of-detail computation.\n\nDescription\ntexture samples texels from the texture bound to sampler at texture coordinate P. An optional bias, specified in bias is included in the level-of-detail computation that is used to choose mipmap(s) from which to sample.\nFor shadow forms, when compare is present, it is used as Dsub and the array layer is specified in P.w. When compare is not present, the last component of P is used as Dsub and the array layer is specified in the second to last component of P.\nFor non-shadow variants, the array layer comes from the last component of P.\n"
      },
      {
        "label": "textureGrad",
        "kind": 1,
        "insertText": "textureGrad",
        "detail": "perform a texture lookup with explicit gradients",
        "documentation": "Declaration\ngvec4 textureGrad(gsampler2D sampler,  vec2 P,  vec2 dPdx,  vec2 dPdy)\ngvec4 textureGrad(gsampler3D sampler,  vec3 P,  vec3 dPdx,  vec3 dPdy)\ngvec4 textureGrad(gsamplerCube sampler,  vec3 P,  vec3 dPdx,  vec3 dPdy)\nfloat textureGrad(sampler2DShadow sampler,  vec3 P,  vec2 dPdx,  vec2 dPdy)\nfloat textureGrad(samplerCubeShadow sampler,  vec4 P,  vec3 dPdx,  vec3 dPdy)\ngvec4 textureGrad(gsampler2DArray sampler,  vec3 P,  vec2 dPdx,  vec2 dPdy)\nfloat textureGrad(gsampler2DArrayShadow sampler,  vec4 P,  vec2 dPdx,  vec2 dPdy)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\ndPdx - Specifies the partial derivative of P with respect to window x.\ndPdy - Specifies the partial derivative of P with respect to window y.\n\nDescription\ntextureGrad performs a texture lookup at coordinate P from the texture bound to sampler with explicit texture coordinate gradiends as specified in dPdx and dPdy. Set:\nδsδx=δP.sδx\nδsδy=δP.sδy\nδtδx=δP.tδx\nδtδy=δP.tδy\nδrδx=0.0  for a 2D texture,  δP.pδx  otherwise\nδrδy=0.0  for a 2D texture,  δP.pδy  otherwise\nFor the cube version, the partial derivatives of P are assumed to be in the coordinate system used before texture coordinates are projected onto the appropriate cube face.\n"
      },
      {
        "label": "textureGradOffset",
        "kind": 1,
        "insertText": "textureGradOffset",
        "detail": "perform a texture lookup with explicit gradients and offset",
        "documentation": "Declaration\ngvec4 textureGradOffset(gsampler2D sampler,  vec2 P,  vec2 dPdx,  vec2 dPdy,  ivec2 offset)\ngvec4 textureGradOffset(gsampler3D sampler,  vec3 P,  vec3 dPdx,  vec3 dPdy,  ivec3 offset)\nfloat textureGradOffset(sampler2DShadow sampler,  vec3 P,  vec2 dPdx,  vec2 dPdy,  ivec2 offset)\ngvec4 textureGradOffset(gsampler2DArray sampler,  vec3 P,  vec2 dPdx,  vec2 dPdy,  ivec2 offset)\nfloat textureGradOffset(sampler2DArrayShadow sampler,  vec4 P,  vec2 dPdx,  vec2 dPdy,  ivec2 offset)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\ndPdx - Specifies the partial derivative of P with respect to window x.\ndPdy - Specifies the partial derivative of P with respect to window y.\noffset - Specifies the offset to be applied to the texture coordinates before sampling.\n\nDescription\ntextureGradOffset performs a texture lookup at coordinate P from the texture bound to sampler with explicit texture coordinate gradiends as specified in dPdx and dPdy. An explicit offset is also supplied in offset. textureGradOffset consumes dPdx and dPdy as textureGrad and offset as textureOffset.\n"
      },
      {
        "label": "textureLod",
        "kind": 1,
        "insertText": "textureLod",
        "detail": "perform a texture lookup with explicit level-of-detail",
        "documentation": "Declaration\ngvec4 textureLod(gsampler2D sampler,  vec2 P,  float lod)\ngvec4 textureLod(gsampler3D sampler,  vec3 P,  float lod)\ngvec4 textureLod(gsamplerCube sampler,  vec3 P,  float lod)\nfloat textureLod(sampler2DShadow sampler,  vec3 P,  float lod)\ngvec4 textureLod(gsampler2DArray sampler,  vec3 P,  float lod)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\nlod - Specifies the explicit level-of-detail\n\nDescription\ntextureLod performs a texture lookup at coordinate P from the texture bound to sampler with an explicit level-of-detail as specified in lod. lod specifies λbase and sets the partial derivatives as follows:\nδuδx=0,δvδx=0,δwδx=0δuδy=0,δvδy=0,δwδy=0\n"
      },
      {
        "label": "textureLodOffset",
        "kind": 1,
        "insertText": "textureLodOffset",
        "detail": "perform a texture lookup with explicit level-of-detail and offset",
        "documentation": "Declaration\ngvec4 textureLodOffset(gsampler2D sampler,  vec2 P,  float lod,  ivec2 offset)\ngvec4 textureLodOffset(gsampler3D sampler,  vec3 P,  float lod,  ivec3 offset)\nfloat textureLodOffset(sampler2DShadow sampler,  vec3 P,  float lod,  ivec2 offset)\ngvec4 textureLodOffset(gsampler2DArray sampler,  vec3 P,  float lod,  ivec2 offset)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which the texture will be sampled.\nlod - Specifies the explicit level-of-detail from which texels will be fetched.\noffset - Specifies the offset that will be applied to P before texels are fetched.\n\nDescription\ntextureLodOffset performs a texture lookup at coordinate P from the texture bound to sampler with an explicit level-of-detail as specified in lod. Behavior is the same as in textureLod except that before sampling, offset is added to P.\n"
      },
      {
        "label": "textureOffset",
        "kind": 1,
        "insertText": "textureOffset",
        "detail": "perform a texture lookup with offset",
        "documentation": "Declaration\ngvec4 textureOffset(gsampler2D sampler,  vec2 P,  ivec2 offset,  [float bias])\ngvec4 textureOffset(gsampler3D sampler,  vec3 P,  ivec3 offset,  [float bias])\nfloat textureOffset(sampler2DShadow sampler,  vec3 P,  ivec2 offset,  [float bias])\ngvec4 textureOffset(gsampler2DArray sampler,  vec3 P,  ivec2 offset,  [float bias])\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\noffset - Specifies offset, in texels that will be applied to P before looking up the texel.\nbias - Specifies an optional bias to be applied during level-of-detail computation.\n\nDescription\ntextureOffset performs a texture lookup at coordinate P from the texture bound to sampler with an an additional offset, specified in texels in offset that will be applied to the (u, v, w) texture coordinates before looking up each texel. The offset value must be a constant expression. A limited range of offset values are supported; the minimum and maximum offset values are implementation-dependent and may be determined by querying GL_MIN_PROGRAM_TEXEL_OFFSET and GL_MAX_PROGRAM_TEXEL_OFFSET, respectively.\nNote that offset does not apply to the layer coordinate for texture arrays. Also note that offsets are not supported for cube maps.\n"
      },
      {
        "label": "textureProj",
        "kind": 1,
        "insertText": "textureProj",
        "detail": "perform a texture lookup with projection",
        "documentation": "Declaration\ngvec4 textureProj(gsampler2D sampler,  vec3 P,  [float bias])\ngvec4 textureProj(gsampler2D sampler,  vec4 P,  [float bias])\ngvec4 textureProj(gsampler3D sampler,  vec4 P,  [float bias])\nfloat textureProj(sampler2DShadow sampler,  vec4 P,  [float bias])\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\nbias - Specifies an optional bias to be applied during level-of-detail computation.\n\nDescription\ntextureProj performs a texture lookup with projection. The texture coordinates consumed from P, not including the last component of P, are divided by the last component of P. The resulting 3rd component of P in the shadow forms is used as Dref. After these values are computed, the texture lookup proceeds as in texture.\n"
      },
      {
        "label": "textureProjGrad",
        "kind": 1,
        "insertText": "textureProjGrad",
        "detail": "perform a texture lookup with projection and explicit gradients",
        "documentation": "Declaration\ngvec4 textureProjGrad(gsampler2D sampler,  vec3 P,  vec2 dPdx,  vec2 dPdy)\ngvec4 textureProjGrad(gsampler2D sampler,  vec4 P,  vec2 dPdx,  vec2 dPdy)\ngvec4 textureProjGrad(gsampler3D sampler,  vec4 P,  vec3 dPdx,  vec3 dPdy)\nfloat textureProjGrad(sampler2DShadow sampler,  vec4 P,  vec2 dPdx,  vec2 dPdy)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\ndPdx - Specifies the partial derivative of P with respect to window x.\ndPdy - Specifies the partial derivative of P with respect to window y.\n\nDescription\ntextureProjGrad performs a texture lookup with projection and explicit gradients. The texture coordinates consumed from P, not including the last component of P, are divided by the last component of P. The resulting 3rd component of P in the shadow forms is used as Dref. After these values are computed, the texture lookup proceeds as in textureGrad, passing dPdx and dPdy as gradients.\n"
      },
      {
        "label": "textureProjGradOffset",
        "kind": 1,
        "insertText": "textureProjGradOffset",
        "detail": "perform a texture lookup with projection, explicit gradients and offset",
        "documentation": "Declaration\ngvec4 textureProjGradOffset(gsampler2D sampler,  vec3 P,  vec2 dPdx,  vec2 dPdy,  ivec2 offset)\ngvec4 textureProjGradOffset(gsampler2D sampler,  vec4 P,  vec2 dPdx,  vec2 dPdy,  ivec2 offset)\ngvec4 textureProjGradOffset(gsampler3D sampler,  vec4 P,  vec3 dPdx,  vec3 dPdy,  ivec3 offset)\nfloat textureProjGradOffset(sampler2DShadow sampler,  vec4 P,  vec2 dPdx,  vec2 dPdy,  ivec2 offset)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\ndPdx - Specifies the partial derivative of P with respect to window x.\ndPdy - Specifies the partial derivative of P with respect to window y.\noffset - Specifies the offsets, in texels at which the texture will be sampled relative to the projection of P.\n\nDescription\ntextureProjGradOffset performs a texture lookup with projection and explicit gradients and offsets. The texture coordinates consumed from P, not including the last component of P, are divided by the last component of P. The resulting 3rd component of P in the shadow forms is used as Dref. After these values are computed, the texture lookup proceeds as in textureGradOffset, passing dPdx and dPdy as gradients, and offset as the offset.\n"
      },
      {
        "label": "textureProjLod",
        "kind": 1,
        "insertText": "textureProjLod",
        "detail": "perform a texture lookup with projection and explicit level-of-detail",
        "documentation": "Declaration\ngvec4 textureProjLod(gsampler2D sampler,  vec3 P,  float lod)\ngvec4 textureProjLod(gsampler2D sampler,  vec4 P,  float lod)\ngvec4 textureProjLod(gsampler3D sampler,  vec4 P,  float lod)\nfloat textureProjLod(sampler2DShadow sampler,  vec4 P,  float lod)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\nlod - Specifies the explicit level-of-detail from which to fetch texels.\n\nDescription\ntextureProjLod performs a texture lookup with projection from an explicitly specified level-of-detail. The texture coordinates consumed from P, not including the last component of P, are divided by the last component of P. The resulting 3rd component of P in the shadow forms is used as Dref. After these values are computed, the texture lookup proceeds as in textureLod, with lod used to specify the level-of-detail from which the texture will be sampled.\n"
      },
      {
        "label": "textureProjLodOffset",
        "kind": 1,
        "insertText": "textureProjLodOffset",
        "detail": "perform a texture lookup with projection and explicit level-of-detail and offset",
        "documentation": "Declaration\ngvec4 textureProjLodOffset(gsampler2D sampler,  vec3 P,  float lod,  ivec2 offset)\ngvec4 textureProjLodOffset(gsampler2D sampler,  vec4 P,  float lod,  ivec2 offset)\ngvec4 textureProjLodOffset(gsampler3D sampler,  vec4 P,  float lod,  ivec3 offset)\nfloat textureProjLodOffset(sampler2DShadow sampler,  vec4 P,  float lod,  ivec2 offset)\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which texture will be sampled.\nlod - Specifies the explicit level-of-detail from which to fetch texels.\noffset - Specifies the offset, in texels, to be applied to P before fetching texels.\n\nDescription\ntextureProjLodOffset performs a texture lookup with projection from an explicitly specified level-of-detail with an offset applied to the texture coordinates before sampling. The texture coordinates consumed from P, not including the last component of P, are divided by the last component of P. The resulting 3rd component of P in the shadow forms is used as Dref. After these values are computed, the texture lookup proceeds as in textureLodOffset, with lod used to specify the level-of-detail from which the texture will be sampled and offset used to specifiy the offset, in texels, to be applied to the texture coordinates before sampling.\n"
      },
      {
        "label": "textureProjOffset",
        "kind": 1,
        "insertText": "textureProjOffset",
        "detail": "perform a texture lookup with projection and offset",
        "documentation": "Declaration\ngvec4 textureProjOffset(gsampler2D sampler,  vec3 P,  ivec2 offset,  [float bias])\ngvec4 textureProjOffset(gsampler2D sampler,  vec4 P,  ivec2 offset,  [float bias])\ngvec4 textureProjOffset(gsampler3D sampler,  vec4 P,  ivec3 offset,  [float bias])\nfloat textureProjOffset(sampler2DShadow sampler,  vec4 P,  ivec2 offset,  [float bias])\n\nParameters\nsampler - Specifies the sampler to which the texture from which texels will be retrieved is bound.\nP - Specifies the texture coordinates at which the texture will be sampled.\noffset - Specifies the offset that is applied to P before sampling occurs.\nbias - Specifies an optional bias to be applied during level-of-detail computation.\n\nDescription\ntextureProjOffset performs a texture lookup with projection. The texture coordinates consumed from P, not including the last component of P, are divided by the last component of P. The resulting 3rd component of P in the shadow forms is used as Dref. After these values are computed, the texture lookup proceeds as in textureOffset, with the offset used to offset the computed texture coordinates.\n"
      },
      {
        "label": "textureSize",
        "kind": 1,
        "insertText": "textureSize",
        "detail": "retrieve the dimensions of a level of a texture",
        "documentation": "Declaration\nivec2 textureSize(gsampler2D sampler,  int lod)\nivec3 textureSize(gsampler3D sampler,  int lod)\nivec2 textureSize(gsamplerCube sampler,  int lod)\nivec2 textureSize(sampler2DShadow sampler,  int lod)\nivec2 textureSize(samplerCubeShadow sampler,  int lod)\nivec3 textureSize(gsampler2DArray sampler,  int lod)\nivec3 textureSize(sampler2DArrayShadow sampler,  int lod)\n\nParameters\nsampler - Specifies the sampler to which the texture whose dimensions to retrieve is bound.\nlod - Specifies the level of the texture for which to retrieve the dimensions.\n\nDescription\ntextureSize returns the dimensions of level lod (if present) of the texture bound to sampler. The components in the return value are filled in, in order, with the width, height and depth of the texture. For the array forms, the last component of the return value is the number of layers in the texture array. The return values are returned as highp ints.\n"
      },
      {
        "label": "transpose",
        "kind": 1,
        "insertText": "transpose",
        "detail": "calculate the transpose of a matrix",
        "documentation": "Declaration\nmat2 transpose(mat2 m)\nmat3 transpose(mat3 m)\nmat4 transpose(mat4 m)\nmat2x3 transpose(mat3x2 m)\nmat2x4 transpose(mat4x2 m)\nmat3x2 transpose(mat2x3 m)\nmat3x4 transpose(mat4x3 m)\nmat4x2 transpose(mat2x4 m)\nmat4x3 transpose(mat3x4 m)\n\nParameters\nm - Specifies the matrix of which to take the transpose.\n\nDescription\ntranspose returns the transpose of the matrix m.\n"
      },
      {
        "label": "trunc",
        "kind": 1,
        "insertText": "trunc",
        "detail": "find the truncated value of the parameter",
        "documentation": "Declaration\ngenType trunc(genType x)\n\nParameters\nx - Specify the value to evaluate.\n\nDescription\ntrunc returns a value equal to the nearest integer to x whose absolute value is not larger than the absolute value of x.\n"
      },
      {
        "label": "unpackHalf2x16",
        "kind": 1,
        "insertText": "unpackHalf2x16",
        "detail": "convert two 16-bit floating-point values packed into a single 32-bit integer into a vector of two 32-bit floating-point quantities",
        "documentation": "Declaration\nvec2 unpackHalf2x16(uint v)\n\nParameters\nv - Specify a single 32-bit unsigned integer values that contains two 16-bit floating point values to be unpacked.\n\nDescription\nunpackHalf2x16 returns a two-component floating-point vector with components obtained by unpacking a 32-bit unsigned integer into a pair of 16-bit values, interpreting those values as 16-bit floating-point numbers according to the OpenGL ES Specification, and converting them to 32-bit floating-point values. The first component of the vector is obtained from the 16 least-significant bits of v; the second component is obtained from the 16 most-significant bits of v.\n"
      },
      {
        "label": "unpackUnorm2x16",
        "kind": 1,
        "insertText": "unpackUnorm2x16",
        "detail": "unpack floating-point values from an unsigned integer",
        "documentation": "Declaration\nvec2 unpackUnorm2x16(uint p)\n\nParameters\np - Specifies an unsigned integer containing packed floating-point values.\n\nDescription\nunpackUnorm2x16, unpackSnorm2x16 unpack single 32-bit unsigned integers, specified in the parameter p into a pair of 16-bit unsigned integers. Then, each component is converted to a normalized floating-point value to generate the returned two- or four-component vector.\nThe conversion for unpacked fixed point value f to floating-point is performed as follows:\nunpackUnorm2x16: f / 65535.0unpackSnorm2x16: clamp(f / 32727.0, -1.0, 1.0)\nThe first component of the returned vector will be extracted from the least significant bits of the input; the last component will be extracted from the most significant bits.\n"
      },
      {
        "label": "unpackSnorm2x16",
        "kind": 1,
        "insertText": "unpackSnorm2x16",
        "detail": "unpack floating-point values from an unsigned integer",
        "documentation": "Declaration\nvec2 unpackSnorm2x16(uint p)\n\nParameters\np - Specifies an unsigned integer containing packed floating-point values.\n\nDescription\nunpackUnorm2x16, unpackSnorm2x16 unpack single 32-bit unsigned integers, specified in the parameter p into a pair of 16-bit unsigned integers. Then, each component is converted to a normalized floating-point value to generate the returned two- or four-component vector.\nThe conversion for unpacked fixed point value f to floating-point is performed as follows:\nunpackUnorm2x16: f / 65535.0unpackSnorm2x16: clamp(f / 32727.0, -1.0, 1.0)\nThe first component of the returned vector will be extracted from the least significant bits of the input; the last component will be extracted from the most significant bits.\n"
      },
      {
        "label": "const",
        "kind": 17,
        "insertText": "const",
        "detail": "const"
      },
      {
        "label": "uniform",
        "kind": 17,
        "insertText": "uniform",
        "detail": "uniform"
      },
      {
        "label": "layout",
        "kind": 17,
        "insertText": "layout",
        "detail": "layout"
      },
      {
        "label": "centroid",
        "kind": 17,
        "insertText": "centroid",
        "detail": "centroid"
      },
      {
        "label": "flat",
        "kind": 17,
        "insertText": "flat",
        "detail": "flat"
      },
      {
        "label": "smooth",
        "kind": 17,
        "insertText": "smooth",
        "detail": "smooth"
      },
      {
        "label": "in",
        "kind": 17,
        "insertText": "in",
        "detail": "in"
      },
      {
        "label": "out",
        "kind": 17,
        "insertText": "out",
        "detail": "out"
      },
      {
        "label": "inout",
        "kind": 17,
        "insertText": "inout",
        "detail": "inout"
      },
      {
        "label": "true",
        "kind": 17,
        "insertText": "true",
        "detail": "true"
      },
      {
        "label": "false",
        "kind": 17,
        "insertText": "false",
        "detail": "false"
      },
      {
        "label": "invariant",
        "kind": 17,
        "insertText": "invariant",
        "detail": "invariant"
      },
      {
        "label": "lowp",
        "kind": 17,
        "insertText": "lowp",
        "detail": "lowp"
      },
      {
        "label": "mediump",
        "kind": 17,
        "insertText": "mediump",
        "detail": "mediump"
      },
      {
        "label": "highp",
        "kind": 17,
        "insertText": "highp",
        "detail": "highp"
      },
      {
        "label": "precision",
        "kind": 17,
        "insertText": "precision",
        "detail": "precision"
      },
      {
        "label": "struct",
        "kind": 17,
        "insertText": "struct",
        "detail": "struct"
      },
      {
        "label": "break",
        "kind": 17,
        "insertText": "break",
        "detail": "break"
      },
      {
        "label": "continue",
        "kind": 17,
        "insertText": "continue",
        "detail": "continue"
      },
      {
        "label": "do",
        "kind": 17,
        "insertText": "do",
        "detail": "do"
      },
      {
        "label": "for",
        "kind": 17,
        "insertText": "for",
        "detail": "for"
      },
      {
        "label": "while",
        "kind": 17,
        "insertText": "while",
        "detail": "while"
      },
      {
        "label": "switch",
        "kind": 17,
        "insertText": "switch",
        "detail": "switch"
      },
      {
        "label": "case",
        "kind": 17,
        "insertText": "case",
        "detail": "case"
      },
      {
        "label": "default",
        "kind": 17,
        "insertText": "default",
        "detail": "default"
      },
      {
        "label": "if",
        "kind": 17,
        "insertText": "if",
        "detail": "if"
      },
      {
        "label": "else",
        "kind": 17,
        "insertText": "else",
        "detail": "else"
      },
      {
        "label": "discard",
        "kind": 17,
        "insertText": "discard",
        "detail": "discard"
      },
      {
        "label": "return",
        "kind": 17,
        "insertText": "return",
        "detail": "return"
      },
      {
        "label": "void",
        "kind": 17,
        "insertText": "void",
        "detail": "void"
      },
      {
        "label": "bool",
        "kind": 17,
        "insertText": "bool",
        "detail": "bool"
      },
      {
        "label": "int",
        "kind": 17,
        "insertText": "int",
        "detail": "int"
      },
      {
        "label": "uint",
        "kind": 17,
        "insertText": "uint",
        "detail": "uint"
      },
      {
        "label": "float",
        "kind": 17,
        "insertText": "float",
        "detail": "float"
      },
      {
        "label": "vec2",
        "kind": 17,
        "insertText": "vec2",
        "detail": "vec2"
      },
      {
        "label": "vec3",
        "kind": 17,
        "insertText": "vec3",
        "detail": "vec3"
      },
      {
        "label": "vec4",
        "kind": 17,
        "insertText": "vec4",
        "detail": "vec4"
      },
      {
        "label": "bvec2",
        "kind": 17,
        "insertText": "bvec2",
        "detail": "bvec2"
      },
      {
        "label": "bvec3",
        "kind": 17,
        "insertText": "bvec3",
        "detail": "bvec3"
      },
      {
        "label": "bvec4",
        "kind": 17,
        "insertText": "bvec4",
        "detail": "bvec4"
      },
      {
        "label": "ivec2",
        "kind": 17,
        "insertText": "ivec2",
        "detail": "ivec2"
      },
      {
        "label": "ivec3",
        "kind": 17,
        "insertText": "ivec3",
        "detail": "ivec3"
      },
      {
        "label": "ivec4",
        "kind": 17,
        "insertText": "ivec4",
        "detail": "ivec4"
      },
      {
        "label": "uvec2",
        "kind": 17,
        "insertText": "uvec2",
        "detail": "uvec2"
      },
      {
        "label": "uvec3",
        "kind": 17,
        "insertText": "uvec3",
        "detail": "uvec3"
      },
      {
        "label": "uvec4",
        "kind": 17,
        "insertText": "uvec4",
        "detail": "uvec4"
      },
      {
        "label": "mat2",
        "kind": 17,
        "insertText": "mat2",
        "detail": "mat2"
      },
      {
        "label": "mat3",
        "kind": 17,
        "insertText": "mat3",
        "detail": "mat3"
      },
      {
        "label": "mat4",
        "kind": 17,
        "insertText": "mat4",
        "detail": "mat4"
      },
      {
        "label": "mat2x2",
        "kind": 17,
        "insertText": "mat2x2",
        "detail": "mat2x2"
      },
      {
        "label": "mat2x3",
        "kind": 17,
        "insertText": "mat2x3",
        "detail": "mat2x3"
      },
      {
        "label": "mat2x4",
        "kind": 17,
        "insertText": "mat2x4",
        "detail": "mat2x4"
      },
      {
        "label": "mat3x2",
        "kind": 17,
        "insertText": "mat3x2",
        "detail": "mat3x2"
      },
      {
        "label": "mat3x3",
        "kind": 17,
        "insertText": "mat3x3",
        "detail": "mat3x3"
      },
      {
        "label": "mat3x4",
        "kind": 17,
        "insertText": "mat3x4",
        "detail": "mat3x4"
      },
      {
        "label": "mat4x2",
        "kind": 17,
        "insertText": "mat4x2",
        "detail": "mat4x2"
      },
      {
        "label": "mat4x3",
        "kind": 17,
        "insertText": "mat4x3",
        "detail": "mat4x3"
      },
      {
        "label": "mat4x4",
        "kind": 17,
        "insertText": "mat4x4",
        "detail": "mat4x4"
      },
      {
        "label": "sampler2D",
        "kind": 17,
        "insertText": "sampler2D",
        "detail": "sampler2D"
      },
      {
        "label": "sampler3D",
        "kind": 17,
        "insertText": "sampler3D",
        "detail": "sampler3D"
      },
      {
        "label": "samplerCube",
        "kind": 17,
        "insertText": "samplerCube",
        "detail": "samplerCube"
      },
      {
        "label": "samplerCubeShadow",
        "kind": 17,
        "insertText": "samplerCubeShadow",
        "detail": "samplerCubeShadow"
      },
      {
        "label": "sampler2DShadow",
        "kind": 17,
        "insertText": "sampler2DShadow",
        "detail": "sampler2DShadow"
      },
      {
        "label": "sampler2DArray",
        "kind": 17,
        "insertText": "sampler2DArray",
        "detail": "sampler2DArray"
      },
      {
        "label": "sampler2DArrayShadow",
        "kind": 17,
        "insertText": "sampler2DArrayShadow",
        "detail": "sampler2DArrayShadow"
      },
      {
        "label": "isampler2D",
        "kind": 17,
        "insertText": "isampler2D",
        "detail": "isampler2D"
      },
      {
        "label": "isampler3D",
        "kind": 17,
        "insertText": "isampler3D",
        "detail": "isampler3D"
      },
      {
        "label": "isamplerCube",
        "kind": 17,
        "insertText": "isamplerCube",
        "detail": "isamplerCube"
      },
      {
        "label": "isampler2DArray",
        "kind": 17,
        "insertText": "isampler2DArray",
        "detail": "isampler2DArray"
      },
      {
        "label": "usampler2D",
        "kind": 17,
        "insertText": "usampler2D",
        "detail": "usampler2D"
      },
      {
        "label": "usampler3D",
        "kind": 17,
        "insertText": "usampler3D",
        "detail": "usampler3D"
      },
      {
        "label": "usamplerCube",
        "kind": 17,
        "insertText": "usamplerCube",
        "detail": "usamplerCube"
      },
      {
        "label": "usampler2DArray",
        "kind": 17,
        "insertText": "usampler2DArray",
        "detail": "usampler2DArray"
      },
      {
        "label": "gl_MaxVertexAttribs",
        "kind": 14,
        "insertText": "gl_MaxVertexAttribs",
        "detail": "gl_MaxVertexAttribs"
      },
      {
        "label": "gl_MaxVertexUniformVectors",
        "kind": 14,
        "insertText": "gl_MaxVertexUniformVectors",
        "detail": "gl_MaxVertexUniformVectors"
      },
      {
        "label": "gl_MaxVertexOutputVectors",
        "kind": 14,
        "insertText": "gl_MaxVertexOutputVectors",
        "detail": "gl_MaxVertexOutputVectors"
      },
      {
        "label": "gl_MaxFragmentInputVectors",
        "kind": 14,
        "insertText": "gl_MaxFragmentInputVectors",
        "detail": "gl_MaxFragmentInputVectors"
      },
      {
        "label": "gl_MaxVertexTextureImageUnits",
        "kind": 14,
        "insertText": "gl_MaxVertexTextureImageUnits",
        "detail": "gl_MaxVertexTextureImageUnits"
      },
      {
        "label": "gl_MaxCombinedTextureImageUnits",
        "kind": 14,
        "insertText": "gl_MaxCombinedTextureImageUnits",
        "detail": "gl_MaxCombinedTextureImageUnits"
      },
      {
        "label": "gl_MaxTextureImageUnits",
        "kind": 14,
        "insertText": "gl_MaxTextureImageUnits",
        "detail": "gl_MaxTextureImageUnits"
      },
      {
        "label": "gl_MaxFragmentUniformVectors",
        "kind": 14,
        "insertText": "gl_MaxFragmentUniformVectors",
        "detail": "gl_MaxFragmentUniformVectors"
      },
      {
        "label": "gl_MaxDrawBuffers",
        "kind": 14,
        "insertText": "gl_MaxDrawBuffers",
        "detail": "gl_MaxDrawBuffers"
      },
      {
        "label": "gl_MinProgramTexelOffset",
        "kind": 14,
        "insertText": "gl_MinProgramTexelOffset",
        "detail": "gl_MinProgramTexelOffset"
      },
      {
        "label": "gl_MaxProgramTexelOffset",
        "kind": 14,
        "insertText": "gl_MaxProgramTexelOffset",
        "detail": "gl_MaxProgramTexelOffset"
      }
    ];
    let list = new Set();
    kSuggestionData.forEach(item => list.add(item.label));
    let word = model.getWordAtPosition(position);
    let words = model.findMatches(
      "//.*|[a-zA-Z_]+[0-9]|[a-zA-Z_]+",
      true,
      true,
      true,
      null,
      true
    );
    let suggestions = new Array();
    words.forEach(item =>
    {
      let is_typed = item.range.startLineNumber === position.lineNumber
                     && item.range.startColumn === word.startColumn
                     && item.range.endColumn === word.endColumn;
      if(!is_typed && item.matches[0][0] !== "/")
      {
        if(!list.has(item.matches[0]))
        {
          suggestions.push({
            label: item.matches[0],
            insertText: item.matches[0],
            kind: 18,
          });
          list.add(item.matches[0]);
        }
      }
    });
    return {suggestions: kSuggestionData.concat(suggestions)}
  }
}