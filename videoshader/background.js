'use strict';

function GetStorageItem(arr, name) {
  let item = arr.find(item => item.name === name);
  return item;
}

function GetIndexByOrigin() {
  return new Promise(resolve => {
    chrome.tabs.query({ currentWindow: true, active: true }, tabs => {
      let origin = (new URL(tabs[0].url)).origin;
      chrome.storage.local.get("indices", indices => {
        let index = indices.indices.find(index => index.origin === origin);
        if (index === void 0) {
          indices.indices.push({ origin: origin, index: 0 });
          chrome.storage.local.set({ indices: indices.indices });
          resolve(0);
        }
        else {
          resolve(index.index);
        }
      });
    });
  });
}

function GetShaderCode() {
  return new Promise(resolve => {
    chrome.storage.local.get("current", current => {
      chrome.storage.local.get("effects", effects => {
        let effect = GetStorageItem(effects.effects, current.current);
        let code = {
          name: effect.name,
          feedback: null,
          vertex: null,
          fragment: null,
          uniforms: effect.uniforms,
          background: effect.background,
          model: effect.model,
          count: effect.count
        }
        chrome.storage.local.get("fdbk_codes", fdbk_codes => {
          code.feedback = GetStorageItem(fdbk_codes.fdbk_codes, effect.fdbk).code;
          chrome.storage.local.get("vert_codes", vert_codes => {
            code.vertex = GetStorageItem(vert_codes.vert_codes, effect.vert).code;
            chrome.storage.local.get("frag_codes", frag_codes => {
              code.fragment = GetStorageItem(frag_codes.frag_codes, effect.frag).code;
              resolve(code);
            });
          });
        });
      });
    });
  });
}

function GetItem(name) {
  return new Promise(resolve => {
    chrome.storage.local.get(name, item => {
      resolve(item[name]);
    });
  });
}

async function ResponseInitObject(msg) {
  switch (msg) {
    case "true":
      let init = {
        video_number: 0,
        name: null,
        feedback: null,
        vertex: null,
        fragment: null,
        scale: null,
        fps: null,
        uniforms: null,
        background: null,
        model: null,
        count: null
      };
      init.video_number = await GetIndexByOrigin();
      let code = await GetShaderCode();
      init.name = code.name;
      init.feedback = code.feedback;
      init.vertex = code.vertex;
      init.fragment = code.fragment;
      init.uniforms = code.uniforms;
      init.background = code.background;
      init.model = code.model;
      init.count = code.count;
      init.fps = await GetItem("fps");
      init.scale = await GetItem("scale");
      return init;
    case "false":
      return "fuck you!!!";
    default:
      return "pardon???";
  }
}

async function ChangeCurrent(msg) {
  chrome.storage.local.set({ current: msg });
  let code = await GetShaderCode();
  chrome.tabs.query({ currentWindow: true, active: true }, tabs => {
    chrome.tabs.sendMessage(tabs[0].id, {
      subject: "effect_change",
      message: code
    }
    );
  });
}

chrome.runtime.onInstalled.addListener(details => {
  if (details.reason === "install") {
    let src = chrome.extension.getURL("modules/init.mjs");
    import(src).then(module => chrome.storage.local.set(module.kInitData));
  }
});

chrome.runtime.onMessage.addListener(
  function (request, sender, callback) {
    switch (request.subject) {
      case "video_existence":
        (async () => callback(await ResponseInitObject(request.message)))();
        break;
      case "change_current":
        ChangeCurrent(request.message);
        break;
    }
    return true;
  }
);