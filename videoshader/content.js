'use strict';

let renderer = null;

function GetVideoElement() {
  return new Promise((resolve, reject) => {
    let counter = 0;
    let _thread_mask = setInterval(function () {
      let video = document.getElementsByTagName("video");
      if (video.length > 0) {
        clearInterval(_thread_mask);
        resolve(video);
      }
      if (counter > 20) {
        clearInterval(_thread_mask);
        reject(null);
      }
      ++counter;
    }, 50);
  });
}

function ResponseVideoStatus() {
  switch (renderer) {
    case null:
      return "searching";
    case 0:
      return "nothing";
    default:
      return "ready";
  }
}

function ResponseEffectName() {
  if (renderer != null) return renderer.GetCurrentEffect();
  else return null;
}

function ResponseEffectStatus() {
  switch (renderer) {
    case null:
    case 0:
      return "off";
    default:
      if (renderer.IsDraw()) return "on";
      else return "off";
  }
}

function ResponseUniformsStatus() {
  if (renderer != null) return renderer.GetExtUniforms();
  else return null;
}

function ResponseVideoLength() {
  if (renderer != null) return renderer.GetVideoLength();
  else return null;
}

function ResponseVideoIndex() {
  if (renderer != null) return renderer.GetCurrentVideoIndex();
  else return null;
}

function EffectSwitch(msg) {
  if (renderer !== void 0 && renderer !== null) {
    switch (msg) {
      case "on":
        renderer.IntervalDraw();
        break;
      case "off":
        renderer.IntervalStop();
        break;
    }
  }
}

function EffectChange(msg) {
  if (renderer !== void 0 && renderer !== null) {
    renderer.ChangeShader(msg);
  }
}

function IndexChange(msg) {
  if (renderer !== void 0 && renderer !== null) {
    renderer.ChangeVideoIndex(msg);
  }
}

function ScaleChange(msg) {
  if (renderer !== void 0 && renderer !== null) {
    renderer.ChangeScale(msg);
  }
}

function FpsChange(msg) {
  if (renderer !== void 0 && renderer !== null) {
    renderer.ChangeFps(msg);
  }
}

function UniformsChange(msg) {
  if (renderer !== void 0 && renderer !== null) {
    renderer.UpdateExtUniformsVales(msg);
  }
}

chrome.runtime.onMessage.addListener(
  function (request, sender, callback) {
    switch (request.subject) {
      case "video_status":
        callback(ResponseVideoStatus());
        break;
      case "video_length":
        callback(ResponseVideoLength());
        break;
      case "video_index":
        callback(ResponseVideoIndex());
        break;
      case "effect_name":
        callback(ResponseEffectName());
        break;
      case "effect_status":
        callback(ResponseEffectStatus());
        break;
      case "uniforms_status":
        callback(ResponseUniformsStatus());
        break;
      case "effect_switch":
        EffectSwitch(request.message);
        break;
      case "effect_change":
        EffectChange(request.message)
        break;
      case "index_change":
        IndexChange(request.message)
        break;
      case "scale_change":
        ScaleChange(request.message)
        break;
      case "fps_change":
        FpsChange(request.message)
        break;
      case "uniforms_change":
        UniformsChange(request.message)
        break;
    }
    return true;
  }
);

(async () => {
  let video = await GetVideoElement().catch(() => null);
  if (video != null) {
    chrome.runtime.sendMessage({
      subject: "video_existence",
      message: "true"
    },
      response => {
        let src = chrome.extension.getURL("modules/renderer.mjs");
        import(src).then(module => {
          renderer = new module.WebGL2Renderer(video, response);
          chrome.runtime.sendMessage({
            subject: "renderer_getready",
            message: "true"
          });
        })
      });
  }
  else {
    renderer = 0;
    chrome.runtime.sendMessage({
      subject: "video_existence",
      message: "false"
    });
  }
})();
