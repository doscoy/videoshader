"use strict";

export class MaskCanvas {
  video_obj = null;
  canvas = null;
  div = null;
  ratio = window.devicePixelRatio;
  scale = null;
  resize_callback = new Array();

  constructor(video_obj, scale) {
    this.video_obj = video_obj;
    this.div = document.createElement("div")
    this.div.setAttribute("id", "effector_container");
    this.div.style.visibility = "hidden";
    this.canvas = document.createElement("canvas");
    this.canvas.setAttribute("id", "effector");
    this.canvas.style.width = "100%";
    this.canvas.style.height = "100%";
    this.canvas.style.pointerEvents = "none";
    this.div.appendChild(this.canvas);
    this.scale = Math.max(Math.min(scale, 1.0), 0.0);
    this._OverRideVideo();
    this.video_obj.SetMissCallback((() => this._OverRideVideo()));
    this.video_obj.SetResizeObs(new ResizeObserver(() => this.FitVideoStyle()));
  }

  _CopyVideoStyle() {
    let tmp_visibility = this.div.style.visibility;
    let tmp_zindex = this.div.style.zIndex;
    this.div.style.cssText = this.video_obj.GetStyle().cssText;
    this.div.style.position = "absolute";
    this.div.style.pointerEvents = "none";
    if (this.div.style.width === "") {
      this.div.style.width = this.video_obj.GetParent().style.width;
      if (this.div.style.width === "") {
        this.div.style.width = "100%";
      }
    }
    if (this.div.style.height === "") {
      this.div.style.height = this.video_obj.GetParent().style.height;
      if (this.div.style.height === "") {
        this.div.style.height = "100%";
      }
    }
    this.div.style.visibility = tmp_visibility;
    this.div.style.zIndex = tmp_zindex;
    let x = this.ratio * this.scale;
    this.canvas.width = this.canvas.getBoundingClientRect().width * x;
    this.canvas.height = this.canvas.getBoundingClientRect().height * x;
  }

  _OverRideVideo() {
    let parent = this.video_obj.GetParent();
    parent.insertBefore(this.div, this.video_obj.GetElement().nextSibling);
    let video_zindex = this.video_obj.GetStyle().zIndex;
    this.div.style.zIndex = video_zindex === "auto" ? "auto" : video_zindex + 1;
    this.FitVideoStyle();
  }

  FitVideoStyle() {
    this._CopyVideoStyle();
    this.resize_callback.forEach(callback => {
      callback(this.canvas.width, this.canvas.height);
    });
  }

  SetResizeCallback(resize_callback) { this.resize_callback.push(resize_callback); }
  GetWebGL2Context() { return this.canvas.getContext("webgl2", { antialias: false, preserveDrawingBuffer: true }); }
  GetResolution() { return [this.canvas.width, this.canvas.height]; }
  GetWidth() { return this.canvas.width; }
  GetHeight() { return this.canvas.height; }
  SetHidden() { this.div.style.visibility = "hidden"; }
  SetVisible() { this.div.style.visibility = "visible"; }
  
  ChangeScale(scale) {
    this.scale = scale;
    let x = this.ratio * this.scale;
    this.canvas.width = this.canvas.getBoundingClientRect().width * x;
    this.canvas.height = this.canvas.getBoundingClientRect().height * x;
    this.resize_callback.forEach(callback => {
      callback(this.canvas.width, this.canvas.height);
    });
  }
}