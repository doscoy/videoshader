"use strict";

export class FrameBuffer {
  frame_buffer = null;
  color_buffer = null;
  depth_buffer = null;
  attach = new Array();
  width = null;
  height = null;

  constructor(gl, width, height, attach_volume) {
    for (let i = 0; i <= attach_volume; ++i) {
      this.attach.push(gl.COLOR_ATTACHMENT0 + i);
    }
    this.frame_buffer = gl.createFramebuffer();
    this.color_buffer = gl.createRenderbuffer();
    this.depth_buffer = gl.createRenderbuffer();
    this._InitRenderBuffers(gl, width, height);
    this.width = width;
    this.height = height;
  }

  _InitRenderBuffers(gl, width, height) {
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.frame_buffer);
    gl.bindRenderbuffer(gl.RENDERBUFFER, this.color_buffer);
    gl.renderbufferStorage(gl.RENDERBUFFER, gl.RGBA8, width, height);
    gl.framebufferRenderbuffer(gl.DRAW_FRAMEBUFFER, gl.COLOR_ATTACHMENT0,
      gl.RENDERBUFFER, this.color_buffer);
    gl.bindRenderbuffer(gl.RENDERBUFFER, this.depth_buffer);
    gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, width, height);
    gl.framebufferRenderbuffer(gl.DRAW_FRAMEBUFFER, gl.DEPTH_ATTACHMENT,
      gl.RENDERBUFFER, this.depth_buffer);
    gl.drawBuffers(this.attach);
    gl.bindRenderbuffer(gl.RENDERBUFFER, null);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
  }

  Resize(gl, width, height) {
    this._InitRenderBuffers(gl, width, height);
    this.width = width;
    this.height = height;
  }

  Use(gl) { gl.bindFramebuffer(gl.DRAW_FRAMEBUFFER, this.frame_buffer); }

  Display(gl) {
    gl.bindFramebuffer(gl.READ_FRAMEBUFFER, this.frame_buffer);
    gl.bindFramebuffer(gl.DRAW_FRAMEBUFFER, null);
    gl.blitFramebuffer(0, 0, this.width, this.height,
      0, 0, this.width, this.height,
      gl.COLOR_BUFFER_BIT, gl.NEAREST);
  }

  Reset(gl) {
    gl.deleteFramebuffer(this.frame_buffer);
    this.frame_buffer = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.frame_buffer);
    gl.framebufferRenderbuffer(gl.DRAW_FRAMEBUFFER, gl.COLOR_ATTACHMENT0,
      gl.RENDERBUFFER, this.color_buffer);
    gl.framebufferRenderbuffer(gl.DRAW_FRAMEBUFFER, gl.DEPTH_ATTACHMENT,
      gl.RENDERBUFFER, this.depth_buffer);
    gl.drawBuffers(this.attach);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
  }
  
  Delete(gl) {
    gl.deleteRenderbuffer(this.depth_buffer);
    this.depth_buffer = null;
    gl.deleteRenderbuffer(this.render_buffer);
    this.render_buffer = null;
    gl.deleteFramebuffer(this.frame_buffer);
    this.frame_buffer = null;
  }
}