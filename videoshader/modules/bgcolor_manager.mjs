"use strict";
export class BgColorManager {
  r_elm = null;
  g_elm = null;
  b_elm = null;
  a_elm = null;
  r_callback = null;
  g_callback = null;
  b_callback = null;
  a_callback = null;
  
  constructor(init) {
    this.r_elm = init.r_elm;
    this.g_elm = init.g_elm;
    this.b_elm = init.b_elm;
    this.a_elm = init.a_elm;
    this.r_callback = (() => init.r_value.innerText = this.r_elm.value);
    this.g_callback = (() => init.g_value.innerText = this.g_elm.value);
    this.b_callback = (() => init.b_value.innerText = this.b_elm.value);
    this.a_callback = (() => init.a_value.innerText = this.a_elm.value);
    this.r_elm.addEventListener("input", this.r_callback);
    this.g_elm.addEventListener("input", this.g_callback);
    this.b_elm.addEventListener("input", this.b_callback);
    this.a_elm.addEventListener("input", this.a_callback);
  }

  SetValues(rgba) {
    this.r_elm.value = rgba.r;
    this.g_elm.value = rgba.g;
    this.b_elm.value = rgba.b;
    this.a_elm.value = rgba.a;
    this.r_callback();
    this.g_callback();
    this.b_callback();
    this.a_callback();
  }

  GetValues() {
    return {
      r: this.r_elm.value,
      g: this.g_elm.value,
      b: this.b_elm.value,
      a: this.a_elm.value
    }
  }
}