"use strict";
export class InstanceManager {
  model_elm = null;
  count_elm = null;

  constructor(init) {
    this.model_elm = init.model_elm;
    this.count_elm = init.count_elm;
    this.count_elm.addEventListener("change", () => {
      this.count_elm.value = this.count_elm.value > 0 ? this.count_elm.value : 1;
    });
  }
  
  SetModel(model) { this.model_elm.value = model; }
  SetCount(count) { this.count_elm.value = count; }
  GetModel() { return this.model_elm.value; }
  GetCount() { return this.count_elm.value; }
}