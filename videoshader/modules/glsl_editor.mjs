"use strict";

import { kConfig, kLanguage, kTheme, kCompletion } from "../monaco/lng_glsl.mjs";

export class GlslEditor {
  static is_regist = false;
  editor = null;
  element = null;
  resize_obs = null;

  constructor(target_elm) {
    this.element = target_elm;
    if (!GlslEditor.is_regist) GlslEditor._Register();
  }

  static _Register() {
    GlslEditor.is_regist = true;
    require.config({ paths: { vs: "./monaco/min/vs" } });
    require(["./vs/editor/editor.main"], () => {
      monaco.languages.register({ id: "glsl" });
      monaco.languages.setLanguageConfiguration("glsl", kConfig);
      monaco.languages.setMonarchTokensProvider("glsl", kLanguage);
      monaco.languages.registerCompletionItemProvider("glsl", kCompletion);
      monaco.editor.defineTheme("glsl", kTheme);
    });
  }

  _Resize() {
    this.editor.updateOptions({
      minimap: {
        enabled: this.element.getBoundingClientRect().width > 800
      }
    });
    this.editor.layout({
      width: this.element.getBoundingClientRect().width,
      height: this.element.getBoundingClientRect().height
    });
  }

  _Init(value) {
    require(["./vs/editor/editor.main"], () => {
      this.editor = monaco.editor.create(this.element, {
        language: "glsl",
        theme: "glsl",
        value: value,
        minimap: {
          enabled: this.element.getBoundingClientRect().width > 800
        },
        lineNumbersMinChars: 4,
        rulers: [80]
      });
      this.resize_obs = new ResizeObserver(() => this._Resize());
      this.resize_obs.observe(this.element);
    });
  }

  GetValue() { return this.editor !== null ? this.editor.getValue() : ""; }

  SetValue(value) {
    if (this.editor !== null) this.editor.setValue(value);
    else this._Init(value);
  }
  
  ResetPosition() {
    if (this.editor !== null) {
      this.editor.setPosition({
        column: 1,
        lineNumber: 1,
      });
      this.editor.setScrollPosition({
        scrollLeft: 0,
        scrollTop: 0
      });
    }
  }
}