"use strict";

export const kConfig = {
  framebuffer: {
    attach_len: 1,
  },
  buffer_tex: {
    name: "_frag_buffer",
    tex_num: 0,
    attach_num: 1
  },
  video_tex: {
    current: {
      name: "_video",
      tex_num: 1
    },
    previous: {
      name: "_prev_video",
      tex_num: 2
    }
  },
  audio_tex: {
    name: "_audio",
    tex_num: 3
  },
  audio_analyser: {
    size_factor: 9,
    smoothing_time: 0.5
  },
  uniforms: {
    frame_count: "_frame",
    fps: "_fps",
    canvas_resolution: "_canvas_res",
    video_resolution: "_video_res",
    video_duration: "_duration",
    video_time: "_time",
    instance_count: "_instance"
  },
  varyings: [
    "_buffer_a",
    "_buffer_b",
    "_buffer_c",
    "_buffer_d",
    "_buffer_e"
  ]
}