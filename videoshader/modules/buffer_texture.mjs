"use strict";

export class BufferTexture {
  is_use = false;
  texture = new Array(2);
  width = null;
  height = null;
  layout = null;
  read = 0;
  write = 1;
  
  constructor(gl, width, height, layout) {
    let ext = gl.getExtension('EXT_color_buffer_float');
    for (let i = 0; i < this.texture.length; ++i) {
      this.texture[i] = gl.createTexture();
      gl.bindTexture(gl.TEXTURE_2D, this.texture[i]);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F,
        width, height, 0, gl.RGBA, gl.FLOAT, null);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    }
    this.width = width;
    this.height = height;
    this.layout = layout;
  }

  _Flip() {
    this.read = (this.read + 1) % 2;
    this.write = (this.write + 1) % 2;
  }

  Resize(gl, width, height) {
    for (let i = 0; i < this.texture.length; ++i) {
      gl.bindTexture(gl.TEXTURE_2D, this.texture[i]);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F,
        width, height, 0, gl.RGBA, gl.FLOAT, null);
    }
    this.width = width;
    this.height = height;
  }

  Update(gl) {
    if (this.is_use) {
      this._Flip();
      gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0 + this.layout,
        gl.TEXTURE_2D, this.texture[this.write], 0);
    }
  }

  UseTexture(gl, location, number) {
    gl.activeTexture(gl.TEXTURE0 + number);
    gl.bindTexture(gl.TEXTURE_2D, this.texture[this.read]);
    gl.uniform1i(location, number);
  }

  SetUseFlag(flag) { this.is_use = this.is_use || flag; }
  ResetUseFlag() { this.is_use = false; }
  GetReadTexture() { return this.texture[this.read]; }
  GetWriteTexture() { return this.texture[this.write]; }

  Delete(gl) {
    for (let i = 0; i < this.texture.length; ++i) {
      gl.deleteTexture(this.texture[i]);
      this.texture[i] = null;
    }
  }
}