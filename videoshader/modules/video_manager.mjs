"use strict";

class VideoObj {
  parent = null;
  video = null;
  miss_obs = null;
  miss_callback = [];
  resize_obs = null;

  static _CheckCrossOrigin(video) {
    let video_origin = (new URL(video.currentSrc, window.location.href)).origin;
    if (video_origin !== window.location.origin) video.crossOrigin = "anonymous";
  }

  _Init(video) {
    this.parent = video.parentElement;
    this.video = video;
    VideoObj._CheckCrossOrigin(this.video);
    this.miss_obs.observe(this.video, { attributes: true });
  }

  constructor(video) {
    this.miss_obs = new MutationObserver(() => {
      this.miss_obs.disconnect();
      this.video = this.parent.getElementsByTagName("video")[0];
      VideoObj._CheckCrossOrigin(this.video);
      this.parent.prepend(this.video);
      if (this.resize_obs !== null) {
        this.resize_obs.disconnect();
        this.resize_obs.observe(this.video);
      }
      this.miss_obs.observe(this.video, { attributes: true });
    });
    this._Init(video);
  }

  ChangeVideo(video) {
    this.miss_obs.disconnect();
    this._Init(video);
    if (this.miss_callback.length !== 0) {
      this.miss_callback.forEach(callback => callback());
    }
  }

  SetMissCallback(miss_callback) { this.miss_callback.push(miss_callback); }

  SetResizeObs(resize_obs) {
    this.resize_obs = resize_obs;
    this.resize_obs.disconnect();
    this.resize_obs.observe(this.video);
  }

  GetParent() { return this.parent; }
  GetElement() { return this.video; }
  GetStyle() { return this.video.style; }
  GetResolution() { return [this.video.videoWidth, this.video.videoHeight]; }
  GetWidth() { return this.video.videoWidth; }
  GetHeight() { return this.video.videoHeight; }
  GetTime() { return this.video.currentTime; }
  GetDuration() { return this.video.duration; }
  IsPaused() { return this.video.paused; }
}

export class VideoManager {
  videos = null;
  index = null;
  current = null;

  constructor(videos, index = 0) {
    this.videos = videos;
    this.index = index < this.videos.length ? index : this.videos.length - 1;
    this.current = new VideoObj(this.videos[this.index]);
  }

  ChangeIndex(index) {
    this.index = index;
    this.current.ChangeVideo(this.videos[this.index]);
  }
  
  GetLength() { return this.videos.length; }
  GetIndex() { return this.index; }
  GetObject() { return this.current; }
  GetElement() { return this.current.GetElement(); }
  GetStyle() { return this.current.GetStyle(); }
  GetResolution() { return this.current.GetResolution(); }
  GetWidth() { return this.current.GetWidth(); }
  GetHeight() { return this.current.GetHeight(); }
  GetTime() { return this.current.GetTime(); }
  GetDuration() { return this.current.GetDuration(); }
  IsPaused() { return this.current.IsPaused(); }
}