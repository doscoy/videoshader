"use strict";

import { kConfig } from "./config.mjs"
import { VideoManager } from "./video_manager.mjs"
import { MaskCanvas } from "./mask_canvas.mjs"
import { FrameBuffer } from "./framebuffer.mjs"
import { DrawShader, FeedBackShader } from "./shader.mjs"
import { BufferTexture } from "./buffer_texture.mjs"
import { VideoTexture } from "./video_texture.mjs"
import { AudioTexture } from "./audio_texture.mjs"
import { VAO } from "./vao.mjs"
import { BufferVertex } from "./buffer_vertex.mjs"

export class WebGL2Renderer {
  video_mng = null;
  mask = null;
  gl = null;
  frame = null;
  ext_uniforms = new Array();
  draw_shader = null;
  fdbk_shader = null;
  buffer_tex = null;
  video_tex = null;
  audio_tex = null;
  vao = null;
  fdbk_vert = null;
  interval = { fps: null, ms: null, id: null }
  draw_count = null;
  counter = 0;

  constructor(video, init_obj) {
    this.video_mng = new VideoManager(video, init_obj.video_number);
    this.mask = new MaskCanvas(this.video_mng.GetObject(), init_obj.scale);
    let mask_width = this.mask.GetWidth();
    let mask_height = this.mask.GetHeight();
    this._InitWebGL(this.mask.GetWebGL2Context(), init_obj.background);
    this.frame = new FrameBuffer(
      this.gl, mask_width, mask_height, kConfig.framebuffer.attach_len
    );
    this.draw_shader = new DrawShader(
      this.gl, init_obj.name, init_obj.vertex, init_obj.fragment
    );
    this.fdbk_shader = new FeedBackShader(
      this.gl, init_obj.name + "_f", init_obj.feedback, kConfig.varyings);
    this.buffer_tex = new BufferTexture(
      this.gl, mask_width, mask_height, kConfig.buffer_tex.attach_num
    );
    this.video_tex = new VideoTexture(this.gl, this.video_mng.GetObject());
    this.audio_tex = new AudioTexture(this.gl, this.video_mng.GetObject());
    this.ext_uniforms = init_obj.uniforms;
    this._InitShader(this.draw_shader);
    this._TextureRegist2Shader(this.draw_shader);
    this._InitShader(this.fdbk_shader);
    this._TextureRegist2Shader(this.fdbk_shader);
    this.vao = VAO.Get(this.gl, init_obj.model);
    this.interval.fps = init_obj.fps;
    this.interval.ms = 1000.0 / this.interval.fps;
    this.draw_count = init_obj.count;
    this.fdbk_vert = new BufferVertex(
      this.gl, this.fdbk_shader.IsReady(), this.fdbk_shader.GetVeryingsInfo(),
      this.draw_count, this.vao.GetIndexTail()
    );
    this._InitResizeCallback();
    this._Draw();
  }

  _InitWebGL(gl, background) {
    this.gl = gl
    this.gl.clearColor(background.r, background.g, background.b, background.a);
    this.gl.clearDepth(1.0);
    this.gl.enable(this.gl.DEPTH_TEST);
    this.gl.depthFunc(this.gl.LEQUAL);
    this.gl.enable(this.gl.CULL_FACE);
  }

  _InitShader(shader) {
    if (shader.IsReady()) {
      shader.SetUniformCallback(this.gl, kConfig.uniforms.frame_count,
        () => { return this.counter; });
      shader.SetUniformCallback(this.gl, kConfig.uniforms.fps,
        () => { return this.interval.fps; });
      shader.SetUniformCallback(this.gl, kConfig.uniforms.canvas_resolution,
        () => this.mask.GetResolution());
      shader.SetUniformCallback(this.gl, kConfig.uniforms.video_resolution,
        () => this.video_mng.GetResolution());
      shader.SetUniformCallback(this.gl, kConfig.uniforms.video_duration,
        () => this.video_mng.GetDuration());
      shader.SetUniformCallback(this.gl, kConfig.uniforms.video_time,
        () => this.video_mng.GetTime());
      shader.SetUniformCallback(this.gl, kConfig.uniforms.instance_count,
        () => { return this.draw_count; });
      for (let i = 0; i < this.ext_uniforms.length; ++i) {
        shader.SetUniformCallback(this.gl, this.ext_uniforms[i].name, () => {
          return this.ext_uniforms[i].values;
        });
      }
    }
  }

  _TextureRegist2Shader(shader) {
    if (shader.IsReady()) {
      this.buffer_tex.SetUseFlag(
        shader.SetTextureCallback(
          this.gl, kConfig.buffer_tex.name, kConfig.buffer_tex.tex_num,
          (gl, loc, num) => this.buffer_tex.UseTexture(gl, loc, num)
        )
      );
      this.video_tex.SetUseFlag(
        shader.SetTextureCallback(
          this.gl, kConfig.video_tex.current.name, kConfig.video_tex.current.tex_num,
          (gl, loc, num) => this.video_tex.UseCurrentTexture(gl, loc, num)
        )
      )
      this.video_tex.SetUseFlag(
        shader.SetTextureCallback(
          this.gl, kConfig.video_tex.previous.name, kConfig.video_tex.previous.tex_num,
          (gl, loc, num) => this.video_tex.UsePreviousTexture(gl, loc, num)
        )
      );
      this.audio_tex.SetUseFlag(
        shader.SetTextureCallback(
          this.gl, kConfig.audio_tex.name, kConfig.audio_tex.tex_num,
          (gl, loc, num) => this.audio_tex.UseTexture(gl, loc, num)
        )
      );
    }
  }

  _InitResizeCallback() {
    this.mask.SetResizeCallback((width, height) => {
      this.ResizeGLViewport(width, height);
    });
    this.mask.SetResizeCallback((width, height) => {
      this.frame.Resize(this.gl, width, height);
    });
    this.mask.SetResizeCallback((width, height) => {
      this.buffer_tex.Resize(this.gl, width, height);
    });
  }

  _Draw() {
    this.frame.Use(this.gl);
    this.buffer_tex.Update(this.gl);
    this.video_tex.Update(this.gl);
    this.audio_tex.Update(this.gl);
    this.fdbk_shader.Use(this.gl);
    this.fdbk_shader.UseUniformCallback(this.gl);
    this.fdbk_shader.UseTextureCallback(this.gl);
    this.fdbk_vert.Update(this.gl);
    this.draw_shader.Use(this.gl);
    this.draw_shader.UseUniformCallback(this.gl);
    this.draw_shader.UseTextureCallback(this.gl);
    this.vao.Use(this.gl);
    this.fdbk_vert.Use(this.gl);
    this.vao.Draw(this.gl, this.draw_count);
    this.frame.Display(this.gl);
    this.gl.flush();
    ++this.counter;
  }

  IntervalDraw() {
    if (this.interval.id === null && this.CheckShaderIsReady()) {
      this._Draw();
      this.mask.SetVisible();
      this.interval.id = setInterval(() => {
        if (!this.video_mng.IsPaused()) this._Draw();
      }, this.interval.ms);
    }
  }

  IntervalStop() {
    if (this.interval.id !== null) {
      this.mask.SetHidden();
      clearInterval(this.interval.id);
      this.interval.id = null;
      this.counter = 0;
    }
  }

  ChangeShader(code) {
    let is_drawing = this.interval.id != null;
    if (is_drawing) this.IntervalStop();
    this.gl.clearColor(
      code.background.r, code.background.g, code.background.b, code.background.a
    );
    this.draw_count = code.count;
    this.counter = 0;
    this.frame.Reset(this.gl);
    this.draw_shader.Delete(this.gl);
    this.fdbk_shader.Delete(this.gl);
    this.vao.Delete(this.gl);
    this.fdbk_vert.Delete(this.gl);
    this.buffer_tex.ResetUseFlag();
    this.video_tex.ResetUseFlag();
    this.audio_tex.ResetUseFlag();
    this.draw_shader = new DrawShader(
      this.gl, code.name, code.vertex, code.fragment
    );
    this.fdbk_shader = new FeedBackShader(
      this.gl, code.name, code.feedback, kConfig.varyings
    );
    this.ext_uniforms = code.uniforms;
    this._InitShader(this.draw_shader);
    this._TextureRegist2Shader(this.draw_shader);
    this._InitShader(this.fdbk_shader);
    this._TextureRegist2Shader(this.fdbk_shader);
    this.vao = VAO.Get(this.gl, code.model);
    this.fdbk_vert = new BufferVertex(
      this.gl, this.fdbk_shader.IsReady(), this.fdbk_shader.GetVeryingsInfo(),
      this.draw_count, this.vao.GetIndexTail()
    );
    if (this.draw_shader.IsReady()) this._Draw();
    if (is_drawing) this.IntervalDraw();
  }

  ResizeGLViewport(width, height) { this.gl.viewport(0, 0, width, height); }
  ChangeVideoIndex(index) { this.video_mng.ChangeIndex(index); }
  ChangeScale(scale) { this.mask.ChangeScale(scale); }

  ChangeFps(fps) {
    this.interval.fps = fps;
    this.interval.ms = 1000.0 / this.interval.fps;
    if (this.interval.id != null) {
      this.IntervalStop();
      this.IntervalDraw();
    }
  }

  ChangeBgColor(rgba) { this.gl.clearColor(rgba.r, rgba.g, rgba.b, rgba.a); }
  ChangeDrawCount(count) { this.draw_count = count; }
  IsDraw() { return this.interval.id != null; }

  CheckShaderMessage() {
    let fdbk_msg = this.fdbk_shader.GetCompileMessage();
    let draw_msg = this.draw_shader.GetCompileMessage();
    return fdbk_msg + "\n" + draw_msg;
  }

  CheckShaderIsReady() {
    let fdbk_flg = this.fdbk_shader.IsReady() === this.fdbk_shader.IsUse();
    let draw_flg = this.draw_shader.IsReady() === this.draw_shader.IsUse();
    return fdbk_flg && draw_flg;
  }

  GetFrameCount() { return this.counter; }
  GetInstanceCount() { return this.draw_count; }
  GetExtUniforms() { return this.ext_uniforms; }
  GetVideoLength() { return this.video_mng.GetLength(); }
  GetCurrentVideoIndex() { return this.video_mng.GetIndex(); }
  GetCurrentEffect() { return this.draw_shader.GetName(); }
  UpdateExtUniformsVales(data) { this.ext_uniforms = data; }
}

export class PreviewRenderer extends WebGL2Renderer {
  uniforms = null;
  background = null;
  instance = null;

  constructor(uniforms, background, instance) {
    const kDummy = {
      video_number: 0,
      feedback: "none",
      vertex: "#version 300 es\nprecision mediump float;void main(void){gl_Position=vec4(0.,0.,0.,1.);}",
      fragment: "#version 300 es\nprecision mediump float;layout (location = 0) out vec4 _color;void main(void){_color=vec4(0.,0.,0.,1.);}",
      fps: 30,
      scale: 1.0,
      uniforms: [],
      background: { r: 0.0, g: 0.0, b: 0.0, a: 1.0 },
      model: "panel",
      count: 1
    };
    super(document.getElementsByTagName("video"), kDummy);
    this.uniforms = uniforms;
    this.background = background;
    this.instance = instance;
  }

  _InitShader(shader) {
    if (shader.IsReady()) {
      shader.SetUniformCallback(this.gl, kConfig.uniforms.frame_count,
        () => { return this.counter; });
      shader.SetUniformCallback(this.gl, kConfig.uniforms.fps,
        () => { return this.interval.fps; });
      shader.SetUniformCallback(this.gl, kConfig.uniforms.canvas_resolution,
        () => this.mask.GetResolution());
      shader.SetUniformCallback(this.gl, kConfig.uniforms.video_resolution,
        () => this.video_mng.GetResolution());
      shader.SetUniformCallback(this.gl, kConfig.uniforms.video_duration,
        () => this.video_mng.GetDuration());
      shader.SetUniformCallback(this.gl, kConfig.uniforms.video_time,
        () => this.video_mng.GetTime());
      shader.SetUniformCallback(this.gl, kConfig.uniforms.instance_count,
        () => { return this.draw_count; });
    }
  }

  _Draw() {
    this.frame.Use(this.gl);
    this.buffer_tex.Update(this.gl);
    this.video_tex.Update(this.gl);
    this.audio_tex.Update(this.gl);
    this.fdbk_shader.Use(this.gl);
    this.fdbk_shader.UseUniformCallback(this.gl);
    this.fdbk_shader.UseUniforms(this.gl, this.ext_uniforms);
    this.fdbk_shader.UseTextureCallback(this.gl);
    this.fdbk_vert.Update(this.gl);
    this.draw_shader.Use(this.gl);
    this.draw_shader.UseUniformCallback(this.gl);
    this.draw_shader.UseUniforms(this.gl, this.ext_uniforms);
    this.draw_shader.UseTextureCallback(this.gl);
    this.vao.Use(this.gl);
    this.fdbk_vert.Use(this.gl);
    this.vao.Draw(this.gl, this.draw_count);
    this.frame.Display(this.gl);
    this.gl.flush();
    ++this.counter;
  }
  
  IntervalDraw() {
    if (this.interval.id === null && this.CheckShaderIsReady()) {
      this._Draw();
      this.mask.SetVisible();
      this.interval.id = setInterval(() => {
        if (!this.video_mng.IsPaused()) {
          this.UpdateExtUniformsVales(this.uniforms.GetDatas());
          this.ChangeBgColor(this.background.GetValues());
          this._Draw();
        }
      }, this.interval.ms);
    }
  }
}