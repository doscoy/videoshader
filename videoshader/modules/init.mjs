'use strict';

export const kInitData = {
  "current": "wave",
  "effects": [
    {
      "background": {
        "a": "1",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "gameboy",
      "model": "panel",
      "name": "gameboy",
      "uniforms": [
        {
          "dimension": "1",
          "max": "1",
          "min": "0",
          "name": "range",
          "step": "0.01",
          "type": "float",
          "values": [
            "0.5"
          ]
        },
        {
          "dimension": "3",
          "max": "1",
          "min": "0",
          "name": "base_color",
          "step": "0.01",
          "type": "rgb",
          "values": [
            "0.45",
            "0.5",
            "0.1"
          ]
        }
      ],
      "vert": "panel_screen"
    },
    {
      "background": {
        "a": "1",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "pencil",
      "model": "panel",
      "name": "pencil",
      "uniforms": [
        {
          "dimension": 1,
          "max": "36",
          "min": "2",
          "name": "tone",
          "step": "1",
          "type": "n",
          "values": [
            "10"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "threshold",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.25"
          ]
        }
      ],
      "vert": "panel_screen"
    },
    {
      "background": {
        "a": "0",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "visualizer",
      "model": "panel",
      "name": "visualizer",
      "uniforms": [
        {
          "dimension": 4,
          "max": "1",
          "min": "0",
          "name": "color",
          "step": "0.01",
          "type": "rgba",
          "values": [
            "0",
            "1",
            "0",
            "1"
          ]
        },
        {
          "dimension": 1,
          "max": "0.05",
          "min": "0.0001",
          "name": "line",
          "step": "0.0001",
          "type": "n",
          "values": [
            "0.01"
          ]
        },
        {
          "dimension": 1,
          "max": "10",
          "min": "1",
          "name": "sharpness",
          "step": "1",
          "type": "n",
          "values": [
            "3"
          ]
        }
      ],
      "vert": "panel_screen"
    },
    {
      "background": {
        "a": "1",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "quake",
      "model": "panel",
      "name": "quake",
      "uniforms": [
        {
          "dimension": 3,
          "max": "0.5",
          "min": "-0.5",
          "name": "scale",
          "step": "0.01",
          "type": "rgb",
          "values": [
            "-0.05",
            "0.05",
            "0.1"
          ]
        },
        {
          "dimension": 3,
          "max": "22050",
          "min": "0",
          "name": "target_freq",
          "step": "50",
          "type": "rgb",
          "values": [
            "1500",
            "7000",
            "5000"
          ]
        },
        {
          "dimension": 3,
          "max": "2",
          "min": "0",
          "name": "direction",
          "step": "1",
          "type": "rgb",
          "values": [
            "0",
            "2",
            "1"
          ]
        }
      ],
      "vert": "panel_screen"
    },
    {
      "background": {
        "a": "1",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "slice",
      "model": "panel",
      "name": "slice",
      "uniforms": [
        {
          "dimension": 3,
          "max": "1",
          "min": "-1",
          "name": "scale",
          "step": "0.01",
          "type": "rgb",
          "values": [
            "0.5",
            "-0.25",
            "0.75"
          ]
        },
        {
          "dimension": 3,
          "max": "1",
          "min": "0",
          "name": "shift",
          "step": "0.03125",
          "type": "rgb",
          "values": [
            "0.28125",
            "0.5",
            "0.71875"
          ]
        },
        {
          "dimension": 3,
          "max": "2",
          "min": "0",
          "name": "direction",
          "step": "1",
          "type": "rgb",
          "values": [
            "2",
            "0",
            "1"
          ]
        }
      ],
      "vert": "panel_screen"
    },
    {
      "background": {
        "a": "1",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "9216",
      "fdbk": "imprint",
      "frag": "vertex_color",
      "model": "fit_cube",
      "name": "imprint",
      "uniforms": [
        {
          "dimension": 1,
          "max": "10",
          "min": "0",
          "name": "speed",
          "step": "0.01",
          "type": "n",
          "values": [
            "6.28"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "decay",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.1"
          ]
        },
        {
          "dimension": 3,
          "max": "1",
          "min": "-1",
          "name": "light",
          "step": "0.01",
          "type": "xyz",
          "values": [
            "0",
            "0",
            "1"
          ]
        }
      ],
      "vert": "imprint"
    },
    {
      "background": {
        "a": "0",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "mirror",
      "model": "icosahedron",
      "name": "mirror",
      "uniforms": [
        {
          "dimension": 3,
          "max": "5",
          "min": "-5",
          "name": "eye",
          "step": "0.01",
          "type": "xyz",
          "values": [
            "0",
            "0",
            "3"
          ]
        },
        {
          "dimension": 3,
          "max": "5",
          "min": "-5",
          "name": "light",
          "step": "0.01",
          "type": "xyz",
          "values": [
            "-2.5",
            "-1.25",
            "2.5"
          ]
        },
        {
          "dimension": 1,
          "max": "100",
          "min": "1",
          "name": "shininess",
          "step": "1",
          "type": "n",
          "values": [
            "60"
          ]
        }
      ],
      "vert": "instance_rot"
    },
    {
      "background": {
        "a": "0",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "loser",
      "model": "panel",
      "name": "loser",
      "uniforms": [
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "range",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.15"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "intensity",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.65"
          ]
        }
      ],
      "vert": "panel_screen"
    },
    {
      "background": {
        "a": "1",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "10000",
      "fdbk": "break",
      "frag": "break",
      "model": "cube",
      "name": "break",
      "uniforms": [],
      "vert": "break"
    },
    {
      "background": {
        "a": "0",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "10000",
      "fdbk": "wave",
      "frag": "wave",
      "model": "cube",
      "name": "wave",
      "uniforms": [
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "smoothing",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.5"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "alpha",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.9"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "size",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.75"
          ]
        },
        {
          "dimension": 1,
          "max": "10",
          "min": "1",
          "name": "sharpness",
          "step": "1",
          "type": "n",
          "values": [
            "2"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "mode",
          "step": "1",
          "type": "n",
          "values": [
            "0"
          ]
        }
      ],
      "vert": "wave"
    },
    {
      "background": {
        "a": "1",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "pc8801",
      "model": "panel",
      "name": "pc8801",
      "uniforms": [
        {
          "dimension": "1",
          "max": "1",
          "min": "0",
          "name": "color_thr",
          "step": "0.01",
          "type": "float",
          "values": [
            "0.5"
          ]
        },
        {
          "dimension": "1",
          "max": "1",
          "min": "0",
          "name": "color_range",
          "step": "0.01",
          "type": "float",
          "values": [
            "0.5"
          ]
        },
        {
          "dimension": "1",
          "max": "1",
          "min": "0",
          "name": "edge_thr",
          "step": "0.01",
          "type": "float",
          "values": [
            "0.5"
          ]
        }
      ],
      "vert": "panel_screen"
    },
    {
      "background": {
        "a": "1",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "crt",
      "model": "panel",
      "name": "crt",
      "uniforms": [
        {
          "dimension": 1,
          "max": "8",
          "min": "1",
          "name": "size",
          "step": "1",
          "type": "n",
          "values": [
            "1"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "bright",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.5"
          ]
        },
        {
          "dimension": 3,
          "max": "3",
          "min": "-3",
          "name": "color_shift_x",
          "step": "0.1",
          "type": "rgb",
          "values": [
            "-1.5",
            "0",
            "1.5"
          ]
        },
        {
          "dimension": 3,
          "max": "3",
          "min": "-3",
          "name": "color_shift_y",
          "step": "0.1",
          "type": "rgb",
          "values": [
            "-1",
            "0",
            "1"
          ]
        },
        {
          "dimension": 1,
          "max": "0.03",
          "min": "0",
          "name": "blur",
          "step": "0.001",
          "type": "n",
          "values": [
            "0.02"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "saturation",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.7"
          ]
        }
      ],
      "vert": "panel_screen"
    },
    {
      "background": {
        "a": "1",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "life",
      "model": "panel",
      "name": "life",
      "uniforms": [
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "born_thr",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.5"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "brightness",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.5"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "decay",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.25"
          ]
        }
      ],
      "vert": "panel_screen"
    },
    {
      "background": {
        "a": "1",
        "b": "0",
        "g": "0",
        "r": "0"
      },
      "count": "1",
      "fdbk": "@none",
      "frag": "burn-in",
      "model": "panel",
      "name": "burn-in",
      "uniforms": [
        {
          "dimension": "1",
          "max": "1",
          "min": "0",
          "name": "burn_thr",
          "step": "0.01",
          "type": "float",
          "values": [
            "0.5"
          ]
        },
        {
          "dimension": 1,
          "max": "1",
          "min": "0",
          "name": "blur",
          "step": "0.01",
          "type": "n",
          "values": [
            "0.05"
          ]
        }
      ],
      "vert": "panel_screen"
    }
  ],
  "fdbk_codes": [
    {
      "code": "none",
      "name": "@none"
    },
    {
      "code": "#version 300 es\n\nlayout (location = 1) in vec3 _rot;\n\nuniform sampler2D _video;\nuniform sampler2D _prev_video;\n\nuniform float speed;\nuniform float decay;\n\nconst float kNum = 8.0;\nconst vec2 kRes = vec2(16.0, 9.0);\n\nout vec2 _buffer_a;\nout vec3 _buffer_b;\nout vec3 _buffer_c;\n\nvec2 Stack2D(in float count, in vec2 res) {\n  return vec2(mod(count, res.x), floor(count / res.x)) / (res - 1.0);\n}\n\nvoid main(void) {\n  vec2 res = kRes * kNum;\n  vec2 uv = Stack2D(float(gl_VertexID), res);\n  vec3 rot = _rot;\n  vec3 color = texture(_video, uv).rgb;\n  rot += (color - texture(_prev_video, uv).rgb) * speed;\n  rot -= decay * sign(rot);\n  rot *= step(decay, rot) + step(rot, -vec3(decay));\n\n  _buffer_a = uv * 2.0 - 1.0;\n  _buffer_b = rot;\n  _buffer_c = color;\n}",
      "name": "imprint"
    },
    {
      "code": "#version 300 es\n\nlayout (location = 0) in vec3 _pos;\nlayout (location = 1) in vec3 _rot;\nlayout (location = 2) in vec3 _acc;\n\nuniform float _frame;\nuniform float _duration;\nuniform float _time;\n\nout vec3 _buffer_a;\nout vec3 _buffer_b;\nout vec3 _buffer_c;\n\nconst float kNum = 8.0;\nconst vec2 kRes = vec2(16.0, 9.0);\nconst float kClip = 2.0;\n\nvec2 Stack2D(in float count, in vec2 res) {\n  return vec2(mod(count, res.x), floor(count / res.x)) / (res - 1.0);\n}\n\nfloat Rand(vec2 co) {\n  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);\n}\n\nvoid main(void) {\n  vec3 pos = _pos;\n  vec3 acc = _acc;\n  vec3 rot = _rot;\n  vec2 res = kRes * kNum;\n  \n  if (_frame < 1.0)\n  {\n    vec2 uv = Stack2D(float(gl_VertexID), res) * 2.0 - 1.0;\n    pos = vec3(uv.x, uv.y, -1.5);\n    float rnd_factor = _duration + _time;\n    acc = vec3( Rand(vec2(uv.y, rnd_factor * 1.3)) * 2.0 - 1.0,\n                Rand(vec2(uv.x, rnd_factor * 1.7)) * 2.0 - 1.0,\n                Rand(uv * rnd_factor) * 2.0 - 1.0) * 0.05;\n    rot = acc * 10.0;\n  }\n  pos += acc;\n  rot += acc * 10.0;\n  acc = acc * -step(vec3(kClip), pos)\n      + acc * -step(pos, vec3(-kClip))\n      + acc * step(vec3(-kClip), pos) * step(pos, vec3(kClip));\n  pos = clamp(pos, -kClip, kClip);\n  rot = mod(rot, 6.283185);\n\n  _buffer_a = pos;\n  _buffer_b = rot;\n  _buffer_c = acc;\n}",
      "name": "break"
    },
    {
      "code": "#version 300 es\n\nlayout (location = 1) in float _wave; \n\nuniform sampler2D _audio;\n\nuniform float smoothing;\nuniform float sharpness;\nuniform float mode;\n\nout vec2 _buffer_a;\nout float _buffer_b;\n\nconst float kAN = 512.0;\nconst float kAW = 128.0; \nconst vec2 kRes = vec2(100.0);\n\nvec2 Stack2D(in float count, in vec2 res) {\n  return vec2(mod(count, res.x), floor(count / res.x)) / (res - 1.0);\n}\n\nfloat IndexSquaring(in float i, in float v, in float w) {\n  float thr = w / v;\n  float si = floor(i * w) / w;\n  return step(thr, i) * si * si + step(i, thr) * floor(i * thr * v) / v;\n}\n\nvoid main(void) {\n  vec2 uv = Stack2D(float(gl_VertexID), kRes);\n  vec2 pos = uv * 2.0 - 1.0;\n  vec2 adj = kRes / kAW;\n  vec2 i = vec2(IndexSquaring(uv.x * adj.x, kAN, kAW),\n                IndexSquaring(uv.y * adj.y, kAN, kAW));\n  float lch = texture(_audio, vec2(i.x, 0.01)).r;\n  float rch = texture(_audio, vec2(i.y, 0.26)).r;\n  float wave = step(mode, 0.5) * (lch + rch)\n             + step(0.5, mode) * (step(0.000001, lch * rch) * (lch + rch));\n  wave = pow(wave * 0.5, sharpness);\n  wave = mix(wave, _wave, smoothing * 0.5);\n\n  _buffer_a = pos;\n  _buffer_b = wave;\n}",
      "name": "wave"
    }
  ],
  "fps": "30",
  "frag_codes": [
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform sampler2D _video;\nuniform vec2 _canvas_res;\nuniform vec2 _video_res;\n\nuniform float range;\nuniform vec3 base_color;\n\nconst vec2 kGBRes = vec2(160.0, 144.0);\n\nlayout (location = 0) out vec4 _color;\n\nfloat FixAspectRatioCoord(inout vec2 coord, inout vec2 res, in vec2 asp) {\n  float ratio = min(res.x / asp.x, res.y / asp.y);\n  vec2 fix_res = floor(asp * ratio);\n  vec2 fix_coord = coord - floor((res - fix_res) * 0.5);\n  vec2 mask = step(0.0, fix_coord) * step(fix_coord, fix_res);\n  res = fix_res;\n  coord = fix_coord;\n  return mask.x * mask.y;\n}\n\nvec2 SetPixelateCoord(inout vec2 coord, inout vec2 res, in vec2 size) {\n  vec2 ratio = res / size;\n  vec2 correct = size - floor(fract(ratio) * size * 0.5);\n  vec2 sub_coord = (coord + correct) / size;\n  coord = floor(sub_coord);\n  res = floor(ratio + step(0.0001, correct));\n  return fract(sub_coord) * size;\n}\n\nfloat GetLuminance(in vec3 color) {\n  return 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;\n}\n\nfloat GetBayerThr4x4(vec2 coord) {\n  const mat4 bayer = mat4(0.000000, 0.750000, 0.187500, 0.937500,\n                          0.500000, 0.250000, 0.687500, 0.437500,\n                          0.125000, 0.875000, 0.062500, 0.812500,\n                          0.625000, 0.375000, 0.562500, 0.312500);\n  return bayer[int(mod(coord.y, 4.0))][int(mod(coord.x, 4.0))];\n}\n\nvec3 RevColorByBayerThr(in vec3 rgb, in float threshold, in float range) {\n  return rgb + (threshold - 0.5) * range;\n}\n\nvec3 GetTone(in vec3 v3, in float tone_num) {\n  vec3 n = v3 * (tone_num - 1.0);\n  vec3 fl = floor(n);\n  return clamp((fl + step(fl + 0.5, n)) / (tone_num - 1.0), 0.0, 1.0);\n}\n\nvoid main(void) {\n  vec2 res = _canvas_res;\n  vec2 coord = gl_FragCoord.xy;\n  float mask1 = FixAspectRatioCoord(coord, res, kGBRes);\n  float scale = res.x > res.y ? ceil(res.x / kGBRes.x) : ceil(res.y / kGBRes.y);\n  float mask2 = FixAspectRatioCoord(coord, res, _video_res);\n  SetPixelateCoord(coord, res, vec2(scale));\n  vec3 color = vec3(GetLuminance(texture(_video, coord / res).rgb));\n  vec3 thr = RevColorByBayerThr(color.rgb, GetBayerThr4x4(coord), range);\n  color = (GetTone(thr, 4.0) * 0.75 + 0.25) * base_color;\n  _color = vec4(color * mask2 + (mask1 - mask2) * base_color * 0.25, 1.0);\n}",
      "name": "gameboy"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform sampler2D _video;\nuniform vec2 _canvas_res;\nuniform vec2 _video_res;\n\nuniform float tone;\nuniform float threshold;\n\nlayout (location = 0) out vec4 _color;\n\nfloat FixAspectRatioCoord(inout vec2 coord, inout vec2 res, in vec2 asp) {\n  float ratio = min(res.x / asp.x, res.y / asp.y);\n  vec2 fix_res = floor(asp * ratio);\n  vec2 fix_coord = coord - floor((res - fix_res) * 0.5);\n  vec2 mask = step(0.0, fix_coord) * step(fix_coord, fix_res);\n  res = fix_res;\n  coord = fix_coord;\n  return mask.x * mask.y;\n}\n\nmat3 GetTone(in mat3 filt_mat, in float tone_num) {\n  mat3 tone;\n  for(int i = 0; i < 3; ++i)\n  {\n    for(int j = 0; j < 3; ++j)\n    {\n      float n = filt_mat[i][j] * (tone_num - 1.0);\n      float fl = floor(n);\n      tone[i][j] = clamp((fl + step(fl + 0.5, n)) / (tone_num - 1.0), 0.0, 1.0);\n    }\n  }\n  return tone;\n}\n\nfloat GetLuminance(in vec3 color) {\n  return 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;\n}\n\nmat3 GetLuminanceMatrix(in sampler2D tex, in vec2 uv) {\n  mat3 luminance;\n  luminance[0][0] = GetLuminance(textureOffset(tex, uv, ivec2(-1,  1)).rgb);\n  luminance[0][1] = GetLuminance(textureOffset(tex, uv, ivec2(-1,  0)).rgb);\n  luminance[0][2] = GetLuminance(textureOffset(tex, uv, ivec2(-1, -1)).rgb);\n  luminance[1][0] = GetLuminance(textureOffset(tex, uv, ivec2( 0,  1)).rgb);\n  luminance[1][1] = GetLuminance(textureOffset(tex, uv, ivec2( 0,  0)).rgb);\n  luminance[1][2] = GetLuminance(textureOffset(tex, uv, ivec2( 0, -1)).rgb);\n  luminance[2][0] = GetLuminance(textureOffset(tex, uv, ivec2( 1,  1)).rgb);\n  luminance[2][1] = GetLuminance(textureOffset(tex, uv, ivec2( 1,  0)).rgb);\n  luminance[2][2] = GetLuminance(textureOffset(tex, uv, ivec2( 1, -1)).rgb);\n  return luminance;\n}\n\nfloat GetEdgeSobel(in mat3 filt_mat, in float threshold) {\n  float dx = filt_mat[0][0]\n            +filt_mat[0][1] * 2.0\n            +filt_mat[0][2]\n            -filt_mat[2][1]\n            -filt_mat[2][1] * 2.0\n            -filt_mat[2][2];\n  float dy = filt_mat[0][0]\n            +filt_mat[1][0] * 2.0\n            +filt_mat[2][0]\n            -filt_mat[0][2]\n            -filt_mat[1][2] * 2.0\n            -filt_mat[2][2];\n  return step(threshold * threshold, dx * dx + dy * dy);\n}\n\nvoid main(void) {\n  vec2 coord = gl_FragCoord.xy;\n  vec2 res = _canvas_res;\n  float mask = FixAspectRatioCoord(coord, res, _video_res);\n  vec2 uv = coord / res;\n  mat3 lumi_mat = GetLuminanceMatrix(_video, uv);\n  lumi_mat = GetTone(lumi_mat, tone);\n  float edge = GetEdgeSobel(lumi_mat, threshold);\n  _color = vec4(vec3(1.0 - edge + lumi_mat[1][1]) * mask, 1.0);\n}",
      "name": "pencil"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform sampler2D _video;\nuniform sampler2D _audio;\nuniform vec2 _canvas_res;\nuniform vec2 _video_res;\n\nuniform vec3 scale;\nuniform vec3 target_freq;\nuniform vec3 direction;\n\nlayout (location = 0) out vec4 _color;\n\nfloat FixAspectRatioCoord(inout vec2 coord, inout vec2 res, in vec2 asp) {\n  float ratio = min(res.x / asp.x, res.y / asp.y);\n  vec2 fix_res = floor(asp * ratio);\n  vec2 fix_coord = coord - floor((res - fix_res) * 0.5);\n  vec2 mask = step(0.0, fix_coord) * step(fix_coord, fix_res);\n  res = fix_res;\n  coord = fix_coord;\n  return mask.x * mask.y;\n}\n\nvec2 GetSpice(in float direction, in float scale) {\n  return vec2(sign((direction - 0.5) * (direction - 1.5)),\n              sign((direction - 1.5) * (direction - 2.5))) * scale;\n}\n\nvec2 GetSpectrum(in float i) {\n  vec2 spectrum = vec2(texture(_audio, vec2(i, 0.01)).r,\n                       texture(_audio, vec2(i, 0.26)).r);\n  return spectrum * spectrum * spectrum;\n}\n\nfloat GetOutRangeMask(in vec2 uv) {\n  vec2 mask = step(vec2(0.0), uv) * step(uv, vec2(1.0));\n  return  mask.x * mask.y;\n}\n\nvoid main(void) {\n  vec2 coord = gl_FragCoord.xy;\n  vec2 res = _canvas_res;\n  FixAspectRatioCoord(coord, res, _video_res);\n  vec2 uv = coord / res;\n  vec3 i = target_freq / 22050.0;\n  vec2 spice_r = GetSpice(direction.r, scale.r);\n  vec2 spice_g = GetSpice(direction.g, scale.g);\n  vec2 spice_b = GetSpice(direction.b, scale.b);\n  vec2 spectrum_r = GetSpectrum(i.r);\n  vec2 spectrum_g = GetSpectrum(i.g);\n  vec2 spectrum_b = GetSpectrum(i.b);\n  vec2 uv_r = uv + spectrum_r * spice_r;\n  vec2 uv_g = uv + spectrum_g * spice_g;\n  vec2 uv_b = uv + spectrum_b * spice_b;\n  vec3 mask = vec3(GetOutRangeMask(uv_r),\n                   GetOutRangeMask(uv_g),\n                   GetOutRangeMask(uv_b));\n  vec3 color = vec3(texture(_video, uv_r).r * mask.r,\n                    texture(_video, uv_g).g * mask.g,\n                    texture(_video, uv_b).b * mask.b);\n  _color = vec4(color, 1.0);\n}",
      "name": "quake"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform sampler2D _video;\nuniform sampler2D _audio;\nuniform vec2 _canvas_res;\nuniform vec2 _video_res;\n\nuniform vec3 scale;\nuniform vec3 shift;\nuniform vec3 direction;\n\nlayout (location = 0) out vec4 _color;\n\nfloat FixAspectRatioCoord(inout vec2 coord, inout vec2 res, in vec2 asp) {\n  float ratio = min(res.x / asp.x, res.y / asp.y);\n  vec2 fix_res = floor(asp * ratio);\n  vec2 fix_coord = coord - floor((res - fix_res) * 0.5);\n  vec2 mask = step(0.0, fix_coord) * step(fix_coord, fix_res);\n  res = fix_res;\n  coord = fix_coord;\n  return mask.x * mask.y;\n}\n\nvec2 GetSpice(in float direction, in float scale) {\n  return vec2(sign((direction - 0.5) * (direction - 1.5)),\n              sign((direction - 1.5) * (direction - 2.5))) * scale;\n}\n\nvec2 GetWave(in vec2 uv) {\n  return (vec2(texture(_audio, vec2(uv.x, 0.76)).r,\n               texture(_audio, vec2(uv.y, 0.51)).r) - 0.5) * 2.0;\n}\n\nfloat GetOutRangeMask(in vec2 uv) {\n  vec2 mask = step(vec2(0.0), uv) * step(uv, vec2(1.0));\n  return  mask.x * mask.y;\n}\n\nvoid main(void) {\n  vec2 coord = gl_FragCoord.xy;\n  vec2 res = _canvas_res;\n  FixAspectRatioCoord(coord, res, _video_res);\n  vec2 uv = coord / res;\n  float asp = max(_video_res.x, _video_res.y) / min(_video_res.x, _video_res.y);\n  vec2 fix = vec2(step(_video_res.x, _video_res.y) * (asp - 1.0) + 1.0,\n                  step(_video_res.y, _video_res.x) * (asp - 1.0) + 1.0);\n  vec2 i_r = mod(uv + shift.r, 1.0);\n  vec2 i_g = mod(uv + shift.g, 1.0);\n  vec2 i_b = mod(uv + shift.b, 1.0);\n  vec2 spice_r = GetSpice(direction.r, scale.r);\n  vec2 spice_g = GetSpice(direction.g, scale.g);\n  vec2 spice_b = GetSpice(direction.b, scale.b);\n  vec2 nw_r = GetWave(i_r);\n  vec2 nw_g = GetWave(i_g);\n  vec2 nw_b = GetWave(i_b);\n  vec2 uv_r = uv + nw_r * nw_r * nw_r * spice_r * fix;\n  vec2 uv_g = uv + nw_g * nw_g * nw_g * spice_g * fix;\n  vec2 uv_b = uv + nw_b * nw_b * nw_b * spice_b * fix;\n  vec3 mask = vec3(GetOutRangeMask(uv_r),\n                   GetOutRangeMask(uv_g),\n                   GetOutRangeMask(uv_b));\n  vec3 color = vec3(texture(_video, uv_r).r * mask.r,\n                    texture(_video, uv_g).g * mask.g,\n                    texture(_video, uv_b).b * mask.b);\n  _color = vec4(color, 1.0);\n}",
      "name": "slice"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform vec2 _canvas_res;\nuniform sampler2D _audio;\n\nuniform vec4 color;\nuniform float line;\nuniform float sharpness;\n\nconst float kAN = 512.0;\nconst float kAW = 128.0; \n\nlayout (location = 0) out vec4 _color;\n\nfloat IndexSquaring(in float i, in float v, in float w) {\n  float thr = w / v;\n  float si = floor(i * w) / w;\n  return step(thr, i) * si * si + step(i, thr) * floor(i * thr * v) / v;\n}\n\nfloat SmoothInterpolation(in sampler2D tex, in vec2 tex_p, in float d) {\n  float n0 = texture(tex, tex_p).r;\n  float n1 = textureOffset(tex, tex_p, ivec2(1, 0)).r;\n  return smoothstep(-0.5, 0.5, d) * (n1 - n0) + n0;\n}\n\nfloat Bar(in vec2 uv, in float pct) {\n  return  step(0.0, uv.y) - step(pct, uv.y);\n}\n\nfloat Plot(in vec2 uv, in float pct, in float width) {\n  return  step(pct - width * 0.5, uv.y) - step(pct + width * 0.5, uv.y);\n}\n\nvoid main(void) {\n  vec2 uv = gl_FragCoord.xy / _canvas_res;\n  float i = abs(uv.x * 2.0 - 1.0);\n  float lr = step(0.5, uv.x) * 0.25 + 0.01;\n\n  float ai = IndexSquaring(i, kAN, kAW);\n  vec2 ap = vec2(ai, lr);\n  float a = pow(texture(_audio, ap).r, sharpness) * 0.25;\n  float mask_a = Bar(uv, a);\n\n  float wd = fract(i * kAN) - 0.5;\n  vec2 wp = vec2(i, lr + 0.5);\n  float w = SmoothInterpolation(_audio, wp, wd);\n  float mask_w = Plot(uv, w, line);\n\n  float mask = step(mask_a + mask_w, 0.5);\n  _color = mix(color, vec4(0.0), mask);\n}",
      "name": "visualizer"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform sampler2D _video;\n\nuniform vec3 light;\nuniform float shininess;\n\nlayout (location = 0) out vec4 _color;\n\nin vec4 vp;\nin vec3 vn;\n\nvec2 EnvMap(in vec3 ref) {\n  const float kPI = 3.141593;\n  const float k2PI = 6.283185;\n  vec3 dir = normalize(ref);\n  float phi = acos(-dir.y);\n  float theta = atan(dir.x, dir.z) + kPI;\n  return vec2(theta / k2PI, phi / kPI);\n}\n\nvec3 Shading(in vec4 pos, in vec3 nrm, in vec3 li, in vec3 clr, in float shi) {\n  const vec3 l_ambi = vec3(0.2);\n  const vec3 l_diff = vec3(0.9);\n  const vec3 l_spec = vec3(0.9);\n  vec3 v = -normalize(pos.xyz);\n  vec3 l = normalize((vec4(li, 1.0) * pos.w - pos).xyz);\n  vec3 h = normalize(l + v);\n  vec3 diff = max(dot(nrm, l), 0.0) * clr * l_diff + clr * l_ambi;\n  vec3 spec = pow(max(dot(normalize(nrm), h), 0.0), shi) * clr * l_spec;\n  return diff + spec;\n}\n\nvoid main(void) {\n  vec2 uv = EnvMap(reflect(vp.xyz * vec3(1.0, 1.0, 0.25), vn));\n  vec3 c = texture(_video, uv).rgb;\n  _color = vec4(Shading(vp, vn, light, c, shininess), 1.0);\n}",
      "name": "mirror"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nin vec4 vp;\nin vec3 vn;\nin vec3 vc;\nin vec3 l;\n\nlayout (location = 0) out vec4 _color;\n\nvec3 Shading(in vec4 pos, in vec3 nrm, in vec3 li, in vec3 clr, in float shi) {\n  const vec3 l_ambi = vec3(0.5);\n  const vec3 l_diff = vec3(0.9);\n  const vec3 l_spec = vec3(0.9);\n  vec3 v = -normalize(pos.xyz);\n  vec3 l = normalize((vec4(li, 1.0) * pos.w - pos).xyz);\n  vec3 h = normalize(l + v);\n  vec3 diff = max(dot(nrm, l), 0.0) * clr * l_diff + clr * l_ambi;\n  vec3 spec = pow(max(dot(normalize(nrm), h), 0.0), shi) * clr * l_spec;\n  return diff + spec;\n}\n\nvoid main(void) {\n  _color = vec4(Shading(vp, vn, l, vc, 50.0), 1.0);\n}",
      "name": "vert_color"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform vec2 _canvas_res;\nuniform sampler2D _frag_buffer;\nuniform sampler2D _video;\n\nuniform float range;\nuniform float intensity;\n\nlayout (location = 0) out vec4 out_color;\nlayout (location = 1) out vec4 buffer;\n\nvoid main(void) {\n  vec2 uv = gl_FragCoord.xy / _canvas_res;\n  vec2 adj_uv = uv * (1.0 - range) + range * 0.5;\n  out_color = vec4(mix(texture(_video, uv).rgb,\n                       texture(_frag_buffer, adj_uv).rgb,\n                       intensity),\n                   1.0);\n  buffer = out_color;\n}",
      "name": "loser"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform sampler2D _video;\n\nin vec4 vp;\nin vec3 vn;\nin vec2 vt;\nin vec3 light;\n\nlayout (location = 0) out vec4 _color;\n\nvec3 Shading(in vec4 pos, in vec3 nrm, in vec3 li, in vec3 clr, in float shi) {\n  const vec3 l_ambi = vec3(0.8);\n  const vec3 l_diff = vec3(0.9);\n  const vec3 l_spec = vec3(0.9);\n  vec3 v = -normalize(pos.xyz);\n  vec3 l = normalize((vec4(li, 1.0) * pos.w - pos).xyz);\n  vec3 h = normalize(l + v);\n  vec3 diff = max(dot(nrm, l), 0.0) * clr * l_diff + clr * l_ambi;\n  vec3 spec = pow(max(dot(normalize(nrm), h), 0.0), shi) * clr * l_spec;\n  return diff + spec;\n}\n\nvoid main(void) {\n  vec3 c = texture(_video, vt).rgb;\n  _color = vec4(Shading(vp, vn, light, c, 50.0), 1.0);\n}",
      "name": "break"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform sampler2D _video;\n\nuniform float alpha;\n\nin vec4 vp;\nin vec3 vn;\nin vec3 vc;\nin vec3 light;\n\nlayout (location = 0) out vec4 _color;\n\nvec3 Shading(in vec4 pos, in vec3 nrm, in vec3 li, in vec3 clr, in float shi) {\n  const vec3 l_ambi = vec3(0.7);\n  const vec3 l_diff = vec3(0.8);\n  const vec3 l_spec = vec3(0.9);\n  vec3 v = -normalize(pos.xyz);\n  vec3 l = normalize((vec4(li, 1.0) * pos.w - pos).xyz);\n  vec3 h = normalize(l + v);\n  vec3 diff = max(dot(nrm, l), 0.0) * clr * l_diff + clr * l_ambi;\n  vec3 spec = pow(max(dot(normalize(nrm), h), 0.0), shi) * clr * l_spec;\n  return diff + spec;\n}\n\nvoid main(void) {\n  _color = vec4(Shading(vp, vn, light, vc, 50.0), alpha);\n}",
      "name": "wave"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nin vec3 vc;\n\nlayout (location = 0) out vec4 _color;\n\nvoid main(void) {\n  _color = vec4(vc, 1.0);\n}",
      "name": "vertex_color"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform sampler2D _video;\nuniform vec2 _canvas_res;\nuniform vec2 _video_res;\n\nuniform float color_thr;\nuniform float color_range;\nuniform float edge_thr;\n\nconst vec2 kPcRes = vec2(640.0, 400);\nconst vec2 kPixScale = vec2(1.0, 2.0);\n\nlayout (location = 0) out vec4 _color;\n\nfloat FixAspectRatioCoord(inout vec2 coord, inout vec2 res, in vec2 asp) {\n  float ratio = min(res.x / asp.x, res.y / asp.y);\n  vec2 fix_res = floor(asp * ratio);\n  vec2 fix_coord = coord - floor((res - fix_res) * 0.5);\n  vec2 mask = step(0.0, fix_coord) * step(fix_coord, fix_res);\n  res = fix_res;\n  coord = fix_coord;\n  return mask.x * mask.y;\n}\n\nvec2 SetPixelateCoord(inout vec2 coord, inout vec2 res, in vec2 size) {\n  vec2 ratio = res / size;\n  vec2 correct = size - floor(fract(ratio) * size * 0.5);\n  vec2 sub_coord = (coord + correct) / size;\n  coord = floor(sub_coord);\n  res = floor(ratio + step(0.0001, correct));\n  return fract(sub_coord) * size;\n}\n\nfloat GetLuminance(in vec3 color) {\n  return 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;\n}\n\nvec4 Get3bitColorCoord(in sampler2D tex, in vec2 coord, in vec2 res,\n                       in float threshold, in float range) {\n  float flag = mod(coord.x + coord.y, 2.0) * 2.0 - 1.0;\n  vec4 color = texture(tex, coord / res);\n  float luminance = GetLuminance(color.rgb);\n  float fix_range = (range * luminance) * 0.5;\n  return vec4(step(threshold - (fix_range * flag), color.rgb), color.a);\n}\n\nmat3 GetLuminanceMatrix(in sampler2D tex, in vec2 uv) {\n  mat3 luminance;\n  luminance[0][0] = GetLuminance(textureOffset(tex, uv, ivec2(-1,  1)).rgb);\n  luminance[0][1] = GetLuminance(textureOffset(tex, uv, ivec2(-1,  0)).rgb);\n  luminance[0][2] = GetLuminance(textureOffset(tex, uv, ivec2(-1, -1)).rgb);\n  luminance[1][0] = GetLuminance(textureOffset(tex, uv, ivec2( 0,  1)).rgb);\n  luminance[1][1] = GetLuminance(textureOffset(tex, uv, ivec2( 0,  0)).rgb);\n  luminance[1][2] = GetLuminance(textureOffset(tex, uv, ivec2( 0, -1)).rgb);\n  luminance[2][0] = GetLuminance(textureOffset(tex, uv, ivec2( 1,  1)).rgb);\n  luminance[2][1] = GetLuminance(textureOffset(tex, uv, ivec2( 1,  0)).rgb);\n  luminance[2][2] = GetLuminance(textureOffset(tex, uv, ivec2( 1, -1)).rgb);\n  return luminance;\n}\n\nfloat GetEdgeSobel(in mat3 filt_mat, in float threshold) {\n  float dx = filt_mat[0][0]\n            +filt_mat[0][1] * 2.0\n            +filt_mat[0][2]\n            -filt_mat[2][1]\n            -filt_mat[2][1] * 2.0\n            -filt_mat[2][2];\n  float dy = filt_mat[0][0]\n            +filt_mat[1][0] * 2.0\n            +filt_mat[2][0]\n            -filt_mat[0][2]\n            -filt_mat[1][2] * 2.0\n            -filt_mat[2][2];\n  return step(threshold * threshold, dx * dx + dy * dy);\n}\n\nvoid main(void) {\n  vec2 res = _canvas_res;\n  vec2 coord = gl_FragCoord.xy;\n  float mask1 = FixAspectRatioCoord(coord, res, kPcRes);\n  float scale = res.x > res.y ? ceil(res.x / kPcRes.x) : ceil(res.y / kPcRes.y);\n  float mask2 = FixAspectRatioCoord(coord, res, _video_res);\n  SetPixelateCoord(coord, res, scale * kPixScale);\n  vec3 color = Get3bitColorCoord(_video, coord, res, color_thr, color_range).rgb;\n  mat3 lumi_mat = GetLuminanceMatrix(_video, coord / res);\n  float edge = GetEdgeSobel(lumi_mat, edge_thr);\n  color = clamp(color - edge, 0.0, 1.0);\n  _color = vec4(color * mask2, 1.0);\n}",
      "name": "pc8801"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform vec2 _canvas_res;\nuniform sampler2D _video;\nuniform sampler2D _prev_video;\nuniform sampler2D _frag_buffer;\nuniform float _frame;\n\nuniform float size;\nuniform float bright;\nuniform vec3 color_shift_x;\nuniform vec3 color_shift_y;\nuniform float blur;\nuniform float saturation;\n\nlayout (location = 0) out vec4 _color;\nlayout (location = 1) out vec4 _buffer;\n\nvec3 rgb2hsv(in vec3 c) {\n    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);\n    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));\n    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));\n\n    float d = q.x - min(q.w, q.y);\n    float e = 1.0e-10;\n    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);\n}\n\nvec3 hsv2rgb(in vec3 c) {\n    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);\n    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);\n    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);\n}\n\nvoid main(void) {\n  vec2 uv = gl_FragCoord.xy / _canvas_res;\n  vec2 uv_off = 1.0 / _canvas_res;\n  float f_mod = mod(_frame, 2.0);\n  float mask = step(mod(gl_FragCoord.y / size + f_mod, 2.0), 1.0);\n  vec3 frame_1 = vec3(texture(_video, uv + uv_off * vec2(color_shift_x.r, color_shift_y.r)).r,\n                      texture(_video, uv + uv_off * vec2(color_shift_x.g, color_shift_y.g)).g,\n                      texture(_video, uv + uv_off * vec2(color_shift_x.b, color_shift_y.b)).b);\n  vec3 frame_2 = vec3(texture(_prev_video, uv + uv_off * vec2(color_shift_x.r, color_shift_y.r)).r,\n                      texture(_prev_video, uv + uv_off * vec2(color_shift_x.g, color_shift_y.g)).g,\n                      texture(_prev_video, uv + uv_off * vec2(color_shift_x.b, color_shift_y.b)).b);\n  frame_1 = rgb2hsv(frame_1);\n  frame_1.y *= saturation;\n  frame_1 = hsv2rgb(frame_1);\n  frame_2 = rgb2hsv(frame_2);\n  frame_2.y *= saturation;\n  frame_2.z *= bright;\n  frame_2 = hsv2rgb(frame_2);\n  vec3 color_1 = frame_1 * mask + frame_2 * step(mask, 0.5) * bright;\n  _buffer = vec4(color_1, 1.0);\n\n  vec3 color_2 = texture(_frag_buffer, uv).rgb;\n  color_2 *= 1.0 - blur * 32.0;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2(-1,  1)).rgb * blur * 2.0;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 0,  1)).rgb * blur * 2.0;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 1,  1)).rgb * blur * 2.0;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2(-1,  0)).rgb * blur * 2.0;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 1,  0)).rgb * blur * 2.0;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2(-1, -1)).rgb * blur * 2.0;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 0, -1)).rgb * blur * 2.0;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 1, -1)).rgb * blur * 2.0;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2(-2,  2)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2(-1,  2)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 0,  2)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 1,  2)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 2,  2)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2(-1,  1)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 1,  1)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2(-1,  0)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 1,  0)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2(-1, -1)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 1, -1)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2(-2, -2)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2(-1, -2)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 0, -2)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 1, -2)).rgb * blur;\n  color_2 += textureOffset(_frag_buffer, uv, ivec2( 2, -2)).rgb * blur;\n  _color = vec4(color_2, 1.0);\n}",
      "name": "crt"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform vec2 _canvas_res;\nuniform sampler2D _frag_buffer;\nuniform sampler2D _video;\nuniform sampler2D _prev_video;\n\nuniform float burn_thr;\nuniform float blur;\n\nlayout (location = 0) out vec4 out_color;\nlayout (location = 1) out vec4 buffer;\n\nfloat GetLuminance(in vec3 color) {\n  return 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;\n}\n\nvec3 GetBlur(in vec2 uv, in sampler2D tex, in float strength) {\n  vec3 blur = textureOffset(tex, uv, ivec2(-1,  1)).rgb * 0.125;\n  blur += textureOffset(tex, uv, ivec2( 0,  1)).rgb * 0.125;\n  blur += textureOffset(tex, uv, ivec2( 1,  1)).rgb * 0.125;\n  blur += textureOffset(tex, uv, ivec2(-1,  0)).rgb * 0.125;\n  blur += textureOffset(tex, uv, ivec2( 1,  0)).rgb * 0.125;\n  blur += textureOffset(tex, uv, ivec2(-1, -1)).rgb * 0.125;\n  blur += textureOffset(tex, uv, ivec2( 0, -1)).rgb * 0.125;\n  blur += textureOffset(tex, uv, ivec2( 1, -1)).rgb * 0.125;\n  return mix(texture(tex, uv).rgb, blur, strength);\n}\n\nvoid main(void) {\n  vec2 uv = gl_FragCoord.xy / _canvas_res;\n  vec3 video = texture(_video, uv).rgb;\n  float lumi = GetLuminance(video);\n  float prev_lumi = GetLuminance(texture(_prev_video, uv).rgb);\n  vec3 current = GetBlur(uv, _frag_buffer, blur);\n  float update = step(burn_thr, abs(lumi - prev_lumi));\n  buffer = vec4(video * update + current * step(update, 0.5), 1.0);\n  out_color = buffer;\n}",
      "name": "burn-in"
    },
    {
      "code": "#version 300 es\nprecision mediump float;\n\nuniform vec2 _canvas_res;\nuniform sampler2D _frag_buffer;\nuniform sampler2D _video;\nuniform sampler2D _prev_video;\nuniform float _frame;\n\nuniform float born_thr;\nuniform float brightness;\nuniform float decay;\n\nlayout (location = 0) out vec4 out_color;\nlayout (location = 1) out vec4 buffer;\n\nfloat GetLuminance(in vec3 color) {\n  return 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;\n}\n\nfloat GetRandom(in vec2 uv, in float seed) {\n  const vec2 param_1 = vec2(12.9898, 78.233);\n  const float param_2 = 43758.5453123;\n  return fract(sin(dot(uv.xy * (seed + 1.0), param_1)) * param_2);\n}\n\nvec4 GetCellCondition(in vec2 uv, in sampler2D cells, in float thr) {\n  vec4 sum = vec4(0.0);\n  vec4 temp = vec4(0.0);\n  temp = textureOffset(cells, uv, ivec2(-1,  1));\n  sum += temp * step(thr, temp.a);\n  temp = textureOffset(cells, uv, ivec2( 0,  1));\n  sum += temp * step(thr, temp.a);\n  temp = textureOffset(cells, uv, ivec2( 1,  1));\n  sum += temp * step(thr, temp.a);\n  temp = textureOffset(cells, uv, ivec2(-1,  0));\n  sum += temp * step(thr, temp.a);\n  temp = textureOffset(cells, uv, ivec2( 1,  0));\n  sum += temp * step(thr, temp.a);\n  temp = textureOffset(cells, uv, ivec2(-1, -1));\n  sum += temp * step(thr, temp.a);\n  temp = textureOffset(cells, uv, ivec2( 0, -1));\n  sum += temp * step(thr, temp.a);\n  temp = textureOffset(cells, uv, ivec2( 1, -1));\n  sum += temp * step(thr, temp.a);\n  sum.rgb = clamp(sum.rgb / (sum.a * 0.999), 0.0, 1.0);\n  return sum;\n}\n\nvec4 GetNextState(in vec4 condition, in vec3 video, in float put, in vec4 prev,\n                  in float thr) {\n  float flag_2 = step(abs(condition.a - 2.0), 0.5);\n  float flag_3 = step(abs(condition.a - 3.0), 0.5);\n  float prev_state = step(thr, prev.a);\n  float born = flag_3 * step(prev_state, 0.5);\n  float survival = (flag_2 + flag_3) * prev_state;\n  float flip_put = step(put, 0.5);\n  float flip_born = step(born, 0.5);\n  vec3 color = video * put +\n               condition.rgb * born * flip_put +\n               prev.rgb * flip_born * flip_put;\n  float state = step(0.5, put + born + survival);\n  return vec4(color, state);\n}\n\n#define DECAY_MIN 0.0001\n#define FIX_BRIGHTNESS_FACTOR 0.9\n\nvoid main(void) {\n  vec2 uv = gl_FragCoord.xy / _canvas_res;\n  vec3 video = texture(_video, uv).rgb;\n  float lumi = GetLuminance(video);\n  float prev_lumi = GetLuminance(texture(_prev_video, uv).rgb);\n  float fix_born_thr = born_thr * 0.8 + 0.1;\n  float put = step(1.0 - fix_born_thr, abs(lumi - prev_lumi)) *\n              step(fix_born_thr, GetRandom(uv, _frame));\n  vec4 prev_cell = texture(_frag_buffer, uv);\n  vec4 condition = GetCellCondition(uv, _frag_buffer, brightness);\n  vec4 new_cell = GetNextState(condition, video, put, prev_cell, brightness);\n  float fix_brightness = brightness * FIX_BRIGHTNESS_FACTOR;\n  float fix_decay = clamp(decay * decay * decay, DECAY_MIN, 1.0);\n  float imprint = clamp(prev_cell.a - fix_decay, 0.0, fix_brightness);\n  new_cell.a = step(new_cell.a, 0.5) * imprint + new_cell.a;\n  buffer = new_cell;\n  out_color = vec4(new_cell.rgb * new_cell.a, 1.0);\n}",
      "name": "life"
    }
  ],
  "indices": [],
  "scale": "1.0",
  "vert_codes": [
    {
      "code": "#version 300 es\n\nlayout (location = 0) in vec3 _vp;\nlayout (location = 1) in vec3 _vn;\nlayout (location = 2) in vec2 _vt;\nlayout (location = 3) in vec2 _pos;\nlayout (location = 4) in vec3 _rot;\nlayout (location = 5) in vec3 _color;\n\nuniform vec2 _canvas_res;\n\nuniform vec3 light;\n\nconst float kNum = 8.0;\nconst vec2 kRes = vec2(16.0, 9.0);\nconst vec3 kEye = vec3(0.0, 0.0, 2.0);\n\nout vec3 vc;\n\nmat4 Translate(in vec3 t) {\n  return mat4(1.0, 0.0, 0.0, 0.0,\n              0.0, 1.0, 0.0, 0.0,\n              0.0, 0.0, 1.0, 0.0,\n              t.x, t.y, t.z, 1.0);\n}\n\nmat4 RotX(in float r) {\n  return mat4(1.0,    0.0,     0.0, 0.0,\n              0.0,  cos(r), sin(r), 0.0,\n              0.0, -sin(r), cos(r), 0.0,\n              0.0,    0.0,     0.0, 1.0);\n}\n\nmat4 RotY(in float r) {\n  return mat4(cos(r), 0.0, -sin(r), 0.0,\n                 0.0, 1.0,     0.0, 0.0,\n              sin(r), 0.0,  cos(r), 0.0,\n                 0.0, 0.0,     0.0, 1.0);\n}\n\nmat4 RotZ(in float r) {\n  return mat4( cos(r), sin(r), 0.0, 0.0,\n              -sin(r), cos(r), 0.0, 0.0,\n                  0.0,    0.0, 1.0, 0.0,\n                  0.0,    0.0, 0.0, 1.0);\n}\n\nmat4 LookAt(in vec3 e, in vec3 g, in vec3 u) {\n  mat4 tv = Translate(-e);\n  vec3 t = e - g;\n  vec3 r = cross(u, t);\n  vec3 s = cross(t, r);\n  if (length(s) == 0.0)return tv;\n  t = normalize(t);\n  r = normalize(r);\n  s = normalize(s);\n  mat4 rv = mat4(r.x, s.x, t.x, 0.0,\n                 r.y, s.y, t.y, 0.0,\n                 r.z, s.z, t.z, 0.0,\n                 0.0, 0.0, 0.0, 1.0);\n  return rv * tv;\n}\n\nmat4 Orthogonal(in float left, in float right, in float bottom, in float top,\n                in float z_near, in float z_far) {\n  vec3 d = vec3(right - left, top - bottom, z_far - z_near);\n  if(any(equal(d, vec3(0.0))))return mat4(1.0);\n  vec3 a = vec3(right + left, top + bottom, z_far + z_near);\n  return mat4( 2.0 / d.x,        0.0,        0.0, 0.0,\n                     0.0,  2.0 / d.y,        0.0, 0.0,\n                     0.0,        0.0, -2.0 / d.z, 0.0,\n              -a.x / d.x, -a.y / d.y, -a.z / d.z, 1.0);\n}\n\nmat3 NormalMatrix(in mat4 mv) {\n  mat3 n = mat3(mv[0][0], mv[0][1], mv[0][2],\n                mv[1][0], mv[1][1], mv[1][2],\n                mv[2][0], mv[2][1], mv[2][2]);\n  return transpose(inverse(n));\n}\n\nvec3 SimpleShading(in vec3 nrm, in vec3 li, in vec3 clr) {\n  const vec3 l_ambi = vec3(0.5);\n  const vec3 l_diff = vec3(0.7);\n  vec3 ambi = clr * l_ambi;\n  vec3 diff = max(dot(nrm, normalize(li)), 0.0) * clr * l_diff;\n  return ambi + diff;\n}\n\nvoid main(void) {\n  vec2 res = kRes * kNum;\n  mat4 m = Translate(vec3(_pos * res, 0.0))\n         * RotZ(_rot.z * _rot.z)\n         * RotY(_rot.y * _rot.y)\n         * RotX(_rot.x * _rot.x);\n  mat4 v = LookAt(kEye, vec3(0.0), vec3(0.0, 1.0, 0.0));\n  mat4 mv = v * m;\n  mat4 o = Orthogonal(-res.x, res.x, -res.y, res.y, 0.1, 4.0);\n\n  vc = SimpleShading(normalize(NormalMatrix(mv) * _vn), light, _color);\n\n  gl_Position = o * mv * vec4(_vp, 1.0);\n}",
      "name": "imprint"
    },
    {
      "code": "#version 300 es\n\nlayout (location = 0) in vec2 _vp;\nlayout (location = 1) in vec2 _vn;\nlayout (location = 2) in vec2 _vt;\n\nvoid main(void) {\n  gl_Position = vec4(_vp, 0.0, 1.0);\n}",
      "name": "panel_screen"
    },
    {
      "code": "#version 300 es\n\nlayout (location = 0) in vec3 _vp;\nlayout (location = 1) in vec3 _vn;\nlayout (location = 2) in vec2 _vt;\n\nuniform vec2 _canvas_res;\nuniform float _time;\n\nuniform vec3 eye;\n\nout vec4 vp;\nout vec3 vn;\n\nmat4 Translate(in vec3 t) {\n  return mat4(1.0, 0.0, 0.0, 0.0,\n              0.0, 1.0, 0.0, 0.0,\n              0.0, 0.0, 1.0, 0.0,\n              t.x, t.y, t.z, 1.0);\n}\n\nmat4 RotX(in float r) {\n  return mat4(1.0,    0.0,     0.0, 0.0,\n              0.0,  cos(r), sin(r), 0.0,\n              0.0, -sin(r), cos(r), 0.0,\n              0.0,    0.0,     0.0, 1.0);\n}\n\nmat4 RotY(in float r) {\n  return mat4(cos(r), 0.0, -sin(r), 0.0,\n                 0.0, 1.0,     0.0, 0.0,\n              sin(r), 0.0,  cos(r), 0.0,\n                 0.0, 0.0,     0.0, 1.0);\n}\n\nmat4 RotZ(in float r) {\n  return mat4( cos(r), sin(r), 0.0, 0.0,\n              -sin(r), cos(r), 0.0, 0.0,\n                  0.0,    0.0, 1.0, 0.0,\n                  0.0,    0.0, 0.0, 1.0);\n}\n\nmat4 LookAt(in vec3 e, in vec3 g, in vec3 u) {\n  mat4 tv = Translate(-e);\n  vec3 t = e - g;\n  vec3 r = cross(u, t);\n  vec3 s = cross(t, r);\n  if (length(s) == 0.0)return tv;\n  t = normalize(t);\n  r = normalize(r);\n  s = normalize(s);\n  mat4 rv = mat4(r.x, s.x, t.x, 0.0,\n                 r.y, s.y, t.y, 0.0,\n                 r.z, s.z, t.z, 0.0,\n                 0.0, 0.0, 0.0, 1.0);\n  return rv * tv;\n}\n\nmat3 NormalMatrix(in mat4 mv) {\n  mat3 n = mat3(mv[0][0], mv[0][1], mv[0][2],\n                mv[1][0], mv[1][1], mv[1][2],\n                mv[2][0], mv[2][1], mv[2][2]);\n  return transpose(inverse(n));\n}\n\nmat4 Perspective(in float fov_y, vec2 res, in float z_near, in float z_far) {\n  float asp = res.x / res.y;\n  float dz = z_far - z_near;\n  mat4 p;\n  if (dz != 0.0) {\n    float f = 1.0 / tan(fov_y * 0.5);\n    p = mat4(f / asp, 0.0,                          0.0,  0.0,\n                 0.0,   f,                          0.0,  0.0,\n                 0.0, 0.0,     -((z_far + z_near) / dz), -1.0,\n                 0.0, 0.0, -(2.0 * z_far * z_near) / dz,  0.0);\n  }\n  return p;\n}\n\nvoid main(void) {\n  mat4 m = RotZ(smoothstep(0.0, 1.0, mod(_time, 1.0)) + floor(_time))\n         * RotY(smoothstep(0.0, 1.3, mod(_time, 1.3)) + floor(_time / 1.3))\n         * RotX(smoothstep(0.0, 1.7, mod(_time, 1.7)) + floor(_time / 1.7));\n  mat4 v = LookAt(eye, vec3(0.0), vec3(0.0, 1.0, 0.0));\n  mat4 p = Perspective(radians(45.0), _canvas_res, 1.0, 10.0);\n  mat4 mv = v * m;\n  mat3 nm = NormalMatrix(mv);\n\n  vp = mv * vec4(_vp, 1.0);\n  vn = normalize(nm * _vn);\n  gl_Position = p * vp;\n}",
      "name": "instance_rot"
    },
    {
      "code": "#version 300 es\n\nlayout (location = 0) in vec3 _vp;\nlayout (location = 1) in vec3 _vn;\nlayout (location = 2) in vec2 _vt;\nlayout (location = 3) in vec3 _pos;\nlayout (location = 4) in vec3 _rot;\n\nuniform float _frame;\nuniform vec2 _canvas_res;\n\nconst float kNum = 8.0;\nconst vec2 kRes = vec2(16.0, 9.0);\nconst float kScale = 50.0;\nconst vec3 kEye = vec3(0.0, 0.0, 2.0);\n\nout vec4 vp;\nout vec3 vn;\nout vec2 vt;\nout vec3 light;\n\nvec2 Stack2D(in float count, in vec2 res) {\n  return vec2(mod(count, res.x), floor(count / res.x));\n}\n\nvec2 Aspect(in vec2 res) {\n  return res / min(res.x, res.y);\n}\n\nmat4 Translate(in vec3 t) {\n  return mat4(1.0, 0.0, 0.0, 0.0,\n              0.0, 1.0, 0.0, 0.0,\n              0.0, 0.0, 1.0, 0.0,\n              t.x, t.y, t.z, 1.0);\n}\n\nmat4 Scale(in vec3 s) {\n  return mat4(s.x, 0.0, 0.0, 0.0,\n              0.0, s.y, 0.0, 0.0,\n              0.0, 0.0, s.z, 0.0,\n              0.0, 0.0, 0.0, 1.0);\n}\n\nmat4 RotX(in float r) {\n  return mat4(1.0,     0.0,    0.0, 0.0,\n              0.0,  cos(r), sin(r), 0.0,\n              0.0, -sin(r), cos(r), 0.0,\n              0.0,     0.0,    0.0, 1.0);\n}\n\nmat4 RotY(in float r) {\n  return mat4(cos(r), 0.0, -sin(r), 0.0,\n                0.0,  1.0,     0.0, 0.0,\n              sin(r), 0.0,  cos(r), 0.0,\n                 0.0, 0.0,     0.0, 1.0);\n}\n\nmat4 RotZ(in float r) {\n  return mat4( cos(r), sin(r), 0.0, 0.0,\n              -sin(r), cos(r), 0.0, 0.0,\n                  0.0,    0.0, 1.0, 0.0,\n                  0.0,    0.0, 0.0, 1.0);\n}\n\nmat4 LookAt(in vec3 e, in vec3 g, in vec3 u) {\n  mat4 tv = Translate(-e);\n  vec3 t = e - g;\n  vec3 r = cross(u, t);\n  vec3 s = cross(t, r);\n  if (length(s) == 0.0)return tv;\n  t = normalize(t);\n  r = normalize(r);\n  s = normalize(s);\n  mat4 rv = mat4(r.x, s.x, t.x, 0.0,\n                  r.y, s.y, t.y, 0.0,\n                  r.z, s.z, t.z, 0.0,\n                  0.0, 0.0, 0.0, 1.0);\n  return rv * tv;\n}\n\nmat3 NormalMatrix(in mat4 mv) {\n  mat3 n = mat3(mv[0][0], mv[0][1], mv[0][2],\n                mv[1][0], mv[1][1], mv[1][2],\n                mv[2][0], mv[2][1], mv[2][2]);\n  return transpose(inverse(n));\n}\n\nmat4 Perspective(in float fov_y, vec2 res, in float z_near, in float z_far) {\n  float asp = res.x / res.y;\n  float dz = z_far - z_near;\n  mat4 p;\n  if (dz != 0.0) {\n    float f = 1.0 / tan(fov_y * 0.5);\n    p = mat4(f / asp,  0.0,                          0.0,  0.0,\n                  0.0,   f,                          0.0,  0.0,\n                  0.0, 0.0,     -((z_far + z_near) / dz), -1.0,\n                  0.0, 0.0, -(2.0 * z_far * z_near) / dz,  0.0);\n  }\n  return p;\n}\n\nvoid main(void) {\n  float t = _frame / 60.0;\n  vec2 res = kRes * kNum;\n  mat4 m = Scale(vec3(Aspect(_canvas_res), 1.0))\n         * Translate(_pos * 2.0 * kScale)\n         * RotZ(_rot.z) * RotY(_rot.y) * RotX(_rot.x);\n  mat4 v = LookAt(kEye * kScale, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));\n  mat4 p = Perspective(radians(45.0), _canvas_res, 0.1, kScale * 20.0);\n  mat4 mv = v * m;\n  mat3 nm = NormalMatrix(mv);\n\n  vp = mv * vec4(_vp, 1.0);\n  vn = normalize(nm * _vn);\n  vt = (_vt + Stack2D(float(gl_InstanceID), res)) / res;\n  light = kEye;\n  gl_Position = p * vp;\n}",
      "name": "break"
    },
    {
      "code": "#version 300 es\n\nlayout (location = 0) in vec3 _vp;\nlayout (location = 1) in vec3 _vn;\nlayout (location = 2) in vec2 _vt;\nlayout (location = 3) in vec2 _pos;\nlayout (location = 4) in float _wave;\n\nuniform float _frame;\nuniform float _fps;\nuniform vec2 _canvas_res;\n\nuniform float size;\n\nconst float kScale = 50.0;\n\nout vec4 vp;\nout vec3 vn;\nout vec3 vc;\nout vec3 light;\n\nmat4 Translate(in vec3 t) {\n  return mat4(1.0, 0.0, 0.0, 0.0,\n              0.0, 1.0, 0.0, 0.0,\n              0.0, 0.0, 1.0, 0.0,\n              t.x, t.y, t.z, 1.0);\n}\n\nmat4 Scale(in vec3 s) {\n  return mat4(s.x, 0.0, 0.0, 0.0,\n              0.0, s.y, 0.0, 0.0,\n              0.0, 0.0, s.z, 0.0,\n              0.0, 0.0, 0.0, 1.0);\n}\n\nmat4 LookAt(in vec3 e, in vec3 g, in vec3 u) {\n  mat4 tv = Translate(-e);\n  vec3 t = e - g;\n  vec3 r = cross(u, t);\n  vec3 s = cross(t, r);\n  if (length(s) == 0.0)return tv;\n  t = normalize(t);\n  r = normalize(r);\n  s = normalize(s);\n  mat4 rv = mat4(r.x, s.x, t.x, 0.0,\n                 r.y, s.y, t.y, 0.0,\n                 r.z, s.z, t.z, 0.0,\n                 0.0, 0.0, 0.0, 1.0);\n  return rv * tv;\n}\n\nmat3 NormalMatrix(in mat4 mv) {\n  mat3 n = mat3(mv[0][0], mv[0][1], mv[0][2],\n                mv[1][0], mv[1][1], mv[1][2],\n                mv[2][0], mv[2][1], mv[2][2]);\n  return transpose(inverse(n));\n}\n\nmat4 Perspective(in float fov_y, vec2 res, in float z_near, in float z_far) {\n  float asp = res.x / res.y;\n  float dz = z_far - z_near;\n  mat4 p;\n  if (dz != 0.0) {\n    float f = 1.0 / tan(fov_y * 0.5);\n    p = mat4(f / asp,  0.0,                          0.0,  0.0,\n                 0.0,    f,                          0.0,  0.0,\n                 0.0,  0.0,     -((z_far + z_near) / dz), -1.0,\n                 0.0,  0.0, -(2.0 * z_far * z_near) / dz,  0.0);\n  }\n  return p;\n}\n\nvoid main(void) {\n  vec3 pos = vec3(_pos.x, _wave * 0.5, _pos.y);\n  mat4 m = Translate(pos * 2.0 * kScale)\n         * Scale(vec3(size, _wave * kScale + 1.0, size));\n  float t = _frame / _fps * 0.5;\n  vec3 e = vec3(sin(t) * 2.0 * kScale * (sin(t * 0.67) + 3.0),\n                2.0 * kScale,\n                cos(t) * 2.0 * kScale * (cos(t * 0.67) + 3.0));\n  mat4 v = LookAt(e, vec3(0.0, kScale * 0.5, 0.0), vec3(0.0, 1.0, 0.0));\n  mat4 p = Perspective(radians(45.0), _canvas_res, 0.1, kScale * 20.0);\n  mat4 mv = v * m;\n  mat3 nm = NormalMatrix(mv);\n  float inv_wave = 1.0 - _wave;\n  float blue = 1.0 - inv_wave * inv_wave * inv_wave;\n\n  vp = mv * vec4(_vp, 1.0);\n  vn = normalize(nm * _vn);\n  vc = vec3((pos.z + 1.0) * 0.5,\n            (pos.x + 1.0) * 0.5,\n            (_vp.y + 1.0) * 0.5 * blue);\n  light = (mv * vec4(e, 1.0)).xyz;\n  gl_Position = p * vp;\n}",
      "name": "wave"
    }
  ]
};