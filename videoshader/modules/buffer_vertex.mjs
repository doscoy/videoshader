"use strict";

export class BufferVertex {
  is_ready = false;
  vao = new Array(2);
  vbo = new Array(2);
  config = new Array();
  stride = null;
  data_length = null;
  draw_count = null;
  index_head = null;
  index_length = null;
  index_tail = null;
  current_vao = 0;
  read_vbo = this.current_vao;
  write_vbo = (this.read_vbo + 1) % 2;
  bytes = 4;
  
  constructor(gl, feedback_status, varyings_info, count, index_head) {
    if (!feedback_status) return;
    this._InitConfig(gl, varyings_info, count);
    if (this.index_length === 0) return;
    this.index_head = index_head;
    this.index_tail = this.index_head + this.index_length;
    this.draw_count = count;
    for (let i = 0; i < this.vbo.length; ++i) {
      this.vbo[i] = gl.createBuffer();
      gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo[i]);
      gl.bufferData(gl.ARRAY_BUFFER,
        new Float32Array(this.data_length), gl.DYNAMIC_COPY);
    }
    this._InitVAO(gl);
    this.is_ready = true;
  }

  _InitConfig(gl, varyings_info, count) {
    let s = 0;
    varyings_info.forEach(info => {
      let cnf = {};
      cnf.offset = s * this.bytes;
      switch (info.type) {
        case gl.FLOAT: cnf.size = 1; break;
        case gl.FLOAT_VEC2: cnf.size = 2; break;
        case gl.FLOAT_VEC3: cnf.size = 3; break;
        case gl.FLOAT_VEC4:
        case gl.FLOAT_MAT2: cnf.size = 4; break;
        case gl.FLOAT_MAT3: cnf.size = 9; break;
        case gl.FLOAT_MAT4: cnf.size = 16; break;
        default: cnf.size = 0; break;
      }
      s += cnf.size;
      if (cnf.size > 0) this.config.push(cnf);
    });
    this.stride = s * this.bytes;
    this.data_length = s * count;
    this.index_length = this.config.length;
  }

  _InitVAO(gl) {
    for (let i = 0; i < this.vao.length; ++i) {
      this.vao[i] = gl.createVertexArray();
      gl.bindVertexArray(this.vao[i]);
      gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo[i]);
      this.config.forEach((cnf, index) => {
        gl.vertexAttribPointer(index, cnf.size, gl.FLOAT,
          false, this.stride, cnf.offset);
        gl.enableVertexAttribArray(index);
      });
      gl.bindVertexArray(null);
      gl.bindBuffer(gl.ARRAY_BUFFER, null);
    }
  }

  _Flip() {
    this.current_vao = (this.current_vao + 1) % 2;
    this.read_vbo = (this.read_vbo + 1) % 2;
    this.write_vbo = (this.write_vbo + 1) % 2;
  }

  Update(gl) {
    if (!this.is_ready) return;
    gl.bindVertexArray(this.vao[this.current_vao]);
    gl.bindBufferBase(gl.TRANSFORM_FEEDBACK_BUFFER, 0, this.vbo[this.write_vbo]);
    gl.enable(gl.RASTERIZER_DISCARD);
    gl.beginTransformFeedback(gl.POINTS);
    gl.drawArrays(gl.POINTS, 0, this.draw_count);
    gl.disable(gl.RASTERIZER_DISCARD);
    gl.endTransformFeedback();
    gl.bindBufferBase(gl.TRANSFORM_FEEDBACK_BUFFER, 0, null);
    this._Flip();
  }

  Use(gl) {
    if (!this.is_ready) return;
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo[this.read_vbo]);
    this.config.forEach((cnf, index) => {
      let adj_index = index + this.index_head;
      gl.vertexAttribPointer(adj_index, cnf.size, gl.FLOAT,
        false, this.stride, cnf.offset);
      gl.enableVertexAttribArray(adj_index);
      gl.vertexAttribDivisor(adj_index, 1)
    });
  }

  GetIndexTail() { return this.index_tail; }

  Delete(gl) {
    this.vao.forEach(vao => gl.deleteVertexArray(vao));
    this.vbo.forEach(vbo => gl.deleteBuffer(vbo));
  }
}