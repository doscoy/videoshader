"use strict";

import { kConfig } from "./config.mjs"

class AudioAnalyser {
  size_factor = null;
  smoothing = null;
  tex_width = null;
  s_fft_size = null;
  w_fft_size = null;
  context = new AudioContext();
  splitter = this.context.createChannelSplitter(2);
  s_analyser = new Array(2);
  w_analyser = new Array(2);
  video_obj = null;
  source = null;
  source_mng = new WeakMap();

  constructor(size_factor, smoothing, video_obj) {
    this.size_factor = Math.floor(Math.max(Math.min(size_factor, 10), 5));
    this.smoothing = Math.max(Math.min(smoothing, 1.0), 0.0);
    this.tex_width = Math.pow(2, this.size_factor);
    this.w_fft_size = this.tex_width;
    this.s_fft_size = this.w_fft_size * 2;
    this.video_obj = video_obj;
    this.video_obj.SetMissCallback((() => this.Reset()));
    this._Init()
  }

  _SetSource() {
    let elm = this.video_obj.GetElement();
    if (this.source_mng.has(elm)) this.source = this.source_mng.get(elm);
    else {
      this.source = this.context.createMediaElementSource(elm);
      this.source_mng.set(elm, this.source);
    }
  }

  _Init() {
    this._SetSource();
    this.source.connect(this.splitter);
    this.source.connect(this.context.destination);
    for (let i = 0; i < 2; ++i) {
      this.s_analyser[i] = this.context.createAnalyser();
      this.s_analyser[i].fftSize = this.s_fft_size;
      this.s_analyser[i].smoothingTimeConstant = this.smoothing;
      this.splitter.connect(this.s_analyser[i], i);
      this.w_analyser[i] = this.context.createAnalyser();
      this.w_analyser[i].fftSize = this.w_fft_size;
      this.splitter.connect(this.w_analyser[i], i);
    }
  }

  Reset() {
    this.source.disconnect();
    this._SetSource();
    this.source.connect(this.splitter);
    this.source.connect(this.context.destination);
  }

  GetTextureWidth() { return this.tex_width; }

  GetSpectrumData(index) {
    let spectrums = new Uint8Array(this.s_analyser[index].frequencyBinCount);
    this.s_analyser[index].getByteFrequencyData(spectrums);
    return spectrums
  }

  GetWaveData(index) {
    let waves = new Uint8Array(this.w_analyser[index].fftSize);
    this.w_analyser[index].getByteTimeDomainData(waves);
    return waves;
  }
}

export class AudioTexture {
  is_use = false;
  texture = null;
  width = null;
  height = 4;
  analyser = null;
  
  constructor(gl, video_obj) {
    this.analyser = new AudioAnalyser(
      kConfig.audio_analyser.size_factor, kConfig.audio_analyser.smoothing_time, video_obj
    );
    this.width = this.analyser.GetTextureWidth();
    this.texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.texStorage2D(gl.TEXTURE_2D, 1, gl.R8, this.width, this.height);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.bindTexture(gl.TEXTURE_2D, null);
  }

  _WriteData(gl, target_height, source) {
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.texSubImage2D(gl.TEXTURE_2D, 0, 0, target_height,
      this.width, 1, gl.RED, gl.UNSIGNED_BYTE, source);
  }

  Update(gl) {
    if (this.is_use) {
      for (let i = 0; i < 2; ++i) {
        this._WriteData(gl, i, this.analyser.GetSpectrumData(i));
        this._WriteData(gl, i + 2, this.analyser.GetWaveData(i));
      }
    }
  }

  UseTexture(gl, location, number) {
    gl.activeTexture(gl.TEXTURE0 + number);
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.uniform1i(location, number);
  }

  SetUseFlag(flag) { this.is_use = this.is_use || flag; }
  ResetUseFlag() { this.is_use = false; }
  ResetAnalyser() { this.analyser.Reset(); }
  Delete(gl) { gl.deleteTexture(this.texture); }
}