"use strict";

class Shader {
  is_ready = false;
  is_use = false;
  name = null;
  program = null;
  uni_location = new Array();
  tex_location = new Array();
  message = "";

  constructor(name) { this.name = name; }

  static _CreateShader(gl, type, code, sub) {
    let shader = gl.createShader(type);
    gl.shaderSource(shader, code);
    gl.compileShader(shader);
    let status = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    let info = gl.getShaderInfoLog(shader);
    return {
      shader: shader,
      status: status,
      info: info === "" ? "" : sub + "\n" + info + "\n"
    };
  }

  static _CreateProgram(gl, vert, frag, sub) {
    let program = gl.createProgram();
    gl.attachShader(program, vert);
    gl.attachShader(program, frag);
    gl.linkProgram(program);
    let link = gl.getProgramParameter(program, gl.LINK_STATUS);
    let validate = gl.getProgramParameter(program, gl.VALIDATE_STATUS);
    let info = gl.getProgramInfoLog(program);
    return {
      program: program,
      link: link,
      validate: validate,
      info: info === "" ? "" : sub + "\n" + info + "\n"
    };
  }

  static _SpecifyUniform(gl, location, value) {
    if (!Array.isArray(value)) gl.uniform1f(location, value);
    else {
      switch (value.length) {
        case 1: gl.uniform1fv(location, value); break;
        case 2: gl.uniform2fv(location, value); break;
        case 3: gl.uniform3fv(location, value); break;
        case 4: gl.uniform4fv(location, value); break;
        default: gl.uniform1fv(location, value); break;
      }
    }
  }

  IsReady() { return this.is_ready; }
  IsUse() { return this.is_use; }
  GetName() { return this.name; }
  GetCompileMessage() { return this.message; }

  SetUniformCallback(gl, name, callback) {
    if (!this.is_ready) return false;
    let location = gl.getUniformLocation(this.program, name);
    if (location === null) return false;
    this.uni_location.push({
      name: name,
      location: location,
      callback: callback
    });
    return true;
  }

  SetTextureCallback(gl, name, number, callback) {
    if (!this.is_ready) return false;
    let location = gl.getUniformLocation(this.program, name);
    if (location === null) return false;
    this.tex_location.push({
      name: name,
      location: location,
      number: number,
      callback: callback
    });
    return true;
  }

  UseUniformCallback(gl) {
    if (!this.is_ready) return false;
    this.uni_location.forEach(cb => {
      Shader._SpecifyUniform(gl, cb.location, cb.callback());
    });
  }

  UseTextureCallback(gl) {
    if (!this.is_ready) return false;
    this.tex_location.forEach(cb => cb.callback(gl, cb.location, cb.number));
  }

  UseUniforms(gl, uniforms) {
    if (!this.is_ready) return false;
    uniforms.forEach(uniform => {
      let location = gl.getUniformLocation(this.program, uniform.name);
      if (location !== null) Shader._SpecifyUniform(gl, location, uniform.values);
    });
  }

  Use(gl) { if (this.is_ready) gl.useProgram(this.program); }
  Delete(gl) { if (this.program !== null) gl.deleteProgram(this.program); }
}

export class DrawShader extends Shader {
  vertex_sub = "vertex code";
  fragment_sub = "fragment code";
  program_sub = "draw program";

  constructor(gl, name, vert_code, frag_code) {
    super(name);
    let vert = Shader._CreateShader(gl, gl.VERTEX_SHADER,
      vert_code, this.vertex_sub);
    this.message += vert.status ? "" : vert.info;
    let frag = Shader._CreateShader(gl, gl.FRAGMENT_SHADER,
      frag_code, this.fragment_sub);
    this.message += frag.status ? "" : frag.info;
    let program = Shader._CreateProgram(gl, vert.shader, frag.shader,
      this.program_sub);
    this.program = program.program;
    this.message += program.info;
    this.is_ready = program.link;
    this.is_use = true;
    gl.deleteShader(vert.shader);
    gl.deleteShader(frag.shader);
  }
}

export class FeedBackShader extends Shader {
  feedback_sub = "feedback code";
  fragment_sub = "dummy code";
  program_sub = "feedback program";
  frag_code = "#version 300 es\nprecision mediump float;layout (location=0) out vec4 _color;void main(void){_color=vec4(1.);}";
  varyings_info = new Array();

  constructor(gl, name, fdbk_code, varyings) {
    super(name);
    if (fdbk_code === "none") return;
    let vert = Shader._CreateShader(gl, gl.VERTEX_SHADER,
      fdbk_code, this.feedback_sub);
    this.message += vert.status ? "" : vert.info;
    let frag = Shader._CreateShader(gl, gl.FRAGMENT_SHADER,
      this.frag_code, this.fragment_sub);
    this.message += frag.status ? "" : frag.info;
    let program = FeedBackShader._CreateProgram(gl, vert.shader, frag.shader,
      varyings, gl.INTERLEAVED_ATTRIBS,
      this.program_sub);
    this.program = program.program;
    this.message += program.info;
    this.is_ready = program.link && program.varyings.length !== 0;
    this.is_use = true;
    this.varyings_info = program.varyings;
    gl.deleteShader(vert.shader);
    gl.deleteShader(frag.shader);
  }

  static _CreateProgram(gl, vert, frag, varyings, mode, sub) {
    if (!Array.isArray(varyings) || varyings.length === 0) {
      return FeedBackShader._NoVaryings(gl, vert, frag, sub);
    }
    let program = gl.createProgram();
    gl.attachShader(program, vert);
    gl.attachShader(program, frag);
    gl.transformFeedbackVaryings(program, varyings, mode);
    gl.linkProgram(program);
    let link = gl.getProgramParameter(program, gl.LINK_STATUS);
    let validate = gl.getProgramParameter(program, gl.VALIDATE_STATUS);
    let info = gl.getProgramInfoLog(program).replace(unescape("%00"), "");
    if (link) {
      return FeedBackShader._GetProgramLinkInfo(gl, program, varyings,
        link, validate, info, sub);
    }
    let info_arr = info.split(" ");
    let varyings_len = varyings.length;
    varyings = varyings.filter(varying => !info_arr.includes(varying));
    if (varyings.length > 0 && varyings.length !== varyings_len) {
      gl.deleteProgram(program);
      return FeedBackShader._CreateProgram(gl, vert, frag, varyings, mode, sub);
    }
    return {
      program: program,
      link: link,
      validate: validate,
      info: sub + "\n" + info + "\n",
      varyings: []
    }
  }

  static _NoVaryings(gl, vert, frag, sub) {
    let result = Shader._CreateProgram(gl, vert, frag, sub);
    result.link = false;
    result.info += "*!* this shader is not available for transform feedback because it has no varying variables\n";
    result.varyings = [];
    return result;
  }

  static _GetProgramLinkInfo(gl, program, varyings, link, validate, info, sub) {
    let varyings_info = new Array();
    varyings.forEach((v, index) => {
      varyings_info.push(gl.getTransformFeedbackVarying(program, index));
    });
    return {
      program: program,
      link: link,
      validate: validate,
      info: info === "" ? "" : sub + "\n" + info + "\n",
      varyings: varyings_info
    }
  }
  
  GetVeryingsInfo() { return this.varyings_info; }
}