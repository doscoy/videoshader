"use strict";

class Texture {
  texture = null;
  width = null;
  height = null;

  constructor(gl, width, height, source) {
    this.texture = gl.createTexture();
    if (width > 0 && height > 0) {
      this._NewImage(gl, width, height, source);
    }
    else {
      this._DummyImage(gl);
    }
    this._InitTexture(gl);
    gl.bindTexture(gl.TEXTURE_2D, null);
  }

  _InitTexture(gl) {
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  }

  _NewImage(gl, width, height, source) {
    this.width = width - width % 2;
    this.height = height - height % 2;
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.texStorage2D(gl.TEXTURE_2D, 1, gl.RGBA8, this.width, this.height);
    this._UpdateImage(gl, source);
  }

  _UpdateImage(gl, source) {
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texSubImage2D(gl.TEXTURE_2D, 0, 0, 0,
      this.width, this.height, gl.RGBA, gl.UNSIGNED_BYTE, source);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
  }

  _DummyImage(gl) {
    this.width = 1;
    this.height = 1;
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
  }

  Update(gl, width, height, source) {
    if (this.width === width && this.height === height) {
      this._UpdateImage(gl, source);
    }
    else if (width > 0 && height > 0) {
      gl.deleteTexture(this.texture);
      this.texture = gl.createTexture();
      this._NewImage(gl, width, height, source);
      this._InitTexture(gl);
    }
    else {
      gl.deleteTexture(this.texture);
      this.texture = gl.createTexture();
      this._DummyImage(gl);
      this._InitTexture(gl);
    }
  }

  Bind(gl) {
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
  }

  Delete(gl) {
    gl.deleteTexture(this.texture);
  }
}

export class VideoTexture {
  is_use = false;
  video_obj = null
  textures = new Array();
  current = 0;
  previous = 1;

  constructor(gl, video_obj) {
    this.video_obj = video_obj;
    for (let i = 0; i < 2; ++i) {
      let tex = new Texture(gl,
        this.video_obj.GetWidth(),
        this.video_obj.GetHeight(),
        this.video_obj.GetElement());
      this.textures.push(tex);
    }
  }

  _Flip() {
    this.current = (this.current + 1) % 2;
    this.previous = (this.previous + 1) % 2;
  }

  Update(gl) {
    if (this.is_use) {
      this._Flip();
      this.textures[this.current].Update(gl,
        this.video_obj.GetWidth(),
        this.video_obj.GetHeight(),
        this.video_obj.GetElement());
    }
  }

  UseCurrentTexture(gl, location, number) {
    gl.activeTexture(gl.TEXTURE0 + number);
    this.textures[this.current].Bind(gl);
    gl.uniform1i(location, number);
  }

  UsePreviousTexture(gl, location, number) {
    gl.activeTexture(gl.TEXTURE0 + number);
    this.textures[this.previous].Bind(gl);
    gl.uniform1i(location, number);
  }

  SetUseFlag(flag) { this.is_use = this.is_use || flag; }
  ResetUseFlag() { this.is_use = false; }
  
  Delete(gl) {
    this.textures[0].Delete(gl);
    this.textures[1].Delete(gl);
  }
}