"use strict";
class UniformSlider {
  div = null;
  index_elm = null;
  value_elm = null;
  range_elm = null;
  range_callback = null;

  constructor(index, value, min, max, step) {
    const kCSSClass = {
      index: "UniformIndex",
      value: "RangeValue",
      range: "Range"
    }
    this.div = document.createElement("div");
    this.index_elm = document.createElement("span");
    this.index_elm.classList = kCSSClass.index;
    this.index_elm.innerText = index;
    this.value_elm = document.createElement("span");
    this.value_elm.classList = kCSSClass.value;
    this.value_elm.innerText = value;
    this.range_elm = document.createElement("input");
    this.range_elm.classList = kCSSClass.range;
    this.range_elm.type = "range";
    this.range_elm.min = min;
    this.range_elm.max = max;
    this.range_elm.step = step;
    this.range_elm.value = value;
    this.range_callback = (() => this.value_elm.innerText = this.range_elm.value);
    this.range_elm.addEventListener("input", this.range_callback);
    this.div.appendChild(this.index_elm);
    this.div.appendChild(this.value_elm);
    this.div.appendChild(this.range_elm);
  }

  Delete() {
    this.range_elm.removeEventListener("input", this.range_callback);
    this.div.remove();
  }

  SetMin(min) { this.range_elm.min = min; }
  SetMax(max) { this.range_elm.max = max; }
  SetStep(step) { this.range_elm.step = step; }
  GetDiv() { return this.div; }
  GetValue() { return this.range_elm.value; }
}

class UniformData {
  main_div = null;
  conf_div = null;
  name_elm = null;
  min_elm = null;
  min_callback = null;
  max_elm = null;
  max_callback = null;
  step_elm = null;
  step_callback = null;
  del_elm = null;
  sliders = [];
  type = null;
  dimension = null;

  constructor(data) {
    const kCSSClass = {
      main_div: "UniformRange",
      conf_div: "UniformConfig",
      name: "UniformName Ls",
      min: "MiniInputText",
      max: "MiniInputText",
      step: "MiniInputText",
      del: "Buttons Rs"
    }
    const kPlaceHolder = {
      name: "variable_name",
      min: "min",
      max: "max",
      step: "step",
      del: "delete"
    }
    this.main_div = document.createElement("div");
    this.main_div.classList = kCSSClass.main_div;
    this._InitConfDiv(data, kCSSClass, kPlaceHolder);
    this.main_div.appendChild(this.conf_div);
    this._InitSliders(data);
    this._RegisterEvent();
    this.type = data.type;
    this.dimension = data.dimension;
  }

  static _GetInputElm(type, value, class_name, placeholder) {
    let input = document.createElement("input");
    input.type = type;
    input.value = value;
    input.placeholder = placeholder;
    input.classList = class_name;
    return input;
  }

  static _GetButtonElm(class_name, placeholder) {
    let button = document.createElement("button");
    button.type = "button";
    button.innerText = placeholder;
    button.classList = class_name;
    return button;
  }

  _InitConfDiv(data, kCSSClass, kPlaceHolder) {
    this.conf_div = document.createElement("div");
    this.conf_div.classList = kCSSClass.conf_div;
    this.name_elm = UniformData._GetInputElm("text", data.name,
      kCSSClass.name, kPlaceHolder.name);
    this.name_elm.spellcheck = false;
    this.min_elm = UniformData._GetInputElm("number", data.min,
      kCSSClass.min, kPlaceHolder.min);
    this.max_elm = UniformData._GetInputElm("number", data.max,
      kCSSClass.max, kPlaceHolder.max);
    this.step_elm = UniformData._GetInputElm("number", data.step,
      kCSSClass.step, kPlaceHolder.step);
    this.del_elm = UniformData._GetButtonElm(kCSSClass.del, kPlaceHolder.del);
    this.conf_div.appendChild(this.name_elm);
    this.conf_div.appendChild(this.min_elm);
    this.conf_div.appendChild(this.max_elm);
    this.conf_div.appendChild(this.step_elm);
    this.conf_div.appendChild(this.del_elm);
  }

  _InitSliders(data) {
    let type = data.type === "float" ? "n" : data.type;
    for (let i = 0; i < data.dimension; ++i) {
      let slider = new UniformSlider(type[i], data.values[i],
        data.min, data.max, data.step);
      this.sliders.push(slider);
      this.main_div.appendChild(slider.GetDiv());
    }
  }

  _RegisterEvent() {
    this.min_callback = (() => {
      this.sliders.forEach(slider => slider.SetMin(this.min_elm.value));
    });
    this.min_elm.addEventListener("change", this.min_callback);
    this.max_callback = (() => {
      this.sliders.forEach(slider => slider.SetMax(this.max_elm.value));
    });
    this.max_elm.addEventListener("change", this.max_callback);
    this.step_callback = (() => {
      this.sliders.forEach(slider => slider.SetStep(this.step_elm.value));
    });
    this.step_elm.addEventListener("change", this.step_callback);
  }

  Delete() {
    this.min_elm.removeEventListener("change", this.min_callback);
    this.max_elm.removeEventListener("change", this.max_callback);
    this.step_elm.removeEventListener("change", this.step_callback);
    this.sliders.forEach(slider => slider.Delete());
    this.main_div.remove();
  }

  GetDiv() { return this.main_div; }
  GetDelButton() { return this.del_elm; }

  GetValues() {
    let values = [];
    this.sliders.forEach(slider => values.push(slider.GetValue()));
    return values;
  }

  GetData() {
    let data = {
      name: this.name_elm.value,
      min: this.min_elm.value,
      max: this.max_elm.value,
      step: this.step_elm.value,
      type: this.type,
      dimension: this.dimension,
      values: this.GetValues()
    }
    return data;
  }
}

export class UniformsManager {
  target_div = null;
  type_elm = null;
  add_elm = null;
  uniforms = new Map;

  constructor(init) {
    this.target_div = init.target_div;
    this.type_elm = init.type_elm;
    this.add_elm = init.add_elm;
    this.add_elm.addEventListener("click", () => {
      let type = this.type_elm.value === "float" ? "n" : this.type_elm.value;
      let data = {
        name: "",
        min: 0.0,
        max: 1.0,
        step: 0.01,
        type: type,
        dimension: type.length,
        values: new Array(type.length).fill(0.5)
      }
      this._SetUniform(data);
    });
  }

  _SetUniform(data) {
    let uniform = new UniformData(data);
    this.target_div.appendChild(uniform.GetDiv());
    let del_button = uniform.GetDelButton();
    let del_callback = (() => {
      uniform.Delete();
      del_button.removeEventListener("click", this.uniforms.get(uniform));
      this.uniforms.delete(uniform);
    });
    this.uniforms.set(uniform, del_callback);
    del_button.addEventListener("click", del_callback);
  }

  SetUniforms(datas) {
    datas.forEach(data => {
      this._SetUniform(data);
    })
  }

  GetDatas() {
    let datas = [];
    this.uniforms.forEach((value, key, map) => datas.push(key.GetData()));
    return datas;
  }
  
  Reset() {
    this.uniforms.forEach((value, key, map) => key.Delete());
    this.uniforms.clear();
  }
}