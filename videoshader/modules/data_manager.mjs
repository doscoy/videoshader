"use strict";
import { GlslEditor } from "./glsl_editor.mjs";

class AbsDataManager {
  name = null;
  select_elm = null;
  text_elm = null;

  constructor(init) {
    this.name = init.name;
    this.select_elm = init.select_elm;
    this.text_elm = init.text_elm;
    this._Init();
  }

  _AddSelectOption(name) {

    let option = document.createElement("option");
    option.text = name;
    option.value = name;
    option.setAttribute("id", "op_" + name);
    let list = Array.prototype.slice.call(this.select_elm.children);
    if (list.length === 0) {
      this.select_elm.appendChild(option);
      return;
    }
    else {
      let item = list.find(item => item.value > name);
      if (item !== void 0) this.select_elm.insertBefore(option, item);
      else this.select_elm.appendChild(option);
    }
  }

  _Init() {
    chrome.storage.local.get(this.name, items => {
      items[this.name].forEach(item => this._AddSelectOption(item.name));
      this.text_elm.value = this.select_elm.value;
    });
  }

  _GetStorageItem(name) {
    return new Promise(resolve => {
      chrome.storage.local.get(this.name, items => {
        let item = items[this.name].find(item => item.name === name);
        resolve(item);
      });
    });
  }

  _SetStorageItem(new_item) {
    chrome.storage.local.get(this.name, items => {
      let index = items[this.name].findIndex(item => item.name === new_item.name);
      if (index < 0) {
        items[this.name].push(new_item);
        this._AddSelectOption(new_item.name);
        this.select_elm.value = new_item.name;
      }
      else {
        items[this.name][index] = new_item;
      }
      chrome.storage.local.set(items);
    });
  }

  _DeleteStorageItem(name) {
    return new Promise(resolve => {
      if (name[0] < "A") {
        this._AlertRemove();
        resolve("");
      }
      else {
        chrome.storage.local.get(this.name, items => {
          if (items[this.name].length === 0) resolve("");
          else {
            let index = items[this.name].findIndex(item => item.name === name);
            items[this.name].splice(index, 1);
            chrome.storage.local.set(items);
            this.select_elm.querySelector("#op_" + name).remove();
            if (items[this.name].length === 0) resolve("");
            else {
              index = index === 0 ? 0 : index - 1;
              resolve(items[this.name][index].name);
            }
          }
        });
      }
    });
  }

  _IsValueOnSelectList(value) {
    let list = Array.prototype.slice.call(this.select_elm.children);
    let item = list.find(item => item.value === value);
    return item !== void 0;
  }

  _CheckDuplicate() {
    if (this.select_elm.value === this.text_elm.value) return false;
    else return this._IsValueOnSelectList(this.text_elm.value);
  }

  _CheckReserved() { return this.text_elm.value[0] < "A"; }
  _CheckRename() { return this.select_elm.value[0] < "A"; }
  _AlertDuplicate() { alert("There is a duplicate name!!"); }
  _AlertBlank() { alert("Enter a name!!"); }
  _AlertReserved() { alert("There is a reserved!!"); }
  _AlertRename() { alert("Can't rename this!!"); }
  _AlertRemove() { alert("Can't delete this!!"); }

  ChangeCurrent(value) {
    this.text_elm.value = value;
    if (this._IsValueOnSelectList(value)) this.select_elm.value = value;
  }

  GetSelectValue() { return this.select_elm.value; }
  GetTextValue() { return this.text_elm.value; }
  SetTextValue(value) { this.text_elm.value = value; }

  RenameCurrent() {
    return new Promise(resolve => {
      if (this.text_elm.value === "") return this._AlertBlank();
      if (this._CheckDuplicate()) return this._AlertDuplicate();
      if (this._CheckReserved()) return this._AlertReserved();
      if (this._CheckRename()) return this._AlertRename();
      chrome.storage.local.get(this.name, items => {
        let index = items[this.name].findIndex(item => item.name === this.select_elm.value);
        if (index >= 0) {
          items[this.name][index].name = this.text_elm.value;
          chrome.storage.local.set(items);
          this.select_elm.querySelector("#op_" + this.select_elm.value).remove();
          this._AddSelectOption(this.text_elm.value);
          this.select_elm.value = this.text_elm.value;
        }
        resolve()
      });
    });
  }
}

export class EffectManager extends AbsDataManager {
  current = null;

  constructor(init) {
    super(init);
  }

  ChangeCurrent(value) {
    super.ChangeCurrent(value);
    return new Promise(async resolve => {
      this.current = await this._GetStorageItem(value);
      if (this.current === void 0) {
        this.current = {
          name: "",
          fdbk: "",
          vert: "",
          frag: "",
          uniforms: [],
          background: { r: 0.0, g: 0.0, b: 0.0, a: 1.0 },
          model: "panel",
          count: 1
        }
      }
      chrome.storage.local.set({ current: this.current.name });
      resolve();
    });
  }

  SaveData(fdbk_name, vert_name, frag_name, uniforms, bg_rgba, model, count) {
    if (this.text_elm.value === "") return this._AlertBlank();
    if (this._CheckDuplicate()) return this._AlertDuplicate();
    if (this._CheckReserved()) return this._AlertReserved();
    this.current.name = this.text_elm.value;
    this.current.fdbk = fdbk_name;
    this.current.vert = vert_name;
    this.current.frag = frag_name;
    this.current.uniforms = uniforms;
    this.current.background = bg_rgba;
    this.current.model = model;
    this.current.count = count;
    this._SetStorageItem(this.current);
    chrome.storage.local.set({ current: this.current.name });
  }

  DeleteData() {
    return new Promise(async resolve => {
      let new_name = await this._DeleteStorageItem(this.select_elm.value);
      if (new_name !== "") await this.ChangeCurrent(new_name);
      chrome.storage.local.set({ current: new_name });
      resolve();
    });
  }

  RenameCurrent() {
    super.RenameCurrent().then(() => {
      chrome.storage.local.set({ current: this.select_elm.value });
      this.current.name = this.select_elm.value;
    });
  }

  GetCurrentFdbk() { return this.current.fdbk; }
  GetCurrentVert() { return this.current.vert; }
  GetCurrentFrag() { return this.current.frag; }
  GetCurrentUniforms() { return this.current.uniforms; }
  GetCurrentBgColor() { return this.current.background; }
  GetCurrentModel() { return this.current.model; }
  GetCurrentCount() { return this.current.count; }
}

export class CodeManager extends AbsDataManager {
  default_code = null;
  editor = null;

  constructor(init) {
    super(init);
    this.editor = new GlslEditor(init.code_elm);
    this.default_code = init.default;
  }

  async ChangeCurrent(value) {
    super.ChangeCurrent(value);
    let item = await this._GetStorageItem(value);
    if (item !== void 0) this.editor.SetValue(item.code);
    else {
      let new_item = {
        name: value,
        code: this.default_code
      }
      this._SetStorageItem(new_item);
      this.editor.SetValue(this.default_code);
    }
    this.editor.ResetPosition();
  }

  SaveCode() {
    if (this.text_elm.value === "") return this._AlertBlank();
    if (this._CheckDuplicate()) return this._AlertDuplicate();
    if (this._CheckReserved()) return this._AlertReserved();
    let new_item = {
      name: this.text_elm.value,
      code: this.editor.GetValue()
    }
    this._SetStorageItem(new_item);
  }

  async DeleteCode() {
    let new_name = await this._DeleteStorageItem(this.select_elm.value);
    if (new_name !== "") this.ChangeCurrent(new_name);
  }
  
  GetCurrentCode() { return this.editor.GetValue(); }
  SetCode(code) { this.editor.SetValue(code); }
  SetDefaultCode() { this.editor.SetValue(this.default_code); }
}